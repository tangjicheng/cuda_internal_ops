#pragma once
#include <stdio.h>
#include <vector>
#include "cudnn.h"
#include "source/ndarray/ndarray.h"
#include "utils.h"

namespace internal_ops {

class Softmax {
 public:
  Softmax() : algo_(CUDNN_SOFTMAX_ACCURATE), mode_(CUDNN_SOFTMAX_MODE_INSTANCE) {}

  ~Softmax() {
    CHECK_CUDNN(cudnnDestroy(handle_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(xDesc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(yDesc_));
  }

  int Init(std::string algo = "accurate", std::string mode = "instance") {
    int ret = 0;
    CHECK_CUDNN(cudnnCreate(&handle_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&xDesc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&yDesc_));
    if (algo == "fast") {
      algo_ = CUDNN_SOFTMAX_FAST;
    }
    if (algo == "log") {
      algo_ = CUDNN_SOFTMAX_LOG;
    }
    if (mode == "channel") {
      mode_ = CUDNN_SOFTMAX_MODE_CHANNEL;
    }
    return ret;
  }

  int softmax(NDArray* y, const NDArray* x, int axis, cudaStream_t* pCur_stream = NULL) {
    if (pCur_stream != NULL) {
      cudnnSetStream(handle_, *pCur_stream);
    }

    int ret = 0;
    auto tshape = x->shape();
    auto shape = tshape.to_vector();
    int rows = 1;
    int cols = 1;
    if (axis >= shape.size() || axis < 0) {
      printf("[Error] Softmax axis >= dim, axis is %d, dim is %d\n", axis, shape.size());
      rows = x->rows();
      cols = x->cols();
    } else {
      for (int i = 0; i < axis; i++) {
        rows *= shape[i];
      }
      for (int i = axis; i < shape.size(); i++) {
        cols *= shape[i];
      }
    }

    float alpha = 1.0f;
    float beta = 0.0f;
    int n = rows;
    int c = 1;
    int h = 1;
    int w = cols;
    y->resize(x->shape());

    CHECK_CUDNN(cudnnSetTensor4dDescriptor(xDesc_, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, n, c, h, w));
    CHECK_CUDNN(cudnnSetTensor4dDescriptor(yDesc_, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, n, c, h, w));
    CHECK_CUDNN(cudnnSoftmaxForward(handle_, algo_, mode_, &alpha, xDesc_, x->data(), &beta, yDesc_, y->data()));

    return ret;
  }

 private:
  cudnnHandle_t handle_;
  cudnnSoftmaxAlgorithm_t algo_;
  cudnnSoftmaxMode_t mode_;
  cudnnTensorDescriptor_t xDesc_;
  cudnnTensorDescriptor_t yDesc_;
};
}  // namespace internal_ops