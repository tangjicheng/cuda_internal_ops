#pragma once

#include "enum.h"
#include "ndarray/ndarray.h"

#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/extrema.h>
#include <thrust/functional.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>

#include "cuda_params.h"
#include "cuda_utils.h"

using namespace internal_ops;

template <class T>
using DeviceVector = thrust::device_vector<T>;
template <class T>
using HostVector = thrust::host_vector<T>;

typedef DeviceVector<float> DevVecF;
typedef DeviceVector<float*> DevVecFP;

// 实现一类pointwise，只有一个输入和一个输出，且没有额外参数
// 输入: input_tensor
// 输出: output_tensor
using PointwiseFunctionPtr = float(float);

template <PointwiseFunctionPtr func>
__global__ void gPointwise(float* out, const float* in, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          out[j * cols + i] = func(in[j * cols + i]);
        }
      }
    }
  }
}

template <PointwiseFunctionPtr func>
int Pointwise(NDArray* out, const NDArray* in, cudaStream_t stream = 0) {
  int ret = 0;
  out->resize(in->shape());

  int blocks = std::min(MAX_BLOCKS, in->rows());
  int threads = std::min(MAX_THREADS, in->cols());

  gPointwise<func><<<blocks, threads, 0, stream>>>(out->data(), in->data(), in->rows(), in->cols());

  return ret;
}

__global__ void gElement(int symbol, float* out, float scale, int rows, int cols);

__global__ void gElement(int symbol, float* out, const float* in1, float scale, int rows, int cols);

__global__ void gElement(int symbol, float* out, const float* in1, const float* in2, int rows, int cols);

__global__ void gBroadcastAdd(float* out, const float* in1, int rows, int cols);

__global__ void gBroadcastAddNCHW(float* out, const float* in1, int n, int c, int h, int w);

__global__ void gBroadcastVec(int symbol, float* out, const float* in, int rows, int cols, int dim_size, int pre_mul);

__global__ void gBroadcastVec3D(int symbol, float* out, const float* in, int rows, int cols, int in_rows, int in_cols);

__global__ void gSub2(float* out, const float* in1, float scale, int rows, int cols);

__global__ void gElementBroadcast(int32_t symbol, int32_t out_rows, int32_t out_cols, int32_t input_ndarry_size,
                                  int32_t out_ndim, int64_t* data, float* offset, float* stride, float* prod_row);

__global__ void gElementBroadcast2(int32_t symbol, int32_t out_rows, int32_t out_cols, int32_t input_ndarry_size,
                                   int32_t out_ndim, int64_t* data, float* offset, float* stride, float* prod_row);

__global__ void gGather(float* outputPtr, const float* inputPtr, const float* indicesPtr, int outside, int N,
                        int outputOutsideStride, int inputOutsideStride, int insideStride, int limit);

__global__ void gConcat(float* outputPtr, int64_t* inputPtr, int32_t inputSize, int32_t num_concats,
                        int32_t concat_size, int32_t output_concat_stride, float* input_limit);

__global__ void gSliceCopy(float* out, const float* in, size_t in_row_begin, size_t in_col_begin, size_t in_col_len,
                           size_t out_row_len, size_t out_col_len);

__global__ void gShapeStrideCopy(float* out_shape, float* out_stride, const int* in_shape, const int* in_stride,
                                 size_t out_row_len, size_t out_col_len);

__global__ void gCopyRowsGeneral(float* out, const float* in, size_t cols, size_t* dst_ids, size_t* src_ids,
                                 size_t numPairs);

__global__ void gSetValue(float* out, int32_t out_rows, int32_t out_cols, float value);