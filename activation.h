#pragma once

#include "cuda_params.h"
#include "enum.h"
#include "ndarray/ndarray.h"
namespace internal_ops {
__global__ void gRelu(float* out, int size);

__global__ void gTanh(float* out, int size);

__global__ void gGelu(float* out, int size);

__global__ void gLog(float* out, int size);

// 目前激活函数，有带有1个参数和2个参数的两类
using ActivationFunc1 = float(float x, float alpha);
using ActivationFunc2 = float(float x, float alpha, float beta);

// ActivationFunc1
__forceinline__ __device__ float internal_ops_elu(float x, float alpha) { return x > 0.0f ? x : alpha * (expf(x) - 1.0f); }

__forceinline__ __device__ float internal_ops_leaky_relu(float x, float alpha) { return x > 0.0f ? x : alpha * x; }

__forceinline__ __device__ float internal_ops_thresholded_relu(float x, float alpha) { return x > alpha ? x : 0.0f; }

// ActivationFunc2
__forceinline__ __device__ float internal_ops_hard_sigmoid(float x, float alpha, float beta) {
  return fmaxf(fminf(alpha * x + beta, 1.0f), 0.0f);
}

__forceinline__ __device__ float internal_ops_selu(float x, float alpha, float gamma) {
  return x > 0.0f ? gamma * x : gamma * alpha * (expf(x) - 1.0f);
}

__forceinline__ __device__ float internal_ops_clip(float x, float min_value, float max_value) {
  if (x > max_value) {
    return max_value;
  }
  if (x < min_value) {
    return min_value;
  }
  return x;
}

template <ActivationFunc1 func>
__global__ void gActivation1(float* out, const float* in, float alpha, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      float* rowOut = out + j * cols;
      const float* rowIn1 = in + j * cols;
      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          rowOut[i] = func(rowIn1[i], alpha);
        }
      }
    }
  }
  return;
}

template <ActivationFunc2 func>
__global__ void gActivation2(float* out, const float* in, float alpha, float beta, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      float* rowOut = out + j * cols;
      const float* rowIn1 = in + j * cols;
      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          rowOut[i] = func(rowIn1[i], alpha, beta);
        }
      }
    }
  }
  return;
}

template <ActivationFunc1 func>
int Activation1(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream = 0) {
  int ret = 0;
  out->resize(in->shape());

  int blocks = std::min(MAX_BLOCKS, in->rows());
  int threads = std::min(MAX_THREADS, in->cols());

  gActivation1<func><<<blocks, threads, 0, stream>>>(out->data(), in->data(), alpha, in->rows(), in->cols());

  return ret;
}

template <ActivationFunc2 func>
int Activation2(NDArray* out, const NDArray* in, float alpha, float beta, cudaStream_t stream = 0) {
  int ret = 0;
  out->resize(in->shape());

  int blocks = std::min(MAX_BLOCKS, in->rows());
  int threads = std::min(MAX_THREADS, in->cols());

  gActivation2<func><<<blocks, threads, 0, stream>>>(out->data(), in->data(), alpha, beta, in->rows(), in->cols());

  return ret;
}

}  // namespace internal_ops