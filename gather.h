// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Gather算子的实现
// input_data, output_data, indice_data都是在GPU显存上的数据
// 按照onnx operator文档中的Gather算子的描述进行实现

#pragma once

#include <vector>

namespace internal_ops {
int GatherImpl(float* output_data, float* input_data, float* indice_data, const std::vector<int>& input_shape,
               const std::vector<int> indices_shape, int axis, cudaStream_t stream);
}
