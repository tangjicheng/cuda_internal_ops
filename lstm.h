#ifndef CUDNN_LSTM_HPP
#define CUDNN_LSTM_HPP

#include <stdio.h>
#include <vector>
#include "common/data_type.h"
#include "cuda_fp16.h"
#include "cudnn.h"
#include "graph_executor/model.h"
#include "source/ndarray/ndarray.h"
#include "source/operator/internal/internal_api.h"
#include "utils.h"

#define MAX_INPUT_LEN_LSTM 1024
#define MAX_BATCH_LSTM 100

namespace internal_ops {

class Lstm {
 public:
  Lstm() : work_size_(0), param_size_(0), work_space_(NULL), param_space_(NULL), data_type_(DataType::float32) {
    hx_half_ = NULL;
    cx_half_ = NULL;
    hy_half_ = NULL;
    cy_half_ = NULL;
    x_half_ = NULL;
    y_half_ = NULL;

    max_batch_ = 0;
    max_step_ = 0;
  }

  ~Lstm() {
    Fini();

    if (hx_half_ != NULL) {
      cudaFree(hx_half_);
      hx_half_ = NULL;
      cudaFree(cx_half_);
      cx_half_ = NULL;
      cudaFree(hy_half_);
      hy_half_ = NULL;
      cudaFree(cy_half_);
      cy_half_ = NULL;
      cudaFree(x_half_);
      x_half_ = NULL;
      cudaFree(y_half_);
      y_half_ = NULL;
    }

    max_batch_ = 0;
    max_step_ = 0;
  }

  int Init(const NDArray** weight_W, const NDArray** weight_R, const NDArray** bias_B, int layer, int x_dim,
           int node_dim, cudaStream_t* pCur_stream = NULL, DataType datatype = DataType::float32,
           bool is_lstm_bidirectional = false) {
    int ret = 0;
    xDim_ = x_dim;
    nodeDim_ = node_dim;
    layer_ = layer;
    data_type_ = datatype;
    is_lstm_bidirectional_ = is_lstm_bidirectional;

    get_cudnn_data_type();

    cudnnCreate(&handle_);

    for (int i = 0; i < MAX_INPUT_LEN_LSTM; ++i) {
      CHECK_CUDNN(cudnnCreateTensorDescriptor(&xDescs_[i]));
      CHECK_CUDNN(cudnnCreateTensorDescriptor(&yDescs_[i]));

      int dim[3] = {MAX_BATCH_LSTM, xDim_, 1};
      int stride[3] = {dim[1] * dim[2], dim[2], 1};
      CHECK_CUDNN(cudnnSetTensorNdDescriptor(xDescs_[i], cudnn_data_type_, 3, dim, stride));
    }
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&hx_desc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&cx_desc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&hy_desc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&cy_desc_));

    CHECK_CUDNN(cudnnCreateDropoutDescriptor(&dropDesc_));
    float drop_rate = 0.0f;
    unsigned long long seed = 1337ull;
    CHECK_CUDNN(cudnnSetDropoutDescriptor(dropDesc_, handle_, drop_rate, NULL, 0, seed));

    CHECK_CUDNN(cudnnCreateRNNDescriptor(&rnn_desc_));

    cudnnDirectionMode_t cudnn_direction_model = CUDNN_UNIDIRECTIONAL;
    if (is_lstm_bidirectional_) {
      cudnn_direction_model = CUDNN_BIDIRECTIONAL;
    }

#ifndef CUDNN_V8
#  ifdef CUDNN_V5
    CHECK_CUDNN(cudnnSetRNNDescriptor(rnn_desc_, node_dim, layer, dropDesc_, CUDNN_LINEAR_INPUT, cudnn_direction_model,
                                      CUDNN_LSTM, cudnn_data_type_));
#  else
    CHECK_CUDNN(cudnnSetRNNDescriptor_v5(rnn_desc_, node_dim, layer, dropDesc_, CUDNN_LINEAR_INPUT,
                                         cudnn_direction_model, CUDNN_LSTM, cudnn_data_type_));
#  endif
#else
    CHECK_CUDNN(cudnnSetRNNDescriptor_v8(rnn_desc_, CUDNN_RNN_ALGO_STANDARD, CUDNN_LSTM, CUDNN_RNN_DOUBLE_BIAS,
                                         CUDNN_BIDIRECTIONAL, CUDNN_LINEAR_INPUT, CUDNN_DATA_FLOAT, CUDNN_DATA_FLOAT,
                                         CUDNN_DEFAULT_MATH, xDim_, node_dim, node_dim, layer, dropDesc_,
                                         CUDNN_RNN_PADDED_IO_DISABLED));
#endif

    size_t worksize = 0;
    CHECK_CUDNN(cudnnGetRNNWorkspaceSize(handle_, rnn_desc_, MAX_INPUT_LEN_LSTM, xDescs_, &worksize));
    if (worksize > work_size_) {
      if (work_space_ != NULL) {
        cudaFree(work_space_);
      }
      cudaMalloc(&work_space_, worksize);
      work_size_ = worksize;
    }

    size_t param_size;
    CHECK_CUDNN(cudnnGetRNNParamsSize(handle_, rnn_desc_, xDescs_[0], &param_size, cudnn_data_type_));
    if (param_size > param_size_) {
      if (param_space_ != NULL) {
        cudaFree(param_space_);
      }
      cudaMalloc(&param_space_, param_size);
      param_size_ = param_size;
    }

    int dim[3] = {0, 1, 1};
    if (cudnn_data_type_ == CUDNN_DATA_FLOAT) {
      dim[0] = (int)(param_size / sizeof(float));
    } else if (cudnn_data_type_ == CUDNN_DATA_HALF) {
      dim[0] = (int)(param_size / sizeof(half));
    }

    CHECK_CUDNN(cudnnCreateFilterDescriptor(&w_desc_));
    CHECK_CUDNN(cudnnSetFilterNdDescriptor(w_desc_, cudnn_data_type_, CUDNN_TENSOR_NCHW, 3, dim));

    LoadParams(weight_W, weight_R, bias_B, layer);

    pre_process(MAX_BATCH_LSTM, layer, node_dim);

    return ret;
  }

  int Fini() {
    int ret = 0;
    for (int i = 0; i < MAX_INPUT_LEN_LSTM; ++i) {
      CHECK_CUDNN(cudnnDestroyTensorDescriptor(xDescs_[i]));
      CHECK_CUDNN(cudnnDestroyTensorDescriptor(yDescs_[i]));
    }
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(hx_desc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(cx_desc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(hy_desc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(cy_desc_));

    CHECK_CUDNN(cudnnDestroyDropoutDescriptor(dropDesc_));
    CHECK_CUDNN(cudnnDestroyRNNDescriptor(rnn_desc_));

    CHECK_CUDNN(cudnnDestroyFilterDescriptor(w_desc_));

    if (work_space_ != NULL) cudaFree(work_space_);
    if (param_space_ != NULL) cudaFree(param_space_);
    cudnnDestroy(handle_);
    return ret;
  }

  int Calc(NDArray& y, const NDArray& x, int32_t* pFrame2BatchNum = NULL, int32_t len = 0, int32_t batch_input = 0,
           bool reset = true, void* stream = NULL) {
    if (stream) CHECK_CUDNN(cudnnSetStream(handle_, *((cudaStream_t*)stream)));
    int ret = 0;

    int batch = 0;
    int step = 0;
    if (pFrame2BatchNum == NULL) {
      batch = x.shape()[1];
      step = x.shape()[0];
    } else {
      batch = batch_input;
      step = len;
    }

    // cudnn lstm check batch and step
    if (batch > MAX_BATCH_LSTM || step > MAX_INPUT_LEN_LSTM) {
      printf(
          "lstm.hpp | error | batch or step exceed the up limit of cudnn | batch = %d, step = %d, MAX_BATCH_LSTM = %d, "
          "MAX_INPUT_LEN_LSTM = %d \n",
          batch, step, MAX_BATCH_LSTM, MAX_INPUT_LEN_LSTM);
      return internal_ops_ERR_ALLOC_FIALED;
    }

    int out_dim = nodeDim_;
    int num_direction = 1;
    if (is_lstm_bidirectional_) {
      out_dim = nodeDim_ * 2;
      num_direction = 2;
    }
    std::vector<int32_t> s = {(int32_t)step * (int32_t)batch, (int32_t)out_dim};
    TShape y_need_shape(s);
    y.AutoResize(y_need_shape);

    if (reset) {
      Reset(batch, step, layer_, pFrame2BatchNum);
    }

    if (cudnn_data_type_ == CUDNN_DATA_HALF) {
      reallocateBuf(batch, step, x, y);
    }

    if (cudnn_data_type_ == CUDNN_DATA_FLOAT) {
      CHECK_CUDNN(cudnnRNNForwardInference(handle_, rnn_desc_, step, xDescs_, x.data(), hx_desc_, NULL, cx_desc_, NULL,
                                           w_desc_, param_space_, yDescs_, y.data(), hy_desc_, NULL, cy_desc_, NULL,
                                           work_space_, work_size_));
    } else if (cudnn_data_type_ == CUDNN_DATA_HALF) {
      // op.Float2Half(x, x_half_);

      CHECK_CUDNN(cudnnRNNForwardInference(handle_, rnn_desc_, step, xDescs_, x_half_, hx_desc_, hx_half_, cx_desc_,
                                           cx_half_, w_desc_, param_space_, yDescs_, y_half_, hy_desc_, hy_half_,
                                           cy_desc_, cy_half_, work_space_, work_size_));

      // op.Half2Float(y, y_half_);
    }

    return ret;
  }

#if 0
    int Calc(Matrix& y, const Matrix& x, Matrix& pre_c, Matrix& pre_m, int32_t* pFrame2BatchNum = NULL, int32_t len = 0, int32_t batch_input = 0, bool reset = true, void *stream=NULL)
    {
        int ret = 0;
        if(stream)
            CHECK_CUDNN(cudnnSetStream(handle_, *((cudaStream_t*) stream)));
        
        int batch = 0;
        int step  = 0;
        if (pFrame2BatchNum == NULL)
        {
            batch = x.Shape()[1];
		    step = x.Shape()[0];
        }
        else
        {
            batch = batch_input;
		    step  = len;
        }

        //cudnn lstm check batch and step
        if (batch > MAX_BATCH_LSTM || step > MAX_INPUT_LEN_LSTM)
        {
            printf("lstm.hpp | error | batch or step exceed the up limit of cudnn | batch = %d, step = %d, MAX_BATCH_LSTM = %d, MAX_INPUT_LEN_LSTM = %d \n", batch, step, MAX_BATCH_LSTM, MAX_INPUT_LEN_LSTM);
            return internal_ops_ERROR_BUFFER_SIZE_NOT_ENOUGH;
        }
        
        std::vector<size_t> s = {(size_t)batch * (size_t)step, (size_t)nodeDim_};
        y.AutoResize(s);

        if (reset)
        {
            Reset(batch, step, layer_, pFrame2BatchNum);
        }

        if (cudnn_data_type_ == CUDNN_DATA_HALF)
        {
            reallocateBuf(batch, step, pre_m, pre_c, x, y);
        }

        if (cudnn_data_type_ == CUDNN_DATA_FLOAT)
        {
            op.Copy(hx_matrix_, pre_m);
            op.Copy(cx_matrix_, pre_c);
            CHECK_CUDNN(cudnnRNNForwardInference(handle_, rnn_desc_, step
            , xDescs_, x.data()
            , hx_desc_, hx_matrix_.data()
            , cx_desc_, cx_matrix_.data()
            , w_desc_, param_space_
            , yDescs_, y.data()
            , hy_desc_, pre_m.data()
            , cy_desc_, pre_c.data()
            , work_space_, work_size_));
        }
        else if (cudnn_data_type_ == CUDNN_DATA_HALF)
        {   
            //op.Float2Half(pre_m, hx_half_);
            //op.Float2Half(pre_c, cx_half_);
            //op.Float2Half(x, x_half_);

            CHECK_CUDNN(cudnnRNNForwardInference(handle_, rnn_desc_, step
            , xDescs_, x_half_
            , hx_desc_, hx_half_
            , cx_desc_, cx_half_
            , w_desc_, param_space_
            , yDescs_, y_half_
            , hy_desc_, hy_half_
            , cy_desc_, cy_half_
            , work_space_, work_size_));

            //op.Half2Float(y, y_half_);
            //op.Half2Float(pre_m, hy_half_);
            //op.Half2Float(pre_c, cy_half_);
        }

        return ret;
    }
#endif

  void pre_process(int max_batch, int layer, int nodeDim) {
    std::vector<int32_t> s = {(int32_t)layer * 2, (int32_t)max_batch, (int32_t)nodeDim * 2};
    TShape sshape(s);
    hx_matrix_.AutoResize(sshape);
    cx_matrix_.AutoResize(sshape);
    hy_matrix_.AutoResize(sshape);
    cy_matrix_.AutoResize(sshape);
  }

  int get_max_input_len() const { return MAX_INPUT_LEN_LSTM; }

 private:
  void get_cudnn_data_type() {
    switch (data_type_) {
      case DataType::float32: {
        cudnn_data_type_ = CUDNN_DATA_FLOAT;
      } break;
      case DataType::float16: {
        cudnn_data_type_ = CUDNN_DATA_HALF;
      } break;
      default: {
        printf("cudnn lstm current do not support this data type, data_type_ = %d \n", data_type_);
      } break;
    }
  }

  void reallocateBuf(int batch, int step, const NDArray& pre_m, const NDArray& pre_c, const NDArray& x,
                     const NDArray& y) {
    if (hx_half_ == NULL || batch > max_batch_ || step > max_step_) {
      if (hx_half_ != NULL) {
        cudaFree(hx_half_);
        cudaFree(cx_half_);
        cudaFree(hy_half_);
        cudaFree(cy_half_);
        cudaFree(x_half_);
        cudaFree(y_half_);
      }
      cudaMalloc((void**)&hx_half_, pre_m.rows() * pre_m.cols() * sizeof(half));
      cudaMalloc((void**)&cx_half_, pre_c.rows() * pre_c.cols() * sizeof(half));
      cudaMalloc((void**)&hy_half_, pre_m.rows() * pre_m.cols() * sizeof(half));
      cudaMalloc((void**)&cy_half_, pre_c.rows() * pre_c.cols() * sizeof(half));
      cudaMalloc((void**)&x_half_, x.rows() * x.cols() * sizeof(half));
      cudaMalloc((void**)&y_half_, y.rows() * y.cols() * sizeof(half));

      max_batch_ = batch;
      max_step_ = step;
    }
  }

  void reallocateBuf(int batch, int step, const NDArray& x, const NDArray& y) {
    if (hx_half_ == NULL || batch > max_batch_ || step > max_step_) {
      if (hx_half_ != NULL) {
        cudaFree(hx_half_);
        cudaFree(cx_half_);
        cudaFree(hy_half_);
        cudaFree(cy_half_);
        cudaFree(x_half_);
        cudaFree(y_half_);
      }
      cudaMalloc((void**)&hx_half_, batch * step * nodeDim_ * sizeof(half));
      cudaMalloc((void**)&cx_half_, batch * step * nodeDim_ * sizeof(half));
      cudaMalloc((void**)&hy_half_, batch * step * nodeDim_ * sizeof(half));
      cudaMalloc((void**)&cy_half_, batch * step * nodeDim_ * sizeof(half));
      cudaMalloc((void**)&x_half_, x.rows() * x.cols() * sizeof(half));
      cudaMalloc((void**)&y_half_, y.rows() * y.cols() * sizeof(half));

      max_batch_ = batch;
      max_step_ = step;
    }
  }

  void Reset(int batch, int step, int layer, int32_t* pFrame2BatchNum = NULL) {
    int xDim[3] = {batch, xDim_, 1};
    int xStride[3] = {xDim[2] * xDim[1], xDim[2], 1};

    int y_dim = nodeDim_;
    if (is_lstm_bidirectional_) {
      y_dim = nodeDim_ * 2;
    }
    int yDim[3] = {batch, y_dim, 1};
    int yStride[3] = {yDim[2] * yDim[1], yDim[2], 1};

    for (int i = 0; i < step; ++i) {
      if (pFrame2BatchNum != NULL) {
        xDim[0] = pFrame2BatchNum[i];
        yDim[0] = pFrame2BatchNum[i];
      }
      CHECK_CUDNN(cudnnSetTensorNdDescriptor(xDescs_[i], cudnn_data_type_, 3, xDim, xStride));
      CHECK_CUDNN(cudnnSetTensorNdDescriptor(yDescs_[i], cudnn_data_type_, 3, yDim, yStride));
    }

    int history_layer = layer;
    if (is_lstm_bidirectional_) {
      history_layer = layer * 2;
    }
    int dim[3] = {history_layer, batch, nodeDim_};
    int stride[3] = {dim[1] * dim[2], dim[2], 1};

    CHECK_CUDNN(cudnnSetTensorNdDescriptor(hx_desc_, cudnn_data_type_, 3, dim, stride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(cx_desc_, cudnn_data_type_, 3, dim, stride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(hy_desc_, cudnn_data_type_, 3, dim, stride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(cy_desc_, cudnn_data_type_, 3, dim, stride));
  }

  void InitMatParam(int id, int layer_id, const NDArray& m) {
    const float* data = m.data();

    cudnnFilterDescriptor_t desc;
    void* weight;
    CHECK_CUDNN(cudnnCreateFilterDescriptor(&desc));
    CHECK_CUDNN(cudnnGetRNNLinLayerMatrixParams(handle_, rnn_desc_, layer_id, xDescs_[0], w_desc_, param_space_, id,
                                                desc, (void**)&weight));
    cudnnDataType_t dataType;
    cudnnTensorFormat_t format;
    int nbDims;
    int dimA[3];

    CHECK_CUDNN(cudnnGetFilterNdDescriptor(desc, 3, &dataType, &format, &nbDims, dimA));

    if (cudnn_data_type_ == CUDNN_DATA_FLOAT) {
      cudaMemcpy((float*)weight, data, dimA[0] * dimA[1] * dimA[2] * sizeof(float), cudaMemcpyDeviceToDevice);
    } else if (cudnn_data_type_ == CUDNN_DATA_HALF) {
      // op.Float2Half(m, (half*)weight);
    }

    CHECK_CUDNN(cudnnDestroyFilterDescriptor(desc));
  }

  void InitBiasParam(int id, int layer_id, const NDArray& m) {
    const float* data = m.data();
    if (m.size() == 0) {
      data = NULL;
    }

    cudnnFilterDescriptor_t desc;
    void* bias;
    CHECK_CUDNN(cudnnCreateFilterDescriptor(&desc));
    CHECK_CUDNN(cudnnGetRNNLinLayerBiasParams(handle_, rnn_desc_, layer_id, xDescs_[0], w_desc_, param_space_, id, desc,
                                              (void**)&bias));

    cudnnDataType_t dataType;
    cudnnTensorFormat_t format;
    int nbDims;
    int dimA[3];
    CHECK_CUDNN(cudnnGetFilterNdDescriptor(desc, 3, &dataType, &format, &nbDims, dimA));

    if (data != NULL) {
      if (cudnn_data_type_ == CUDNN_DATA_FLOAT) {
        cudaMemcpy((float*)bias, data, dimA[0] * dimA[1] * dimA[2] * sizeof(float), cudaMemcpyDeviceToDevice);
      } else if (cudnn_data_type_ == CUDNN_DATA_HALF) {
        // op.Float2Half(m, (half*)bias);
      }
    }

    CHECK_CUDNN(cudnnDestroyFilterDescriptor(desc));
  }

  int LoadParams(const NDArray** weight_W, const NDArray** weight_R, const NDArray** bias_B, int layer) {
    int ret = 0;

    int direction_num = 1;
    if (is_lstm_bidirectional_) {
      direction_num = 2;
    }

    for (int i = 0; i < layer; ++i) {
      TShape wxshape(2);
      wxshape[0] = nodeDim_;
      wxshape[1] = weight_W[i]->shape()[2];
      TShape whshape(2);
      whshape[0] = nodeDim_;
      whshape[1] = nodeDim_;
      NDArray wx_0(wxshape, false);
      NDArray wx_1(wxshape, false);
      NDArray wx_2(wxshape, false);
      NDArray wx_3(wxshape, false);
      NDArray wh_0(whshape, false);
      NDArray wh_1(whshape, false);
      NDArray wh_2(whshape, false);
      NDArray wh_3(whshape, false);
      SliceCopy(wx_0, *weight_W[i], 0, 0, nodeDim_, wxshape[1]);
      SliceCopy(wx_1, *weight_W[i], nodeDim_, 0, nodeDim_, wxshape[1]);
      SliceCopy(wx_2, *weight_W[i], nodeDim_ * 2, 0, nodeDim_, wxshape[1]);
      SliceCopy(wx_3, *weight_W[i], nodeDim_ * 3, 0, nodeDim_, wxshape[1]);
      SliceCopy(wh_0, *weight_R[i], 0, 0, nodeDim_, nodeDim_);
      SliceCopy(wh_1, *weight_R[i], nodeDim_, 0, nodeDim_, nodeDim_);
      SliceCopy(wh_2, *weight_R[i], nodeDim_ * 2, 0, nodeDim_, nodeDim_);
      SliceCopy(wh_3, *weight_R[i], nodeDim_ * 3, 0, nodeDim_, nodeDim_);

      // onnx: iofc, torch: ifco, cudnn need same as torch
      InitMatParam(0, i * direction_num, wx_0);
      InitMatParam(3, i * direction_num, wx_1);
      InitMatParam(1, i * direction_num, wx_2);
      InitMatParam(2, i * direction_num, wx_3);
      InitMatParam(4, i * direction_num, wh_0);
      InitMatParam(7, i * direction_num, wh_1);
      InitMatParam(5, i * direction_num, wh_2);
      InitMatParam(6, i * direction_num, wh_3);

      if (is_lstm_bidirectional_) {
        NDArray wxb_0(wxshape, false);
        NDArray wxb_1(wxshape, false);
        NDArray wxb_2(wxshape, false);
        NDArray wxb_3(wxshape, false);
        NDArray whb_0(whshape, false);
        NDArray whb_1(whshape, false);
        NDArray whb_2(whshape, false);
        NDArray whb_3(whshape, false);
        SliceCopy(wxb_0, *weight_W[i], nodeDim_ * 4, 0, nodeDim_, wxshape[1]);
        SliceCopy(wxb_1, *weight_W[i], nodeDim_ * 5, 0, nodeDim_, wxshape[1]);
        SliceCopy(wxb_2, *weight_W[i], nodeDim_ * 6, 0, nodeDim_, wxshape[1]);
        SliceCopy(wxb_3, *weight_W[i], nodeDim_ * 7, 0, nodeDim_, wxshape[1]);
        SliceCopy(whb_0, *weight_R[i], nodeDim_ * 4, 0, nodeDim_, nodeDim_);
        SliceCopy(whb_1, *weight_R[i], nodeDim_ * 5, 0, nodeDim_, nodeDim_);
        SliceCopy(whb_2, *weight_R[i], nodeDim_ * 6, 0, nodeDim_, nodeDim_);
        SliceCopy(whb_3, *weight_R[i], nodeDim_ * 7, 0, nodeDim_, nodeDim_);

        InitMatParam(0, i * direction_num + 1, wxb_0);
        InitMatParam(3, i * direction_num + 1, wxb_1);
        InitMatParam(1, i * direction_num + 1, wxb_2);
        InitMatParam(2, i * direction_num + 1, wxb_3);
        InitMatParam(4, i * direction_num + 1, whb_0);
        InitMatParam(7, i * direction_num + 1, whb_1);
        InitMatParam(5, i * direction_num + 1, whb_2);
        InitMatParam(6, i * direction_num + 1, whb_3);
      }
    }

    for (int i = 0; i < layer; ++i) {
      TShape bshape(1);
      bshape[0] = nodeDim_;
      NDArray bx_0(bshape, false);
      NDArray bx_1(bshape, false);
      NDArray bx_2(bshape, false);
      NDArray bx_3(bshape, false);
      NDArray bh_0(bshape, false);
      NDArray bh_1(bshape, false);
      NDArray bh_2(bshape, false);
      NDArray bh_3(bshape, false);
      cudaMemcpy(bx_0.data(), bias_B[i]->data(), nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bx_1.data(), bias_B[i]->data() + nodeDim_, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bx_2.data(), bias_B[i]->data() + nodeDim_ * 2, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bx_3.data(), bias_B[i]->data() + nodeDim_ * 3, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bh_0.data(), bias_B[i]->data() + nodeDim_ * 4, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bh_1.data(), bias_B[i]->data() + nodeDim_ * 5, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bh_2.data(), bias_B[i]->data() + nodeDim_ * 6, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
      cudaMemcpy(bh_3.data(), bias_B[i]->data() + nodeDim_ * 7, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);

      InitBiasParam(0, i * direction_num, bx_0);
      InitBiasParam(3, i * direction_num, bx_1);
      InitBiasParam(1, i * direction_num, bx_2);
      InitBiasParam(2, i * direction_num, bx_3);
      InitBiasParam(4, i * direction_num, bh_0);
      InitBiasParam(7, i * direction_num, bh_1);
      InitBiasParam(5, i * direction_num, bh_2);
      InitBiasParam(6, i * direction_num, bh_3);

      if (is_lstm_bidirectional_) {
        NDArray bxb_0(bshape, false);
        NDArray bxb_1(bshape, false);
        NDArray bxb_2(bshape, false);
        NDArray bxb_3(bshape, false);
        NDArray bhb_0(bshape, false);
        NDArray bhb_1(bshape, false);
        NDArray bhb_2(bshape, false);
        NDArray bhb_3(bshape, false);
        cudaMemcpy(bxb_0.data(), bias_B[i]->data() + nodeDim_ * 8, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bxb_1.data(), bias_B[i]->data() + nodeDim_ * 9, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bxb_2.data(), bias_B[i]->data() + nodeDim_ * 10, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bxb_3.data(), bias_B[i]->data() + nodeDim_ * 11, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bhb_0.data(), bias_B[i]->data() + nodeDim_ * 12, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bhb_1.data(), bias_B[i]->data() + nodeDim_ * 13, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bhb_2.data(), bias_B[i]->data() + nodeDim_ * 14, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);
        cudaMemcpy(bhb_3.data(), bias_B[i]->data() + nodeDim_ * 15, nodeDim_ * sizeof(float), cudaMemcpyDeviceToDevice);

        InitBiasParam(0, i * direction_num + 1, bxb_0);
        InitBiasParam(3, i * direction_num + 1, bxb_1);
        InitBiasParam(1, i * direction_num + 1, bxb_2);
        InitBiasParam(2, i * direction_num + 1, bxb_3);
        InitBiasParam(4, i * direction_num + 1, bhb_0);
        InitBiasParam(7, i * direction_num + 1, bhb_1);
        InitBiasParam(5, i * direction_num + 1, bhb_2);
        InitBiasParam(6, i * direction_num + 1, bhb_3);
      }
    }
    return ret;
  }

 private:
  cudnnHandle_t handle_;

  DataType data_type_;
  cudnnDataType_t cudnn_data_type_;

  int xDim_;
  int nodeDim_;
  int layer_;
  int max_batch_;
  int max_step_;

  bool is_lstm_bidirectional_;

  cudnnRNNDescriptor_t rnn_desc_;
  cudnnFilterDescriptor_t w_desc_;

  cudnnTensorDescriptor_t xDescs_[MAX_INPUT_LEN_LSTM];
  cudnnTensorDescriptor_t yDescs_[MAX_INPUT_LEN_LSTM];
  cudnnTensorDescriptor_t hx_desc_;
  cudnnTensorDescriptor_t cx_desc_;
  cudnnTensorDescriptor_t hy_desc_;
  cudnnTensorDescriptor_t cy_desc_;

  cudnnDropoutDescriptor_t dropDesc_;

  void* param_space_;
  size_t param_size_;
  void* work_space_;
  size_t work_size_;

  // fp32
  NDArray hx_matrix_;
  NDArray cx_matrix_;
  NDArray hy_matrix_;
  NDArray cy_matrix_;

  // fp16
  half* hx_half_;
  half* cx_half_;
  half* hy_half_;
  half* cy_half_;
  half* x_half_;
  half* y_half_;
};
}  // namespace internal_ops
#endif
