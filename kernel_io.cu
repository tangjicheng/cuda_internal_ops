#include <cmath>
#include "kernel_io.h"

__global__ void gCopyRows(float* out, const float* in, const int* src_ids, int nums, int cols) {
  for (int bid = blockIdx.x; bid < nums; bid += gridDim.x) {
    int dst_id = bid;
    int src_id = src_ids[bid];

    float* rowOut = out + dst_id * cols;
    const float* rowIn = in + src_id * cols;

    for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
      rowOut[tid] = rowIn[tid];
    }
  }
}

__global__ void gCopyRows(float* out, const float* in, const float* src_ids, int nums, int cols) {
  for (int bid = blockIdx.x; bid < nums; bid += gridDim.x) {
    int dst_id = bid;
    int src_id = roundf(src_ids[bid]);

    float* rowOut = out + dst_id * cols;
    const float* rowIn = in + src_id * cols;

    for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
      rowOut[tid] = rowIn[tid];
    }
  }
}

__global__ void gLowTriangleVec(float* out, const float* in, int idx, int cols) {
  int len = idx + 1;
  const float* rowIn = in + idx * cols;

  for (int tid = threadIdx.x; tid < len; tid += blockDim.x) {
    out[tid] = rowIn[tid];
  }
}

__global__ void gTranspose(float* out, const float* in, int rows, int cols, int* out_trans, int* pre_mul,
                           int* out_shape, int dim) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int row = bid + blockIdx.x;
    if (row < rows) {
      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int col = tid + threadIdx.x;
        if (col < cols) {
          int out_flat_index = row * cols + col;
          int in_flat_index = 0;
          for (int i = dim - 1; i >= 0; i--) {
            size_t out_index = out_flat_index % out_shape[i];
            out_flat_index /= out_shape[i];
            in_flat_index += out_index * pre_mul[out_trans[i]];
          }
          out[row * cols + col] = in[in_flat_index];
        }
      }
    }
  }
}

__global__ void gTranspose3dZYX2ZXYAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE];
  int x_in = 0;
  int y_in = 0;
  int z = 0;
  int x_out = 0;
  int y_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  y_in = ty + TILE_SIZE * by;
  z = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  y_out = tx + TILE_SIZE * by;
  index_in = x_in + (y_in + z * ndimy) * ndimx;
  index_out = y_out + (x_out + z * ndimx) * ndimy;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && y_in < ndimy) {
    tile[tx][ty] = in[index_in];
    if (y_in + gap < ndimy) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx];
    }
    if (y_in + 2 * gap < ndimy) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx];
    }
    if (y_in + 3 * gap < ndimy) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx];
    }
  }

  __syncthreads();

  if (x_out < ndimx && y_out < ndimy) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimy] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimy] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimy] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gTranspose3dZYX2XYZAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE];
  int x_in = 0;
  int y = 0;
  int z_in = 0;
  int x_out = 0;
  int z_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  z_in = ty + TILE_SIZE * by;
  y = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  z_out = tx + TILE_SIZE * by;
  index_in = x_in + (y + z_in * ndimy) * ndimx;
  index_out = z_out + (y + x_out * ndimy) * ndimz;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && z_in < ndimz) {
    tile[tx][ty] = in[index_in];
    if (z_in + gap < ndimz) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx * ndimy];
    }
    if (z_in + 2 * gap < ndimz) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx * ndimy];
    }
    if (z_in + 3 * gap < ndimz) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx * ndimy];
    }
  }

  __syncthreads();

  if (z_out < ndimz && x_out < ndimx) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimz * ndimy] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimz * ndimy] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimz * ndimy] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gTranspose3dZYX2YZXAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE + 1];
  int x = 0;
  int y = 0;
  int z = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x = tx + TILE_SIZE * bx;
  y = ty + TILE_SIZE * by;
  z = blockIdx.z;

  index_in = x + (y + z * ndimy) * ndimx;
  index_out = x + (z + y * ndimz) * ndimx;

  int gap = TILE_SIZE / 4;

  if (x < ndimx && y < ndimy) {
    tile[tx][ty] = in[index_in];
    if (y + gap < ndimy) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx];
    }
    if (y + 2 * gap < ndimy) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx];
    }
    if (y + 3 * gap < ndimy) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx];
    }
  }

  __syncthreads();

  if (x < ndimx && y < ndimy) {
    out[index_out] = tile[tx][ty];
    if (y + gap < ndimy) {
      out[index_out + gap * ndimx * ndimz] = tile[tx][ty + gap];
    }
    if (y + 2 * gap < ndimy) {
      out[index_out + 2 * gap * ndimx * ndimz] = tile[tx][ty + 2 * gap];
    }
    if (y + 3 * gap < ndimy) {
      out[index_out + 3 * gap * ndimx * ndimz] = tile[tx][ty + 3 * gap];
    }
  }
}

__global__ void gTranspose3dZYX2YXZAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE + 1];
  int x_in = 0;
  int y = 0;
  int z_in = 0;
  int x_out = 0;
  int z_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  z_in = ty + TILE_SIZE * by;
  y = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  z_out = tx + TILE_SIZE * by;
  index_in = x_in + (y + z_in * ndimy) * ndimx;
  index_out = z_out + (x_out + y * ndimx) * ndimz;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && z_in < ndimz) {
    tile[tx][ty] = in[index_in];
    if (z_in + gap < ndimz) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx * ndimy];
    }
    if (z_in + 2 * gap < ndimz) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx * ndimy];
    }
    if (z_in + 3 * gap < ndimz) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx * ndimy];
    }
  }

  __syncthreads();

  if (x_out < ndimx && z_out < ndimz) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimz] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimz] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimz] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gTranspose3dZYX2XZYAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE + 1];
  int x_in = 0;
  int y_in = 0;
  int z = 0;
  int x_out = 0;
  int y_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  y_in = ty + TILE_SIZE * by;
  z = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  y_out = tx + TILE_SIZE * by;
  index_in = x_in + (y_in + z * ndimy) * ndimx;
  index_out = y_out + (z + x_out * ndimz) * ndimy;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && y_in < ndimy) {
    tile[tx][ty] = in[index_in];
    if (y_in + gap < ndimy) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx];
    }
    if (y_in + 2 * gap < ndimy) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx];
    }
    if (y_in + 3 * gap < ndimy) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx];
    }
  }

  __syncthreads();

  if (y_out < ndimy && x_out < ndimx) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimy * ndimz] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimy * ndimz] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimy * ndimz] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gConcat3DY(float* out, const float* in1, const float* in2, int dimX, int dimY1, int dimY2, int dimZ) {
  int rows = dimX * (dimY1 + dimY2);
  int cols = dimZ;
  for (int bid = blockIdx.x; bid < rows; bid += gridDim.x) {
    int rowsX = bid / (dimY1 + dimY2);
    int rowsY = bid % (dimY1 + dimY2);
    float* rowOut = out + bid * cols;
    const float* rowsIn1 = in1 + rowsX * dimY1 * dimZ + rowsY * dimZ;
    const float* rowsIn2 = in2 + rowsX * dimY2 * dimZ + (rowsY - dimY1) * dimZ;
    for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
      if (rowsY < dimY1) {
        rowOut[tid] = rowsIn1[tid];
      } else {
        rowOut[tid] = rowsIn2[tid];
      }
    }
  }
}

__global__ void gSum(float* out, const float* in, int rows, int cols, int y) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int row_id = bid + blockIdx.x;
    if (row_id < rows) {
      float* out_1 = out + row_id * cols;
      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int col_id = tid + threadIdx.x;
        if (col_id < cols) {
          out_1[col_id] = 0;
          for (int head = 0; head < y; head++) {
            out_1[col_id] += in[row_id * y * cols + head * cols + col_id];
          }
        }
      }
    }
  }
  return;
}
