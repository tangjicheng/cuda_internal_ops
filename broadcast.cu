#include "broadcast.h"

#include <cstdio>
#include "cuda_params.h"
#include "cuda_runtime.h"
#include "cuda_utils.h"
#include "source/ndarray/ndarray.h"

namespace internal_ops {
using BroadcastFunctonPtr = float(float, float);

inline bool IsLeftVector(const std::vector<int>& left_shape_padded, const std::vector<int>& right_shape_padded) {
  if (left_shape_padded.size() != right_shape_padded.size()) {
    return false;
  }
  int dim = left_shape_padded.size();
  int cur_index = dim - 1;
  // 判断left_shape_padded右侧是否与right_shape_padded右侧相同
  while (cur_index >= 0) {
    if (left_shape_padded[cur_index] != right_shape_padded[cur_index]) {
      break;
    }
    cur_index--;
  }
  // 判断left_shape_padded左侧是否全为1
  while (cur_index >= 0) {
    if (left_shape_padded[cur_index] != 1) {
      return false;
    }
    cur_index--;
  }
  return true;
}

// case 1 : 输入的left和right的shape完全一致，不要需要进行stride，只需要对应的元素进行计算即可
template <BroadcastFunctonPtr func, int ThreadsPerBlock, int ElementsPerThread>
__global__ void broadcast_kernel_without_stride(float* output, const float* left, const float* right, int output_size,
                                                int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int this_thread_start_index = (blockIdx.x + bid) * ThreadsPerBlock * ElementsPerThread + threadIdx.x;
    int current_index = this_thread_start_index;
    float left_value_reg[ElementsPerThread];
    float right_value_reg[ElementsPerThread];

    // 将数据从显存中读取到寄存器，注意利用cuda连续thread读取连续数据的特性
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_index < output_size) {
        left_value_reg[i] = left[current_index];
        right_value_reg[i] = right[current_index];
      }
      current_index += ThreadsPerBlock;
    }

    current_index = this_thread_start_index;
    // 执行func操作
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_index < output_size) {
        output[current_index] = func(left_value_reg[i], right_value_reg[i]);
        current_index += ThreadsPerBlock;
      }
    }
  }
}

// case 2 : 二维矩阵加一维向量的特殊情况。相比通用情况，可以减少stride运算。
template <BroadcastFunctonPtr func, int ThreadsPerBlock, int ElementsPerThread>
__global__ void broadcast_kernel_left_is_vector(float* output, const float* left, const float* right, int output_size,
                                                int vector_size, int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int this_thread_start_index = (blockIdx.x + bid) * ThreadsPerBlock * ElementsPerThread + threadIdx.x;
    int current_index = this_thread_start_index;
    float left_value_reg[ElementsPerThread];
    float right_value_reg[ElementsPerThread];

    // 将数据从显存中读取到寄存器，注意利用cuda连续thread读取连续数据的特性
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_index < output_size) {
        int left_index = current_index % vector_size;
        left_value_reg[i] = left[left_index];
        right_value_reg[i] = right[current_index];
      }
      current_index += ThreadsPerBlock;
    }

    current_index = this_thread_start_index;
    // 执行func操作
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_index < output_size) {
        output[current_index] = func(left_value_reg[i], right_value_reg[i]);
        current_index += ThreadsPerBlock;
      }
    }
  }
}

template <BroadcastFunctonPtr func, int ThreadsPerBlock, int ElementsPerThread>
__global__ void broadcast_kernel_right_is_vector(float* output, const float* left, const float* right, int output_size,
                                                 int vector_size, int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int this_thread_start_index = (blockIdx.x + bid) * ThreadsPerBlock * ElementsPerThread + threadIdx.x;
    int current_index = this_thread_start_index;
    float left_value_reg[ElementsPerThread];
    float right_value_reg[ElementsPerThread];

    // 将数据从显存中读取到寄存器，注意利用cuda连续thread读取连续数据的特性
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_index < output_size) {
        int right_index = current_index % vector_size;
        left_value_reg[i] = left[current_index];
        right_value_reg[i] = right[right_index];
      }
      current_index += ThreadsPerBlock;
    }

    current_index = this_thread_start_index;
    // 执行func操作
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_index < output_size) {
        output[current_index] = func(left_value_reg[i], right_value_reg[i]);
        current_index += ThreadsPerBlock;
      }
    }
  }
}

// 通用的broadcast kernel，支持left_stride/right_stride
// 输入的left_stride_padded, right_stried_padded, output_stride，必须都是dim维的
// TODO: 使用fast_divmod代替int* output_stride，output_stride[k]需要频繁使用div和mod
template <BroadcastFunctonPtr func, int ThreadsPerBlock, int ElementsPerThread>
__global__ void broadcast_kernel_general(float* output, const float* left, const float* right, int output_size, int dim,
                                         int* left_stride_padded, int* right_stride_padded, int* output_stride,
                                         int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int this_thread_start_index = (blockIdx.x + bid) * ThreadsPerBlock * ElementsPerThread + threadIdx.x;
    float left_value_reg[ElementsPerThread];
    float right_value_reg[ElementsPerThread];
    int current_output_index = this_thread_start_index;

    // 取数
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      // 下面的循环for k : [0, dim) , 用来计算left_index/right_index
      // current_output_index/output_stride_fast/left_stride  ---> left_index
      // current_output_index/output_stride_fast/right_stride ---> right_index
      int left_index = 0;
      int right_index = 0;
      int offset = current_output_index;
#pragma unroll
      for (int k = 0; k < dim; k++) {
        int q, r;
        // output_stride_fast[k].divmod(offset, q, r);
        q = offset / output_stride[k];
        r = offset % output_stride[k];
        left_index += q * left_stride_padded[k];
        right_index += q * right_stride_padded[k];
        offset = r;
      }

      if (current_output_index < output_size) {
        left_value_reg[i] = left[left_index];
        right_value_reg[i] = right[right_index];
      }
      current_output_index += ThreadsPerBlock;
    }

    // 执行func操作
    current_output_index = this_thread_start_index;
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_output_index < output_size) {
        output[current_output_index] = func(left_value_reg[i], right_value_reg[i]);
      }
      current_output_index += ThreadsPerBlock;
    }
  }
}

template <BroadcastFunctonPtr func, int ThreadsPerBlock, int ElementsPerThread>
void BroadcastImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
                   const std::vector<int>& right_shape, cudaStream_t stream) {
  // 计算output_shape
  // 对齐left_shape和right_shape，使其长度与output_shape一致。开头补1
  std::vector<int> output_shape;
  std::vector<int> left_shape_padded;
  std::vector<int> right_shape_padded;
  int is_broadcast_success =
      broadcast_rule(left_shape, right_shape, output_shape, left_shape_padded, right_shape_padded);
  if (is_broadcast_success != 0) {
    printf("[Error] Broadcast rule failed!\n");
    return;
  }
  int dim = output_shape.size();
  int output_size = 1;
  for (auto iter : output_shape) {
    output_size *= iter;
  }

  // 计算cuda_kernel需要的block数量
  int elements_per_block = ThreadsPerBlock * ElementsPerThread;
  int required_blocks = output_size / elements_per_block + 1;
  int cuda_blocks = std::min(MAX_BLOCKS, required_blocks);

  // case 1: left_shape_padded == right_shape_padded，直接调用broadcast_kernel_without_stride
  bool is_left_right_shape_equal = true;
  for (int i = 0; i < dim; i++) {
    if (left_shape_padded[i] != right_shape_padded[i]) {
      is_left_right_shape_equal = false;
      break;
    }
  }
  if (is_left_right_shape_equal) {
    broadcast_kernel_without_stride<func, ThreadsPerBlock, ElementsPerThread>
        <<<cuda_blocks, ThreadsPerBlock, 0, stream>>>(output, left, right, output_size, required_blocks);
    return;
  }

  // case 2: left 或 right可以当做一个vector
  // 以left为例，如果left_shape=[3, 4], right_shape=[10, 3, 4]，那么left可以被当做一个vector [12],
  // right可以被当做一个matrix [10, 12] 如果left_shape=[1, 3, 4]，与上述情况相同，left也可以当做一个vector
  // 判断的依据是，left的右侧与right的右侧完全相同，左侧不存在或者全1
  // left_shape_padded 和 right_shape_padded长度已经完全相同，左侧不存在则补1
  bool is_left_vector = IsLeftVector(left_shape_padded, right_shape_padded);
  if (is_left_vector) {
    int left_size = 1;
    for (auto iter : left_shape_padded) {
      left_size *= iter;
    }
    broadcast_kernel_left_is_vector<func, ThreadsPerBlock, ElementsPerThread>
        <<<cuda_blocks, ThreadsPerBlock, 0, stream>>>(output, left, right, output_size, left_size, required_blocks);
    return;
  }

  // 判断right是否可以当做vector，只需要交换left_shape_padded和right_shape_padded
  bool is_right_vector = IsLeftVector(right_shape_padded, left_shape_padded);
  if (is_right_vector) {
    int right_size = 1;
    for (auto iter : right_shape_padded) {
      right_size *= iter;
    }
    broadcast_kernel_right_is_vector<func, ThreadsPerBlock, ElementsPerThread>
        <<<cuda_blocks, ThreadsPerBlock, 0, stream>>>(output, left, right, output_size, right_size, required_blocks);
    return;
  }

  // 计算left_stride_padded/right_stride_padded/output_stride
  std::vector<int> left_stride_padded = shape_to_stride(left_shape_padded);
  std::vector<int> right_stride_padded = shape_to_stride(right_shape_padded);
  std::vector<int> output_stride = shape_to_stride(output_shape);
  // the stride for broadcast dimension is kept as 0
  for (int i = 0; i < dim; i++) {
    if (left_shape_padded[i] != output_shape[i]) {
      left_stride_padded[i] = 0;
    }
    if (right_shape_padded[i] != output_shape[i]) {
      right_stride_padded[i] = 0;
    }
  }

  // 拷贝stride数据到显存dev_buf
  int* left_stride_buf = nullptr;
  int* right_stride_buf = nullptr;
  int* output_stride_buf = nullptr;
  GetBuf(left_stride_buf, left_stride_padded.size(), int, buf_1);
  GetBuf(right_stride_buf, right_stride_padded.size(), int, buf_2);
  GetBuf(output_stride_buf, output_stride.size(), int, buf_3);
  cudaMemcpyAsync(left_stride_buf, left_stride_padded.data(), sizeof(int) * left_stride_padded.size(),
             cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(right_stride_buf, right_stride_padded.data(), sizeof(int) * right_stride_padded.size(),
             cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(output_stride_buf, output_stride.data(), sizeof(int) * output_stride.size(), cudaMemcpyHostToDevice, stream);

  // 运算
  broadcast_kernel_general<func, ThreadsPerBlock, ElementsPerThread><<<cuda_blocks, ThreadsPerBlock, 0, stream>>>(
      output, left, right, output_size, dim, left_stride_buf, right_stride_buf, output_stride_buf, required_blocks);
  return;
}

void AddImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream) {
  BroadcastImpl<internal_ops_add, BROADCAST_THREADS_PER_BLOCK, BROADCAST_ELEMENTS_PER_THREAD>(output, left, right, left_shape,
                                                                                      right_shape, stream);
}

void SubImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream) {
  BroadcastImpl<internal_ops_sub, BROADCAST_THREADS_PER_BLOCK, BROADCAST_ELEMENTS_PER_THREAD>(output, left, right, left_shape,
                                                                                      right_shape, stream);
}

void MulImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream) {
  BroadcastImpl<internal_ops_mul, BROADCAST_THREADS_PER_BLOCK, BROADCAST_ELEMENTS_PER_THREAD>(output, left, right, left_shape,
                                                                                      right_shape, stream);
}

void DivImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream) {
  BroadcastImpl<internal_ops_div, BROADCAST_THREADS_PER_BLOCK, BROADCAST_ELEMENTS_PER_THREAD>(output, left, right, left_shape,
                                                                                      right_shape, stream);
}

void PowImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream) {
  BroadcastImpl<internal_ops_pow, BROADCAST_THREADS_PER_BLOCK, BROADCAST_ELEMENTS_PER_THREAD>(output, left, right, left_shape,
                                                                                      right_shape, stream);
}

void PReluImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
               const std::vector<int>& right_shape, cudaStream_t stream) {
  BroadcastImpl<internal_ops_prelu, BROADCAST_THREADS_PER_BLOCK, BROADCAST_ELEMENTS_PER_THREAD>(output, left, right, left_shape,
                                                                                        right_shape, stream);
}

}  // namespace internal_ops