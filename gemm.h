#pragma once

#include <cublas_v2.h>
#include <stdio.h>
#include <vector>
#include "source/ndarray/ndarray.h"

namespace internal_ops {

class Gemm {
 public:
  Gemm() { cublasCreate(&handle_); }

  ~Gemm() { cublasDestroy(handle_); }

  int Gemm2d(NDArray* C, const NDArray* A, const NDArray* B, bool transA = false, bool transB = false,
             float alpha = 1.0f, float beta = 0.0f, cudaStream_t* pCur_stream = NULL) {
    if (pCur_stream != NULL) {
      cublasSetStream(handle_, *pCur_stream);
    }

    int ret = 0;

    const TShape& A_shape = A->shape();
    const TShape& B_shape = B->shape();

    if (A_shape.ndim() != B_shape.ndim()) {
      if (A_shape.ndim() == 3 && B_shape.ndim() == 2) {
        std::vector<int> A_new_shape(2);
        A_new_shape[0] = A_shape[0] * A_shape[1];
        A_new_shape[1] = A_shape[2];
      } else {
        printf("Gemm error, dim mismatch\n");
        printf("First matrix dim:  %d\n", A_shape.ndim());
        printf("Second matrix dim: %d\n", B_shape.ndim());
        return -1;
      }
    }

    int dim_size = A_shape.ndim();
    int matrix_num = 1;
    for (int i = 0; i < dim_size - 2; i++) {
      matrix_num *= A_shape[i];
    }

    int m = A_shape[dim_size - 2];
    int k = A_shape[dim_size - 1];
    if (transA) {
      std::swap(m, k);
    }

    int l = B_shape[dim_size - 2];
    int n = B_shape[dim_size - 1];
    if (transB) {
      std::swap(l, n);
    }

    int lda = A_shape[dim_size - 1];
    int ldb = B_shape[dim_size - 1];
    int ldc = B_shape[dim_size - 1];
    if (transB) {
      ldc = B_shape[dim_size - 2];
    }

    std::vector<int> c_shape_vec;
    for (int i = 0; i < dim_size - 2; i++) {
      c_shape_vec.push_back(A_shape[i]);
    }
    c_shape_vec.push_back(m);
    c_shape_vec.push_back(n);

    TShape c_shape(c_shape_vec.begin(), c_shape_vec.end());
    C->resize(c_shape);

    // 这里进行的操作是C = alpha * A * B
    // cublas: C = alpha * A * B + beta * C
    // TODO:
    beta = 0.0f;

    // printf("C(%d, %d)=A(%d, %d) * B(%d, %d)\n", C.Rows(), C.Cols(), A.Rows(), A.Cols(), B.Rows(), B.Cols());

    int A_size = m * k;
    int B_size = l * n;
    int C_size = m * n;
    for (int i = 0; i < matrix_num; i++) {
      cublasOperation_t opA = transA ? CUBLAS_OP_T : CUBLAS_OP_N;
      cublasOperation_t opB = transB ? CUBLAS_OP_T : CUBLAS_OP_N;
#ifdef USE_TENSOR_CORE
      cublasGemmEx(handle_, opB, opA, n, m, k, &alpha, B->data() + i * B_size, CUDA_R_32F, ldb, A->data() + i * A_size,
                   CUDA_R_32F, lda, &beta, C->data() + i * C_size, CUDA_R_32F, ldc, CUDA_R_32F,
                   CUBLAS_GEMM_DEFAULT_TENSOR_OP);
#else
      cublasSgemm(handle_, opB, opA, n, m, k, &alpha, B->data() + i * B_size, ldb, A->data() + i * A_size, lda, &beta,
                  C->data() + i * C_size, ldc);
#endif
    }

    return ret;
  }

  int BatchedGemm2d(NDArray* C, const NDArray* A, const NDArray* B, int m, int n, int k, int batch_size,
                    cudaStream_t* pCur_stream = NULL) {
    if (pCur_stream != NULL) {
      cublasSetStream(handle_, *pCur_stream);
    }

    // TODO(tangjicheng): Check input shape
    // A: batch_size * m * k
    // B: batch_size * k * n
    // C: batch_size * m * n
    int lda = k;
    int ldb = n;
    int ldc = n;
    float alpha = 1.0f;
    float beta = 0.0f;
    const float* A_ptr = A->data();
    const float* B_ptr = B->data();
    float* C_ptr = C->data();
    int stride_A = m * k;
    int stride_B = k * n;
    int stride_C = m * n;

    cublasSgemmStridedBatched(handle_, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, B_ptr, ldb, stride_B, A_ptr, lda,
                              stride_A, &beta, C_ptr, ldc, stride_C, batch_size);
    return 0;
  }

 private:
  cublasHandle_t handle_;
};

}  // namespace internal_ops
