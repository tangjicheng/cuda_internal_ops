#pragma once

__global__ void gAbs(float* out, const float* in, int rows, int cols);

__global__ void gAcos(float* out, const float* in, int rows, int cols);

__global__ void gAcosh(float* out, const float* in, int rows, int cols);

__global__ void gAsin(float* out, const float* in, int rows, int cols);

__global__ void gAsinh(float* out, const float* in, int rows, int cols);

__global__ void gAtan(float* out, const float* in, int rows, int cols);

__global__ void gAtanh(float* out, const float* in, int rows, int cols);

__global__ void gAdd(float* out, const float* in1, const float* in2, int rows, int cols);
