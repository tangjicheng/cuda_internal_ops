#pragma once

#include <cuda_runtime.h>
#include <vector>
#include "enum.h"
#include "ndarray/ndarray.h"

namespace internal_ops {

int Transpose(NDArray* out, const NDArray* in, const std::vector<int>& perm, cudaStream_t stream = 0);

// broadcast类算子：Add/Sub/Mul/Div/Pow/PRelu
int AddFast(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int SubFast(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int MulFast(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int DivFast(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int PowFast(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int PRelu(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);

// 将输入的in，按照broadcast规则扩充为expand_shape形状的out。expand_shape和in->shape要满足broadcast规则。
int Expand(NDArray* out, NDArray* in, const TShape& expand_shape, cudaStream_t stream = 0);
int ExpandFast(NDArray* out, NDArray* in, NDArray* expand_shape, cudaStream_t stream = 0);

int Continuous(NDArray* out, const NDArray* in, cudaStream_t stream = 0);

int Range(NDArray* out, float start, float delta, int number_of_element, cudaStream_t stream = 0);

// activate
int Relu(NDArray* tensor, cudaStream_t stream = 0);
int Tanh(NDArray* tensor, cudaStream_t stream = 0);

// active with 1 param
int Elu(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream = 0);
int LeakyRelu(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream = 0);
int ThresholdedRelu(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream = 0);

// active with 2 param
int HardSigmoid(NDArray* out, const NDArray* in, float alpha, float beta, cudaStream_t stream = 0);
int Selu(NDArray* out, const NDArray* in, float alpha, float gamma, cudaStream_t stream = 0);

int Clip(NDArray* out, const NDArray* in, float min_value, float max_value, cudaStream_t stream = 0);

// pointwise : 1 input, 1 output
int Abs(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Acos(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Acosh(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Asin(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Asinh(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Atan(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Atanh(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Ceil(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Cos(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Cosh(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Erf(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Exp(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Floor(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Identity(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Log(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Neg(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Sign(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Sin(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Sinh(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Sqrt(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Tanh(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Tan(NDArray* out, const NDArray* in, cudaStream_t stream = 0);

int Sigmoid(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Softplus(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
int Softsign(NDArray* out, const NDArray* in, cudaStream_t stream = 0);

// pointwise
int ElementAdd(NDArray* out, float scale, cudaStream_t stream = 0);
int ElementMul(NDArray* out, float scale, cudaStream_t stream = 0);
int ElementSub(NDArray* out, float scale, cudaStream_t stream = 0);
int ElementSub2(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream = 0);
int ElementAdd(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream = 0);
int ElementMul(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream = 0);
int ElementSub(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream = 0);
int ElementAdd(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int ElementMul(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);
int ElementSub(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream = 0);

int BroadcastAdd(NDArray* out, const NDArray* in1, cudaStream_t stream = 0);
int BroadcastAddNCHW(NDArray* out, const NDArray* in1, cudaStream_t stream = 0);

int ElementBroadcast(NDArray* out, const std::vector<NDArray*>& in, enum Symbol s, cudaStream_t stream = 0);

int CopyRows(NDArray& Out, const NDArray& In, size_t* dst_ids, size_t* src_ids, size_t nums, cudaStream_t stream = 0);
int CopyRows(NDArray& Out, const NDArray& In, const std::vector<size_t>& dst_ids, const std::vector<size_t>& src_ids,
             cudaStream_t stream = 0);
int Concat(NDArray* out, const std::vector<NDArray*>& in, int axis = 0, cudaStream_t stream = 0);
int ConcatFast(NDArray* out, const std::vector<NDArray*>& in, int axis = 0, cudaStream_t stream = 0);

int LayerNorm(NDArray* out, const NDArray* in, const NDArray* scales, const NDArray* biases, cudaStream_t stream = 0);

int Topk(NDArray* out_value, NDArray* out_index, const NDArray* in, int32_t k, int32_t axis, int32_t largest,
         int32_t sorted, cudaStream_t stream = 0);

int TopkFast(NDArray* out_value, NDArray* out_index, const NDArray* in, int32_t k, int32_t axis, int32_t largest,
         int32_t sorted, cudaStream_t stream = 0);

int Gather(NDArray* out, NDArray* data, NDArray* indices, int32_t axis, cudaStream_t stream = 0);
int GatherFast(NDArray* out, NDArray* data, NDArray* indices, int32_t axis, cudaStream_t stream = 0);
int GatherElements(NDArray* out, NDArray* data, NDArray* indices, int32_t axis, cudaStream_t stream = 0);

int Slice(NDArray* out, const NDArray* in, const NDArray* axes, const NDArray* starts, const NDArray* ends,
          const NDArray* steps, cudaStream_t stream = 0);
int SliceCopy(NDArray& Out, const NDArray& In, size_t in_row_begin, size_t in_col_begin, size_t out_row_len,
              size_t out_col_len, cudaStream_t stream = 0);

int Reduce(NDArray* out, const NDArray* data, const std::vector<int32_t>& axes, enum ReduceType,
           cudaStream_t stream = 0);

int SetValue(NDArray* out, float value, cudaStream_t stream = 0);

namespace glue {
int Shape(NDArray* out, const NDArray* in, cudaStream_t stream = 0);

int Shape(NDArray* out, const NDArray* in, int start, int end, cudaStream_t stream = 0);
}  // namespace glue

namespace cv {
int GlobalAveragePool(NDArray* out, const NDArray* in, cudaStream_t stream = 0);
}

}  // namespace internal_ops
