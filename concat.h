// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Concat算子的实现
#pragma once

#include <vector>

#include <cuda_runtime.h>

namespace internal_ops {
int ConcatImpl(float* output, const std::vector<int64_t>& input, const std::vector<std::vector<int>>& input_shape_vec,
               int axis, cudaStream_t stream);
}