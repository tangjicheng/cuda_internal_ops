#pragma once

#include <vector>
#include <cuda_runtime.h>

#include "source/ndarray/ndarray.h"

namespace internal_ops {
int TransposeImpl(NDArray* out, const NDArray* in, const std::vector<int>& perm, cudaStream_t stream);
}
