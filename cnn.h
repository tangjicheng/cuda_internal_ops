#pragma once

#include <stdio.h>
#include <vector>
#include "cudnn.h"
#include "ndarray/ndarray.h"
#include "utils.h"

namespace internal_ops {

class Cnn {
 public:
  Cnn() : work_space_(NULL), work_size_(0) {
    algo_ = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
    find_algo_ = false;
  }

  ~Cnn() {
    Fini();
    if (work_space_ != NULL) {
      mempool::MemPoolGpu::get()->subref(work_space_);
      mempool::MemPoolGpu::get()->fastFree(work_space_);
    }
    work_space_ = NULL;
    work_size_ = 0;
  }

  int Fini() {
    int ret = 0;
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(xDesc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(yDesc_));
    CHECK_CUDNN(cudnnDestroyFilterDescriptor(wDesc_));
    CHECK_CUDNN(cudnnDestroyConvolutionDescriptor(convDesc_));
    CHECK_CUDNN(cudnnDestroy(handle_));
    return ret;
  }

  int Init(const NDArray& w) {
    int ret = 0;
    w_ = &w;
    CHECK_CUDNN(cudnnCreate(&handle_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&xDesc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&yDesc_));
    CHECK_CUDNN(cudnnCreateFilterDescriptor(&wDesc_));
    CHECK_CUDNN(cudnnCreateConvolutionDescriptor(&convDesc_));

    return ret;
  }

  int Conv2d(NDArray* y, const NDArray* x, int stride_h = 1, int stride_w = 1, int pad_h = 0, int pad_w = 0) {
    int ret = 0;

    const TShape& tx = x->shape();
    const TShape& tw = w_->shape();

    if (tx.ndim() != 4 || tw.ndim() != 4) {
      printf("dim error! x:%d w:%d\n", tx.ndim(), tw.ndim());
    }

    int dim = 4;
    int xShape[4] = {(int)tx[0], (int)tx[1], (int)tx[2], (int)tx[3]};
    int yShape[4] = {(int)tx[0], (int)tw[0], (int)(tx[2] + 2 * pad_h - tw[2]) / stride_h + 1,
                     (int)(tx[3] + 2 * pad_w - tw[3]) / stride_w + 1};
    int wShape[4] = {(int)tw[0], (int)tw[1], (int)tw[2], (int)tw[3]};

    const float* pin = x->data();

    int xStride[4] = {xShape[1] * xShape[2] * xShape[3], xShape[2] * xShape[3], xShape[3], 1};
    int yStride[4] = {yShape[1] * yShape[2] * yShape[3], yShape[2] * yShape[3], yShape[3], 1};
    std::vector<int> ys_vec = {(int)yShape[0], (int)yShape[1], (int)yShape[2], (int)yShape[3]};
    TShape ys(ys_vec.begin(), ys_vec.end());

    // y->AutoResize(ys);

    CHECK_CUDNN(cudnnSetTensorNdDescriptor(xDesc_, CUDNN_DATA_FLOAT, dim, xShape, xStride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(yDesc_, CUDNN_DATA_FLOAT, dim, yShape, yStride));
    CHECK_CUDNN(cudnnSetFilterNdDescriptor(wDesc_, CUDNN_DATA_FLOAT, CUDNN_TENSOR_NCHW, dim, wShape));

    int arr_len = 2;
    int pad[2] = {pad_h, pad_w};
    int fstride[2] = {stride_h, stride_w};
    int upscale[2] = {1, 1};
    CHECK_CUDNN(cudnnSetConvolutionNdDescriptor(convDesc_, arr_len, pad, fstride, upscale, CUDNN_CROSS_CORRELATION,
                                                CUDNN_DATA_FLOAT));
    // CHECK_CUDNN(cudnnSetConvolution2dDescriptor(convDesc_, 0, 0, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION,
    // CUDNN_DATA_FLOAT));
    /*
    #ifdef USE_TENSOR_CORE
            CHECK_CUDNN(cudnnSetConvolutionMathType(convDesc_, CUDNN_TENSOR_OP_MATH));
            cudnnConvolutionFwdAlgo_t algo = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM;
    #else
            cudnnConvolutionFwdAlgo_t algo = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
    #endif
    */

    if (!find_algo_) {
      int requestedAlgoCount = 1;
      int returnedAlgoCount = 0;
      cudnnConvolutionFwdAlgoPerf_t perfResults;
      CHECK_CUDNN(cudnnFindConvolutionForwardAlgorithm(handle_, xDesc_, wDesc_, convDesc_, yDesc_, requestedAlgoCount,
                                                       &returnedAlgoCount, &perfResults));

      /*
                  printf("returnedAlgoCount:%d\n", returnedAlgoCount);
                  printf("algo:%d\n", perfResults.algo);
                  printf("time:%f\n", perfResults.time);
                  printf("mathType:%d\n", perfResults.mathType);
      */

      algo_ = perfResults.algo;
      find_algo_ = true;
    }

    size_t work_size = 0;
    CHECK_CUDNN(cudnnGetConvolutionForwardWorkspaceSize(handle_, xDesc_, wDesc_, convDesc_, yDesc_, algo_, &work_size));

    if (work_size > work_size_) {
      if (work_space_ != NULL) {
        mempool::MemPoolGpu::get()->subref(work_space_);
        mempool::MemPoolGpu::get()->fastFree(work_space_);
      }
      work_size_ = work_size;
      work_space_ = (float*)mempool::MemPoolGpu::get()->fastMalloc(work_size_ * sizeof(float));
      mempool::MemPoolGpu::get()->addref(work_space_);
    }

    // printf("work size:%f G bytes\n", work_size_*1.0f/1024/1024/1024);
    float alpha = 1.0f;
    float beta = 0.0f;

    CHECK_CUDNN(cudnnConvolutionForward(handle_, &alpha, xDesc_, pin, wDesc_, w_->data(), convDesc_, algo_, work_space_,
                                        work_size, &beta, yDesc_, y->data()));

    return ret;
  }

 private:
  void* work_space_;
  int32_t work_size_;

  cudnnHandle_t handle_;
  cudnnTensorDescriptor_t xDesc_;
  cudnnTensorDescriptor_t yDesc_;
  cudnnFilterDescriptor_t wDesc_;
  cudnnConvolutionDescriptor_t convDesc_;

  const NDArray* w_;

  cudnnConvolutionFwdAlgo_t algo_;
  bool find_algo_;
};

}  // namespace internal_ops
