// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// cuda常用的工具函数

#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <vector>

#include <cuda_runtime.h>

namespace internal_ops {

#define GetBuf(ptr, size, type, name)                      \
  TShape name##_shape__(1);                                \
  name##_shape__[0] = size * sizeof(type) / sizeof(float); \
  NDArray name##_ndarray__(name##_shape__);                \
  ptr = reinterpret_cast<type*>(name##_ndarray__.data())

// 取余函数，用法：
// 输入除数d，定义一个fast_divmod对象，
// 调用void divmod(int n, int& q, int& r)，获得 q = n / 5，r = n % 5
// 例如，
// fast_divmod my_div(5);
// int n = 11, q = 0, r = 0;
// my_div.divmod(n, q, r); // q = (11 / 5) = 2,  r = (11 % 5) = 1.
//
// Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved
// Licensed under the MIT license. See LICENSE.md file in the project root for full license information.
//
// The code below is based on section 4 Unsigned division of paper https://gmplib.org/~tege/divcnst-pldi94.pdf
// In current ORT, fast_divmod is used for calculating the position of a element in tensor,
// so unsigned integer division from the paper is good enough for ORT. The advantage is that div is very simple,
// then GPU compiler can do loop unroll easilly when divmod is called in a loop.
struct fast_divmod {
  fast_divmod(int d = 1) {
    d_ = (d == 0) ? 1 : d;
    // ORT_ENFORCE(d_ >= 1 && d_ <= static_cast<uint32_t>(std::numeric_limits<int>::max()));

    for (l_ = 0; l_ < 32; l_++)
      if ((1U << l_) >= d_) break;

    uint64_t one = 1;
    uint64_t m = ((one << 32) * ((one << l_) - d_)) / d_ + 1;
    M_ = static_cast<uint32_t>(m);
    // according to paper, the value of m' should fit in a unsigned integer.
    // ORT_ENFORCE(M_ > 0 && M_ == m);
  }

  __host__ __device__ inline int div(int n) const {
#if defined(__CUDA_ARCH__)
    uint32_t t = __umulhi(M_, n);
    return (t + n) >> l_;
#else
    // Using uint64_t for t, then t + n won't overflow.
    uint64_t t = ((uint64_t)M_ * n) >> 32;
    return static_cast<int>((t + n) >> l_);
#endif
  }

  __host__ __device__ inline int mod(int n) const { return n - div(n) * d_; }

  __host__ __device__ inline void divmod(int n, int& q, int& r) const {
    q = div(n);
    r = n - q * d_;
  }

  uint32_t d_;  // divisor
  uint32_t M_;  // m' in the paper.
  uint32_t l_;  // l_ = ceil(log2(d_))
};

// 激活函数类型运算
__forceinline__ __device__ float internal_ops_sigmoid(float x) { return (1.0f / (1.0f + expf(-x))); }
__forceinline__ __device__ float internal_ops_softplus(float x) { return (logf(expf(x) + 1.0f)); }
__forceinline__ __device__ float internal_ops_softsign(float x) { return x / (1.0f + fabsf(x)); }

// 简单算术运算
__forceinline__ __device__ float internal_ops_identity(float x) { return x; }

__forceinline__ __device__ float internal_ops_neg(float x) { return (-x); }

__forceinline__ __device__ float internal_ops_sign(float x) {
  if (x > 0.0f) {
    return 1.0f;
  }
  if (x < 0.0f) {
    return -1.0f;
  }
  return 0.0f;
}

// 加减乘除等二元算术运算
__forceinline__ __device__ float internal_ops_add(float x, float y) { return (x + y); }

__forceinline__ __device__ float internal_ops_sub(float x, float y) { return (x - y); }

__forceinline__ __device__ float internal_ops_mul(float x, float y) { return (x * y); }

__forceinline__ __device__ float internal_ops_div(float x, float y) { return y == 0.0f ? 0.0f : x / y; }

__forceinline__ __device__ float internal_ops_pow(float x, float y) { return (powf(x, y)); }

__forceinline__ __device__ float internal_ops_prelu(float x, float slope) { return (x > 0.0f ? x : x * slope); }

// left_shape & right_shape ----> output_shape
// sucess: return 0, fail: return 1
inline int broadcast_rule(const std::vector<int>& left_shape, const std::vector<int>& right_shape,
                          std::vector<int>& output_shape, std::vector<int>& left_shape_padded,
                          std::vector<int>& right_shape_padded) {
  int ret = 0;
  if (left_shape.empty() && right_shape.empty()) {
    output_shape.clear();
    return ret;
  }
  int left_dim = left_shape.size();
  int right_dim = right_shape.size();
  int max_dim = std::max(left_dim, right_dim);

  // left_shape, right_shape都扩展为max_dim维度，靠右对齐，左侧补1
  left_shape_padded.clear();
  right_shape_padded.clear();
  left_shape_padded.resize(max_dim, 1);
  right_shape_padded.resize(max_dim, 1);
  for (int i = 0; i < left_dim; i++) {
    left_shape_padded[max_dim - 1 - i] = left_shape[left_dim - 1 - i];
  }
  for (int i = 0; i < right_dim; i++) {
    right_shape_padded[max_dim - 1 - i] = right_shape[right_dim - 1 - i];
  }

  // set output_shape && check broadcast rule
  output_shape.resize(max_dim);
  for (int i = 0; i < max_dim; i++) {
    output_shape[i] = std::max(left_shape_padded[i], right_shape_padded[i]);
    if (left_shape_padded[i] != right_shape_padded[i] && left_shape_padded[i] != 1 && right_shape_padded[i] != 1) {
      ret = 1;
    }
  }

  return ret;
}

inline std::vector<int> shape_to_stride(const std::vector<int>& shape) {
  std::vector<int> stride;
  if (shape.empty()) {
    return stride;
  }
  int dim = shape.size();
  stride.resize(dim, 1);
  for (int i = dim - 2; i >= 0; i--) {
    stride[i] = stride[i + 1] * shape[i + 1];
  }
  return stride;
}

}  // namespace internal_ops