// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Gather算子的实现

#include "cuda_params.h"
#include "cuda_utils.h"
#include "source/operator/internal/gather.h"

#include <cuda_runtime.h>

#define USE_FASTDIVMOD 1

namespace internal_ops {

__global__ void gather_kernel(float* output_data, float* input_data, float* indices_data, int block_size,
                              int input_block_size, int output_block_size, int max_indices_val, int max_output_index,
                              int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    for (int tid = 0; tid < cols; tid += blockDim.x) {
      int output_index = (blockIdx.x + bid) * cols + threadIdx.x + tid;
      if (output_index >= max_output_index) {
        return;
      }

      // output_index / output_block_size  --> block_id, output_block_offset
      // output_block_offset / block_size  --> indices_id, block_offset
      // indices_id, indices_data          --> input_axis_id
      // input_index = block_id * input_block_size + input_axis_id * block_size + block_offset;

      int block_id = output_index / output_block_size;
      int output_block_offset = output_index % output_block_size;
      int indices_id = output_block_offset / block_size;
      int block_offset = output_block_offset % block_size;

      int input_axis_id = (int)indices_data[indices_id];
      if (input_axis_id >= max_indices_val) {
        output_data[output_index] = 0.0f;
        return;
      }

      int input_index = block_id * input_block_size + input_axis_id * block_size + block_offset;
      output_data[output_index] = input_data[input_index];
    }
  }
  return;
}

__global__ void gather_kernel_with_fastdivmod(float* output_data, float* input_data, float* indices_data,
                                              const fast_divmod block_size_fast, int input_block_size,
                                              const fast_divmod output_block_size_fast, int max_indices_val,
                                              int max_output_index, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    for (int tid = 0; tid < cols; tid += blockDim.x) {
      int output_index = (blockIdx.x + bid) * cols + threadIdx.x + tid;
      if (output_index >= max_output_index) {
        return;
      }

      int block_id;
      int output_block_offset;
      output_block_size_fast.divmod(output_index, block_id, output_block_offset);

      int indices_id;
      int block_offset;
      block_size_fast.divmod(output_block_offset, indices_id, block_offset);

      int input_axis_id = (int)indices_data[indices_id];
      if (input_axis_id >= max_indices_val) {
        output_data[output_index] = 0.0f;
        return;
      }

      int input_index = block_id * input_block_size + input_axis_id * block_size_fast.d_ + block_offset;
      output_data[output_index] = input_data[input_index];
    }
  }
  return;
}

int GatherImpl(float* output_data, float* input_data, float* indices_data, const std::vector<int>& input_shape,
               const std::vector<int> indices_shape, int axis, cudaStream_t stream) {
  int ret = 0;

  // 计算block_size, input_block_size, output_block_size
  // block_size，从axis后一个维度开始，到最后一个维度的元素数量
  // input_block_size，input_shape从axis维度开始，到最后一个维度的元素数量
  // output_block_size，output_shape从indices的第1个维度开始，到最后一个维度的元素数量
  // 这里的block指的是数据上连续的一块，与cuda的线程块block没有关系
  int max_indices_val = input_shape[axis];
  int block_size = 1;
  for (int i = axis + 1; i < input_shape.size(); i++) {
    block_size *= input_shape[i];
  }
  int input_block_size = max_indices_val * block_size;
  int indices_size = 1;
  for (int i = 0; i < indices_shape.size(); i++) {
    indices_size *= indices_shape[i];
  }
  int output_block_size = indices_size * block_size;
  int num_of_block = 1;
  for (int i = 0; i < axis; i++) {
    num_of_block *= input_shape[i];
  }
  int max_output_index = num_of_block * output_block_size;

  int rows = num_of_block;
  int cols = output_block_size;

  if (cols > MAX_THREADS * 10 && rows < 10) {
    rows = output_block_size / MAX_THREADS + 1;
    cols = MAX_THREADS;
  }

  int blocks_per_grid = std::min(rows, MAX_BLOCKS);
  int threads_per_block = std::min(cols, MAX_THREADS);

#ifdef USE_FASTDIVMOD
  const fast_divmod block_size_fast(block_size);
  const fast_divmod output_block_size_fast(output_block_size);
  gather_kernel_with_fastdivmod<<<blocks_per_grid, threads_per_block, 0, stream>>>(
      output_data, input_data, indices_data, block_size_fast, input_block_size, output_block_size_fast, max_indices_val,
      max_output_index, rows, cols);
#else
  gather_kernel<<<blocks_per_grid, threads_per_block, 0, stream>>>(output_data, input_data, indices_data, block_size,
                                                                   input_block_size, output_block_size, max_indices_val,
                                                                   max_output_index, rows, cols);
#endif

  return ret;
}

}  // namespace internal_ops
