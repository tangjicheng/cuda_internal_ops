#include "source/operator/internal/activation.h"
namespace internal_ops {

__global__ void gRelu(float* out, int size) {
  for (int bid = blockIdx.x; (bid * blockDim.x) < size; bid += gridDim.x) {
    int idx = bid * blockDim.x + threadIdx.x;
    if (idx < size) {
      if (out[idx] < 0) {
        out[idx] = 0.0f;
      }
    }
  }
  return;
}

__global__ void gTanh(float* out, int size) {
  for (int bid = blockIdx.x; (bid * blockDim.x) < size; bid += gridDim.x) {
    int idx = bid * blockDim.x + threadIdx.x;
    if (idx < size) {
      out[idx] = tanhf(out[idx]);
    }
  }
  return;
}

__global__ void gGelu(float* out, int size) {
  for (int bid = blockIdx.x; (bid * blockDim.x) < size; bid += gridDim.x) {
    int idx = bid * blockDim.x + threadIdx.x;
    if (idx < size) {
      float val = out[idx];
      out[idx] = val * 0.5f * (1.0f + erff(val * rsqrtf(2.0f)));
    }
  }
  return;
}

__global__ void gLog(float* out, int size) {
  for (int bid = blockIdx.x; (bid * blockDim.x) < size; bid += gridDim.x) {
    int idx = bid * blockDim.x + threadIdx.x;
    if (idx < size) {
      out[idx] = logf(out[idx]);
    }
  }
  return;
}

template <>
int Activation1<internal_ops_elu>(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream);

template <>
int Activation1<internal_ops_leaky_relu>(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream);

template <>
int Activation1<internal_ops_thresholded_relu>(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream);

template <>
int Activation2<internal_ops_hard_sigmoid>(NDArray* out, const NDArray* in, float alpha, float beta, cudaStream_t stream);

template <>
int Activation2<internal_ops_selu>(NDArray* out, const NDArray* in, float alpha, float beta, cudaStream_t stream);

template <>
int Activation2<internal_ops_clip>(NDArray* out, const NDArray* in, float max_value, float min_value, cudaStream_t stream);

}  // namespace internal_ops