#include "transpose.h"

#include <cstdio>

#include "cuda_params.h"
#include "cuda_utils.h"

namespace internal_ops {

// Transpose 3D kernel实现

__global__ void gTranspose3D021(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE];
  int x_in = 0;
  int y_in = 0;
  int z = 0;
  int x_out = 0;
  int y_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  y_in = ty + TILE_SIZE * by;
  z = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  y_out = tx + TILE_SIZE * by;
  index_in = x_in + (y_in + z * ndimy) * ndimx;
  index_out = y_out + (x_out + z * ndimx) * ndimy;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && y_in < ndimy) {
    tile[tx][ty] = in[index_in];
    if (y_in + gap < ndimy) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx];
    }
    if (y_in + 2 * gap < ndimy) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx];
    }
    if (y_in + 3 * gap < ndimy) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx];
    }
  }

  __syncthreads();

  if (x_out < ndimx && y_out < ndimy) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimy] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimy] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimy] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gTranspose3D210(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE];
  int x_in = 0;
  int y = 0;
  int z_in = 0;
  int x_out = 0;
  int z_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  z_in = ty + TILE_SIZE * by;
  y = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  z_out = tx + TILE_SIZE * by;
  index_in = x_in + (y + z_in * ndimy) * ndimx;
  index_out = z_out + (y + x_out * ndimy) * ndimz;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && z_in < ndimz) {
    tile[tx][ty] = in[index_in];
    if (z_in + gap < ndimz) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx * ndimy];
    }
    if (z_in + 2 * gap < ndimz) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx * ndimy];
    }
    if (z_in + 3 * gap < ndimz) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx * ndimy];
    }
  }

  __syncthreads();

  if (z_out < ndimz && x_out < ndimx) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimz * ndimy] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimz * ndimy] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimz * ndimy] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gTranspose3D102(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE + 1];
  int x = 0;
  int y = 0;
  int z = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x = tx + TILE_SIZE * bx;
  y = ty + TILE_SIZE * by;
  z = blockIdx.z;

  index_in = x + (y + z * ndimy) * ndimx;
  index_out = x + (z + y * ndimz) * ndimx;

  int gap = TILE_SIZE / 4;

  if (x < ndimx && y < ndimy) {
    tile[tx][ty] = in[index_in];
    if (y + gap < ndimy) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx];
    }
    if (y + 2 * gap < ndimy) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx];
    }
    if (y + 3 * gap < ndimy) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx];
    }
  }

  __syncthreads();

  if (x < ndimx && y < ndimy) {
    out[index_out] = tile[tx][ty];
    if (y + gap < ndimy) {
      out[index_out + gap * ndimx * ndimz] = tile[tx][ty + gap];
    }
    if (y + 2 * gap < ndimy) {
      out[index_out + 2 * gap * ndimx * ndimz] = tile[tx][ty + 2 * gap];
    }
    if (y + 3 * gap < ndimy) {
      out[index_out + 3 * gap * ndimx * ndimz] = tile[tx][ty + 3 * gap];
    }
  }
}

__global__ void gTranspose3D120(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE + 1];
  int x_in = 0;
  int y = 0;
  int z_in = 0;
  int x_out = 0;
  int z_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  z_in = ty + TILE_SIZE * by;
  y = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  z_out = tx + TILE_SIZE * by;
  index_in = x_in + (y + z_in * ndimy) * ndimx;
  index_out = z_out + (x_out + y * ndimx) * ndimz;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && z_in < ndimz) {
    tile[tx][ty] = in[index_in];
    if (z_in + gap < ndimz) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx * ndimy];
    }
    if (z_in + 2 * gap < ndimz) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx * ndimy];
    }
    if (z_in + 3 * gap < ndimz) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx * ndimy];
    }
  }

  __syncthreads();

  if (x_out < ndimx && z_out < ndimz) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimz] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimz] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimz] = tile[ty + 3 * gap][tx];
    }
  }
}

__global__ void gTranspose3D201(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx) {
  const int TILE_SIZE = 64;
  __shared__ float tile[TILE_SIZE][TILE_SIZE + 1];
  int x_in = 0;
  int y_in = 0;
  int z = 0;
  int x_out = 0;
  int y_out = 0;
  int index_in = 0;
  int index_out = 0;
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;

  x_in = tx + TILE_SIZE * bx;
  y_in = ty + TILE_SIZE * by;
  z = blockIdx.z;
  x_out = ty + TILE_SIZE * bx;
  y_out = tx + TILE_SIZE * by;
  index_in = x_in + (y_in + z * ndimy) * ndimx;
  index_out = y_out + (z + x_out * ndimz) * ndimy;

  int gap = TILE_SIZE / 4;
  if (x_in < ndimx && y_in < ndimy) {
    tile[tx][ty] = in[index_in];
    if (y_in + gap < ndimy) {
      tile[tx][ty + gap] = in[index_in + gap * ndimx];
    }
    if (y_in + 2 * gap < ndimy) {
      tile[tx][ty + 2 * gap] = in[index_in + 2 * gap * ndimx];
    }
    if (y_in + 3 * gap < ndimy) {
      tile[tx][ty + 3 * gap] = in[index_in + 3 * gap * ndimx];
    }
  }

  __syncthreads();

  if (y_out < ndimy && x_out < ndimx) {
    out[index_out] = tile[ty][tx];
    if (x_out + gap < ndimx) {
      out[index_out + gap * ndimy * ndimz] = tile[ty + gap][tx];
    }
    if (x_out + 2 * gap < ndimx) {
      out[index_out + 2 * gap * ndimy * ndimz] = tile[ty + 2 * gap][tx];
    }
    if (x_out + 3 * gap < ndimx) {
      out[index_out + 3 * gap * ndimy * ndimz] = tile[ty + 3 * gap][tx];
    }
  }
}

int Transpose3D(NDArray* out, const NDArray* in, const std::vector<int>& index, cudaStream_t stream) {
  int ret = 0;
  std::vector<int> in_shape = in->shape().to_vector();
  std::vector<int> out_shape = out->shape().to_vector();
  int data_size = in->size();

  const int ELEMENT_PER_THREAD = 4;
  const int TILE_SIZE = 64;
  dim3 block_size(TILE_SIZE, TILE_SIZE / ELEMENT_PER_THREAD);
  int ndimx = in->shape()[2];
  int ndimy = in->shape()[1];
  int ndimz = in->shape()[0];
  int grid_size_x = ndimx % TILE_SIZE == 0 ? ndimx / TILE_SIZE : ndimx / TILE_SIZE + 1;
  int grid_size_y = ndimy % TILE_SIZE == 0 ? ndimy / TILE_SIZE : ndimy / TILE_SIZE + 1;
  int grid_size_z = ndimz % TILE_SIZE == 0 ? ndimz / TILE_SIZE : ndimz / TILE_SIZE + 1;

  dim3 grid_size_axis_z(grid_size_x, grid_size_y, ndimz);
  dim3 grid_size_axis_y(grid_size_x, grid_size_z, ndimy);

  if (index[0] == 1 && index[1] == 0 && index[2] == 2) {
    gTranspose3D102<<<grid_size_axis_z, block_size, 0, stream>>>(out->data(), in->data(), in->shape()[0],
                                                                          in->shape()[1], in->shape()[2]);
  } else if (index[0] == 1 && index[1] == 2 && index[2] == 0) {
    gTranspose3D120<<<grid_size_axis_y, block_size, 0, stream>>>(out->data(), in->data(), in->shape()[0],
                                                                          in->shape()[1], in->shape()[2]);
  } else if (index[0] == 0 && index[1] == 2 && index[2] == 1) {
    gTranspose3D021<<<grid_size_axis_z, block_size, 0, stream>>>(out->data(), in->data(), in->shape()[0],
                                                                          in->shape()[1], in->shape()[2]);
  } else if (index[0] == 0 && index[1] == 1 && index[2] == 2) {
    out->copy_from_gpu_to_gpu(*in, stream);
    printf("[Transpose3D] index[0] == 0 && index[1] == 1 && index[2] == 2\n");
  } else if (index[0] == 2 && index[1] == 0 && index[2] == 1) {
    gTranspose3D201<<<grid_size_axis_z, block_size, 0, stream>>>(out->data(), in->data(), in->shape()[0],
                                                                          in->shape()[1], in->shape()[2]);
  } else if (index[0] == 2 && index[1] == 1 && index[2] == 0) {
    gTranspose3D210<<<grid_size_axis_y, block_size, 0, stream>>>(out->data(), in->data(), in->shape()[0],
                                                                          in->shape()[1], in->shape()[2]);
  } else {
    printf("[Error] Transpose3d | transpose3d index is invalid ! | index is %d %d %d \n", index[0], index[1], index[2]);
  }
  return ret;
}

// 通用情况下的transpose kernel实现
__global__ void gTransposeGeneral(float* output, const float* input, int rank, int* input_strides, int* output_strides,
                                  int data_size) {
  int output_index = blockDim.x * blockIdx.x + threadIdx.x;
  int input_index = 0;
  if (output_index > data_size) {
    return;
  }
  int q = 0;
  int r = 0;
  int output_index_r = output_index;
#pragma unroll
  for (int dim = 0; dim < rank; dim++) {
    q = output_index_r / output_strides[dim];
    r = output_index_r % output_strides[dim];
    output_index_r = r;
    input_index += input_strides[dim] * q;
  }
  output[output_index] = input[input_index];
  return;
}

int TransposeImpl(NDArray* out, const NDArray* in, const std::vector<int>& perm, cudaStream_t stream) {
  auto input_shape = in->shape().to_vector();
  auto output_shape = out->shape().to_vector();
  int rank = input_shape.size();
  if (rank == 3) {
    return Transpose3D(out, in, perm, stream);
  }
  int data_size = in->size();
  int output_size = 1;
  for (int i = 0; i < rank; i++) {
    output_size *= output_shape[i];
  }
  if (output_size != data_size) {
    printf("[Error] Transpose Op output.size != in.size\n");
    return -1;
  }

  std::vector<int> input_strides = shape_to_stride(input_shape);
  std::vector<int> output_strides = shape_to_stride(output_shape);
  std::vector<int> input_strides_trans(input_strides.size(), 0);
  for (int i = 0; i < input_strides.size(); i++) {
    input_strides_trans[i] = input_strides[perm[i]];
  }

  int* input_strides_trans_gpu = nullptr;
  int* output_strides_gpu = nullptr;
  GetBuf(input_strides_trans_gpu, input_strides_trans.size(), int, buf_1);
  GetBuf(output_strides_gpu, output_strides.size(), int, buf_2);

  cudaMemcpyAsync(input_strides_trans_gpu, input_strides_trans.data(), input_strides_trans.size() * sizeof(int),
             cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(output_strides_gpu, output_strides.data(), output_strides.size() * sizeof(int), cudaMemcpyHostToDevice, stream);

  int threads = MAX_THREADS;
  int blocks = data_size % threads == 0 ? data_size / threads : data_size / threads + 1;
  gTransposeGeneral<<<blocks, threads, 0, stream>>>(out->data(), in->data(), rank, input_strides_trans_gpu,
                                                    output_strides_gpu, data_size);

  return 0;
}
}  // namespace internal_ops