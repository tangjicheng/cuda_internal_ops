// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// topk算子的实现
// 目前包含了：双调排序的实现方式、

#include "topk.h"

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <vector>

#include "cuda_runtime.h"

#include "source/ndarray/ndarray.h"
#include "source/operator/internal/cuda_utils.h"
#include "spdlog/spdlog.h"

namespace internal_ops {
// 1. 双调排序的topk实现方式
namespace bitonic_topk {
struct KV {
  float key;
  int index;
};

constexpr float float_max = std::numeric_limits<float>::max();
constexpr float float_min = std::numeric_limits<float>::lowest();

#define FROM(idx) (left_dim + (idx)*mid_dim + right_dim)
#define TO(idx) (left_dim * K / dimension + (idx)*mid_dim + right_dim)
#define TRIVIAL (1 == largest ? float_min : float_max)
#define BIGGER(n, m) \
  (n.key > m.key ? n : (n.key < m.key ? m : (n.index > m.index ? (1 == largest ? m : n) : (1 == largest ? n : m))))
#define SMALLER(n, m) \
  (n.key < m.key ? n : (n.key > m.key ? m : (n.index < m.index ? (1 == largest ? m : n) : (1 == largest ? n : m))))
#define IS_SMALLER(n, m) (n.key < m.key || !(n.key > m.key) && (1 == largest ? n.index > m.index : n.index < m.index))

__global__ void gBitonicTopk(const float* X, float* V, float* I, int* elem_nums, int dims, int axis, int K,
                             int aligned_K, int largest, int sorted, int dimension, int aligned_dimension) {
  int tid = threadIdx.x;
  int bid = blockIdx.x;
  int bdim = blockDim.x;

  // 使用的共享内存的大小：sizeof(KV)=8, aligned_dimension最大为1024, 因此最大占用8K
  // P4为48K，T4为64K。限定上述最长排序长度为1024，可以保证在机器限制范围内
  extern __shared__ char shared_mem[];
  KV* S = (KV*)(shared_mem);
  int mid_dim = (axis == dims - 1) ? 1 : elem_nums[axis + 1];
  int left_dim = bid / mid_dim * elem_nums[axis];
  int right_dim = (axis == dims - 1) ? 0 : bid % elem_nums[axis + 1];
  for (int i = tid; i < aligned_dimension; i += bdim) {
    S[i].key = i < dimension ? X[FROM(i)] : TRIVIAL;
    S[i].index = i;
  }
  __syncthreads();
  for (int len = 1; len < aligned_K; len <<= 1) {
    int dir = len << 1;
    for (int inc = len; inc > 0; inc >>= 1) {
      int i = (tid << 1) - (tid & (inc - 1));
      int j = i + inc;
      if (j < aligned_dimension) {
        bool reverse = (dir & i) == 0;
        bool swap = reverse ^ IS_SMALLER(S[i], S[j]);
        if (swap) {
          KV tmp = S[i];
          S[i] = S[j];
          S[j] = tmp;
        }
      }
      __syncthreads();
    }
    __syncthreads();
  }
  for (int len = aligned_K; len < aligned_dimension; len <<= 1) {
    int dir = len << 1;
    int i = (tid << 1) - (tid & (len - 1));
    int j = i + len;
    if (i % dir < aligned_K && j < aligned_dimension) {
      S[i] = (1 == largest) ? BIGGER(S[i], S[j]) : SMALLER(S[i], S[j]);
    }
    __syncthreads();
    for (int inc = aligned_K >> 1; inc > 0; inc >>= 1) {
      int ii = (tid << 1) - (tid & (inc - 1));
      int jj = ii + inc;
      if (ii % dir < aligned_K && jj < aligned_dimension) {
        bool reverse = (dir & ii) == 0;
        bool swap = reverse ^ IS_SMALLER(S[ii], S[jj]);
        if (swap) {
          KV tmp = S[ii];
          S[ii] = S[jj];
          S[jj] = tmp;
        }
      }
      __syncthreads();
    }
    __syncthreads();
  }
  if (1 == sorted) {
    if (1 == largest) {
      int start = aligned_K - K;
      if (tid >= start && tid < aligned_K) {
        int to = TO(aligned_K - 1 - tid);
        V[to] = S[tid].key;
        I[to] = S[tid].index;
      }
    } else {
      if (tid < K) {
        auto to = TO(tid);
        V[to] = S[tid].key;
        I[to] = S[tid].index;
      }
    }
  } else {
    if (1 == largest) {
      int start = aligned_K - K;
      if (tid < start) {
        S[tid].index = aligned_dimension;
      }
    } else {
      if (tid >= K && tid < aligned_K) {
        S[tid].index = aligned_dimension;
      }
    }
    __syncthreads();
    // sort by index ascending
    for (int64_t len = 1; len < aligned_K; len <<= 1) {
      int dir = len << 1;
      for (int64_t inc = len; inc > 0; inc >>= 1) {
        int low = tid & (inc - 1);
        int i = (tid << 1) - low;
        int j = i + inc;
        if (j < aligned_K) {
          bool reverse = (dir & i) == 0;
          bool swap = reverse ^ (S[i].index < S[j].index);
          if (swap) {
            KV tmp = S[i];
            S[i] = S[j];
            S[j] = tmp;
          }
        }
        __syncthreads();
      }
      __syncthreads();
    }
    if (tid < K) {
      auto to = TO(tid);
      V[to] = S[tid].key;
      I[to] = S[tid].index;
    }
  }
}
}  // namespace bitonic_topk

void BitonicTopk(NDArray* out_value, NDArray* out_index, const NDArray* in, int32_t k, int32_t axis, int32_t largest,
                 int32_t sorted, cudaStream_t stream) {
  TShape tshape = in->shape();
  std::vector<int> shape = tshape.to_vector();
  std::vector<int> elem(shape);
  for (int i = 1; i < elem.size(); i++) {
    elem[elem.size() - 1 - i] = elem[elem.size() - 1 - i] * elem[elem.size() - i];
  }
  int* elem_gpu = nullptr;
  GetBuf(elem_gpu, elem.size(), int, buf_1);
  cudaMemcpyAsync(elem_gpu, elem.data(), elem.size() * sizeof(int), cudaMemcpyHostToDevice, stream);

  int dims = shape.size();
  int dimension = shape[axis];
#define ALIGN(N) static_cast<int>(pow(2, ceil(log2(static_cast<double>(N)))))
  int aligned_k = ALIGN(k);
  int aligned_dimension = ALIGN(dimension);
#undef ALIGN
  float* V = out_value->data();
  float* I = out_index->data();
  const float* data = in->data();

  int threads = aligned_dimension;
  assert(threads <= 1024);
  int blocks = 1;
  for (int i = 0; i < shape.size(); i++) {
    if (i != axis) {
      blocks *= shape[i];
    }
  }

  bitonic_topk::gBitonicTopk<<<blocks, threads, aligned_dimension * sizeof(bitonic_topk::KV), stream>>>(
      data, V, I, elem_gpu, dims, axis, k, aligned_k, largest, sorted, dimension, aligned_dimension);

  return;
}

}  // namespace internal_ops