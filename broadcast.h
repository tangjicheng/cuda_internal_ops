// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Broadcast类算子的实现
// 包括：Add/Sub/Mul/Div/Pow/PRelu
#pragma once

#include <vector>
#include "cuda_runtime.h"

namespace internal_ops {

void AddImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream);

void SubImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream);

void MulImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream);

void DivImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream);

void PowImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
             const std::vector<int>& right_shape, cudaStream_t stream);

void PReluImpl(float* output, const float* left, const float* right, const std::vector<int>& left_shape,
               const std::vector<int>& right_shape, cudaStream_t stream);

}  // namespace internal_ops
