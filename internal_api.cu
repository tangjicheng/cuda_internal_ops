#include <math.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/gather.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <cstdio>
#include <cstring>
#include <iostream>
#include "activation.h"
#include "broadcast.h"
#include "concat.h"
#include "cuda_params.h"
#include "enum.h"
#include "expand.h"
#include "gather.h"
#include "kernel_io.h"
#include "kernel_transformer.h"
#include "pointwise.h"
#include "reduce.h"
#include "source/operator/internal/internal_api.h"
#include "topk.h"
#include "transpose.h"

namespace internal_ops {

int Transpose(NDArray* out, const NDArray* in, const std::vector<int>& perm, cudaStream_t stream) {
  TransposeImpl(out, in, perm, stream);
  return 0;
}

#define ADD_BROADCAST_FUNC(name, impl)                                                                 \
  int name(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream) {                \
    TShape in1_shape = in1->shape();                                                                   \
    TShape in2_shape = in2->shape();                                                                   \
    impl(out->data(), in1->data(), in2->data(), in1_shape.to_vector(), in2_shape.to_vector(), stream); \
    return 0;                                                                                          \
  }

ADD_BROADCAST_FUNC(AddFast, AddImpl)
ADD_BROADCAST_FUNC(SubFast, SubImpl)
ADD_BROADCAST_FUNC(MulFast, MulImpl)
ADD_BROADCAST_FUNC(DivFast, DivImpl)
ADD_BROADCAST_FUNC(PowFast, PowImpl)
ADD_BROADCAST_FUNC(PRelu, PReluImpl)

int Expand(NDArray* out, NDArray* in, const TShape& expand_shape, cudaStream_t stream) {
  int ret = 0;

  NDArray expand_ndarray(expand_shape);
  expand_ndarray.set_value(0.0f);
  out->resize(expand_shape);

  // in 和 expand_shape生成的expand_ndarray进行broadcast相加
  std::vector<NDArray*> in_vec{in, &expand_ndarray};

  ret = (internal_opsStatus_t)ElementBroadcast(out, in_vec, Symbol::plus, stream);
  return ret;
}

int ExpandFast(NDArray* out, NDArray* in, NDArray* expand_shape, cudaStream_t stream) {
  int ret = 0;
  float* output_ptr = out->data();
  float* input_ptr = in->data();
  float* expand_ptr = expand_shape->data();
  int expand_shape_size = expand_shape->shape().Size();
  TShape in_shape = in->shape();
  ExpandImpl(output_ptr, input_ptr, expand_ptr, in_shape.to_vector(), expand_shape_size, stream);
  return ret;
}

int Continuous(NDArray* out, const NDArray* in, cudaStream_t stream) {
  int ret = 0;
  TShape in_tshape = in->shape();
  int dim = in->dims();
  int* shape = in_tshape.data();
  int* stride = in->stride();
  out->resize(in_tshape);

  TShape s_shape(1);
  s_shape[0] = dim;
  NDArray nd_shape(s_shape, false);
  NDArray nd_stride(s_shape, false);
  cudaMemcpy(nd_shape.data(), shape, dim * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(nd_stride.data(), stride, dim * sizeof(int), cudaMemcpyHostToDevice);
  int shape_blocks = 1;
  int shape_threads = dim;
  NDArray nd_shpae_float(s_shape, false);
  NDArray nd_stride_float(s_shape, false);
  gShapeStrideCopy<<<shape_blocks, shape_threads, 0, stream>>>(nd_shpae_float.data(), nd_stride_float.data(),
                                                               (int*)(nd_shape.data()), (int*)(nd_stride.data()),
                                                               nd_shape.rows(), nd_shape.cols());

  int blocks = std::min(MAX_BLOCKS, in->rows());
  int threads = std::min(MAX_THREADS, in->cols());
  int shareds = threads * dim * sizeof(int);

  gContinuous<<<blocks, threads, shareds, stream>>>(out->data(), in->data(), nd_shpae_float.data(),
                                                    nd_stride_float.data(), dim, in->rows(), in->cols());

  return ret;
}

int Range(NDArray* out, float start, float delta, int number_of_element, cudaStream_t stream) {
  int ret = 0;
  if (number_of_element == 0) {
    out->clear();
    return ret;
  }
  TShape out_shape(1);
  out_shape[0] = number_of_element;
  out->resize(out_shape);

  int threads = std::min(MAX_THREADS, number_of_element);
  int cols = threads;
  int rows = number_of_element / threads + 1;
  int blocks = std::min(MAX_BLOCKS, rows);

  gRange<<<blocks, threads, 0, stream>>>(out->data(), start, delta, number_of_element, rows, cols);

  return ret;
}

// active with 1 param
int Elu(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream) {
  Activation1<internal_ops_elu>(out, in, alpha, stream);
  return 0;
}

int LeakyRelu(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream) {
  Activation1<internal_ops_leaky_relu>(out, in, alpha, stream);
  return 0;
}

int ThresholdedRelu(NDArray* out, const NDArray* in, float alpha, cudaStream_t stream) {
  Activation1<internal_ops_thresholded_relu>(out, in, alpha, stream);
  return 0;
}

// active with 2 param
int HardSigmoid(NDArray* out, const NDArray* in, float alpha, float beta, cudaStream_t stream) {
  Activation2<internal_ops_hard_sigmoid>(out, in, alpha, beta, stream);
  return 0;
}

int Selu(NDArray* out, const NDArray* in, float alpha, float gamma, cudaStream_t stream) {
  Activation2<internal_ops_selu>(out, in, alpha, gamma, stream);
  return 0;
}

int Clip(NDArray* out, const NDArray* in, float min_value, float max_value, cudaStream_t stream) {
  Activation2<internal_ops_clip>(out, in, min_value, max_value, stream);
  return 0;
}

// general operator

int Slice(NDArray* out, const NDArray* in, const NDArray* axes, const NDArray* starts, const NDArray* ends,
          const NDArray* steps, cudaStream_t stream) {
  // Check input
  if (out == nullptr || in == nullptr || starts == nullptr || ends == nullptr || axes == nullptr || steps == nullptr) {
    printf("[Error] [Slice] some input NDArray is nullptr\n");
    return 1;
  }
  int in_dim = in->dims();
  TShape in_shape = in->shape();
  int slice_dim = starts->dims();
  if (ends->dims() != slice_dim || axes->dims() != slice_dim || steps->dims() != slice_dim) {
    printf("[Error] [Slice] input dim error\n");
    return 1;
  }

  auto axes_vec = axes->copy_to_host();
  auto starts_vec = starts->copy_to_host();
  auto ends_vec = ends->copy_to_host();
  auto steps_vec = steps->copy_to_host();

  // TODO(tangjicheng)
  NDArray in_copy(in->shape());
  in_copy.copy_from_gpu_to_gpu(*in, stream);

  for (int i = 0; i < slice_dim; i++) {
    in_copy.Slice2(axes_vec[i], starts_vec[i], ends_vec[i], steps_vec[i]);
  }
  Continuous(out, &in_copy, stream);

  return 0;
}

int SliceCopy(NDArray& Out, const NDArray& In, size_t in_row_begin, size_t in_col_begin, size_t out_row_len,
              size_t out_col_len, cudaStream_t stream) {
  if (out_row_len * out_col_len == 0) {
    return 0;
  }

  TShape oshape(2);
  oshape[0] = out_row_len;
  oshape[1] = out_col_len;
  Out.AutoResize(oshape);

  float* d_out = Out.data();
  const float* d_in = In.data();

  int threads = std::min(MAX_THREADS, (int)out_col_len);
  int blocks = std::min(MAX_BLOCKS, (int)out_row_len);

  gSliceCopy<<<blocks, threads, 0, stream>>>(d_out, d_in, in_row_begin, in_col_begin, In.cols(), out_row_len,
                                             out_col_len);

  return 0;
}

int Abs(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<fabsf>(out, in, stream); }

int Acos(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<acosf>(out, in, stream); }

int Acosh(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<acoshf>(out, in, stream); }

int Asin(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<asinf>(out, in, stream); }

int Asinh(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<asinhf>(out, in, stream); }

int Atan(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<atanf>(out, in, stream); }

int Atanh(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<atanhf>(out, in, stream); }

int Ceil(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<ceilf>(out, in, stream); }

int Cos(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<cosf>(out, in, stream); }

int Cosh(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<coshf>(out, in, stream); }

int Erf(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<erff>(out, in, stream); }

int Exp(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<expf>(out, in, stream); }

int Floor(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<floorf>(out, in, stream); }

int Identity(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<internal_ops_identity>(out, in, stream); }

int Log(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<logf>(out, in, stream); }

int Neg(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<internal_ops_neg>(out, in, stream); }

int Sign(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<internal_ops_sign>(out, in, stream); }

int Sin(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<sinf>(out, in, stream); }

int Sinh(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<sinhf>(out, in, stream); }

int Sqrt(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<sqrtf>(out, in, stream); }

int Tanh(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<tanhf>(out, in, stream); }

int Tan(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<tanf>(out, in, stream); }

int Sigmoid(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<internal_ops_sigmoid>(out, in, stream); }

int Softplus(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<internal_ops_softplus>(out, in, stream); }

int Softsign(NDArray* out, const NDArray* in, cudaStream_t stream) { return Pointwise<internal_ops_softsign>(out, in, stream); }

int Relu(NDArray* tensor, cudaStream_t stream) {
  int ret = 0;

  int blocks = std::min(MAX_BLOCKS, tensor->rows());
  int threads = std::min(MAX_THREADS, tensor->cols());

  gRelu<<<blocks, threads, 0, stream>>>(tensor->data(), tensor->shape().Size());

  return ret;
}

int Tanh(NDArray* tensor, cudaStream_t stream) {
  int ret = 0;

  int blocks = std::min(MAX_BLOCKS, tensor->rows());
  int threads = std::min(MAX_THREADS, tensor->cols());

  gTanh<<<blocks, threads, 0, stream>>>(tensor->data(), tensor->shape().Size());

  return ret;
}

int Element(enum Symbol symbol, NDArray* out, float scale, cudaStream_t stream) {
  int ret = 0;

  if (out->shape().Size() == 0) {
    return ret;
  }

  int blocks = std::min(MAX_BLOCKS, out->rows());
  int threads = std::min(MAX_THREADS, out->cols());
  gElement<<<blocks, threads, 0, stream>>>(int(symbol), out->data(), scale, out->rows(), out->cols());

  return ret;
}

int Element(enum Symbol symbol, NDArray* out, const NDArray* in1, float scale, cudaStream_t stream) {
  int ret = 0;

  out->AutoResize(in1->shape());

  float* d_out = out->data();
  const float* d_in1 = in1->data();

  int blocks = std::min(MAX_BLOCKS, out->rows());
  int threads = std::min(MAX_THREADS, out->cols());
  gElement<<<blocks, threads, 0, stream>>>(int(symbol), d_out, d_in1, scale, out->rows(), out->cols());

  return ret;
}

int Element(enum Symbol symbol, NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream) {
  int ret = 0;

  out->AutoResize(in1->shape());

  float* d_out = out->data();
  const float* d_in1 = in1->data();
  const float* d_in2 = in2->data();

  int blocks = std::min(MAX_BLOCKS, out->rows());
  int threads = std::min(MAX_THREADS, out->cols());
  gElement<<<blocks, threads, 0, stream>>>(int(symbol), d_out, d_in1, d_in2, out->rows(), out->cols());

  return ret;
}

int ElementAdd(NDArray* out, float scale, cudaStream_t stream) { return Element(Symbol::plus, out, scale, stream); }

int ElementMul(NDArray* out, float scale, cudaStream_t stream) { return Element(Symbol::times, out, scale, stream); }

int ElementSub(NDArray* out, float scale, cudaStream_t stream) { return Element(Symbol::minus, out, scale, stream); }

int ElementAdd(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream) {
  return Element(Symbol::plus, out, in1, scale, stream);
}

int ElementMul(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream) {
  return Element(Symbol::times, out, in1, scale, stream);
}

int ElementSub(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream) {
  return Element(Symbol::minus, out, in1, scale, stream);
}

int ElementSub2(NDArray* out, const NDArray* in1, float scale, cudaStream_t stream) {
  int ret = 0;

  out->AutoResize(in1->shape());

  float* d_out = out->data();
  const float* d_in1 = in1->data();

  int blocks = std::min(MAX_BLOCKS, out->rows());
  int threads = std::min(MAX_THREADS, out->cols());
  gSub2<<<blocks, threads, 0, stream>>>(d_out, d_in1, scale, out->rows(), out->cols());

  return ret;
}

int ElementAdd(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream) {
  return Element(Symbol::plus, out, in1, in2, stream);
}

int ElementMul(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream) {
  return Element(Symbol::times, out, in1, in2, stream);
}

int ElementSub(NDArray* out, const NDArray* in1, const NDArray* in2, cudaStream_t stream) {
  return Element(Symbol::minus, out, in1, in2, stream);
}

int BroadcastAdd(NDArray* out, const NDArray* in1, cudaStream_t stream) {
  int ret = 0;

  float* d_out = out->data();
  const float* d_in1 = in1->data();

  int blocks = std::min(MAX_BLOCKS, out->rows());
  int threads = std::min(MAX_THREADS, out->cols());
  gBroadcastAdd<<<blocks, threads, 0, stream>>>(d_out, d_in1, out->rows(), out->cols());

  return ret;
}

int BroadcastAddNCHW(NDArray* out, const NDArray* in1, cudaStream_t stream) {
  int ret = 0;

  float* d_out = out->data();
  int n = out->shape()[0];
  int c = out->shape()[1];
  int h = out->shape()[2];
  int w = out->shape()[3];
  const float* d_in = in1->data();
  dim3 blocks(n, c);
  int threads = std::min(MAX_THREADS, h * w);
  gBroadcastAddNCHW<<<blocks, threads, 0, stream>>>(d_out, d_in, n, c, h, w);

  return ret;
}

int ElementBroadcast(NDArray* out, const std::vector<NDArray*>& vec_in, enum Symbol s, cudaStream_t stream) {
  int ret = 0;

  if (out->shape().Size() == 0) {
    return ret;
  }

  if (out->shape().ndim() == 0) {
    std::vector<float> cpu_in_0 = vec_in[0]->copy_to_host();
    std::vector<float> vec_out(1, 0.0f);
    vec_out[0] = cpu_in_0[0];
    for (int32_t idx_input = 1; idx_input < vec_in.size(); idx_input++) {
      std::vector<float> cpu_in = vec_in[idx_input]->copy_to_host();
      if (s == Symbol::plus) {
        vec_out[0] += cpu_in[0];
      } else if (s == Symbol::minus) {
        vec_out[0] -= cpu_in[0];
      } else if (s == Symbol::times) {
        vec_out[0] *= cpu_in[0];
      } else if (s == Symbol::division) {
        vec_out[0] /= cpu_in[0];
      } else if (s == Symbol::pow) {
        vec_out[0] = powf(vec_out[0], cpu_in[0]);
      }
    }
    out->copy_from_host(vec_out);

    return ret;
  }

  std::vector<NDArray*> vec_in_copy = vec_in;
  std::vector<int64_t> vec_in_data = broadcast_stride(vec_in_copy);
  vec_in_data.insert(vec_in_data.begin(), reinterpret_cast<int64_t>(out->data()));
  int64_t* dev_buf = NULL;
  cudaMalloc((void**)&dev_buf, vec_in_data.size() * sizeof(int64_t));
  cudaMemcpy(dev_buf, vec_in_data.data(), vec_in_data.size() * sizeof(int64_t), cudaMemcpyHostToDevice);

  int32_t out_ndim = out->shape().ndim();

  std::vector<float> vec_offset;
  std::vector<float> vec_stride;
  std::vector<float> vec_row;
  vec_offset.push_back(out->offset());
  vec_stride.insert(vec_stride.end(), out->stride(), out->stride() + out_ndim);
  for (int32_t i = 0; i < vec_in_copy.size(); i++) {
    vec_offset.push_back(vec_in_copy[i]->offset());
    vec_stride.insert(vec_stride.end(), vec_in_copy[i]->stride(),
                      vec_in_copy[i]->stride() + vec_in_copy[i]->shape().ndim());
  }
  vec_row = out->compute_row();

  int blocks = std::min(MAX_BLOCKS, out->rows());
  int threads = std::min(MAX_THREADS, out->cols());

  int32_t out_rows = out->rows();
  int32_t out_cols = out->cols();

  TShape shape_offset(1);
  TShape shape_stride(1);
  TShape shape_row(1);
  shape_offset[0] = 1 + vec_in.size();
  shape_stride[0] = out_ndim * (vec_in.size() + 1);
  shape_row[0] = out_ndim - 2;

  NDArray nd_offset(shape_offset, false);
  NDArray nd_stride(shape_stride, false);
  NDArray nd_row(shape_row, false);

  nd_offset.copy_from_host(vec_offset);
  nd_stride.copy_from_host(vec_stride);
  nd_row.copy_from_host(vec_row);

  float* offset = nd_offset.data();
  float* stride = nd_stride.data();
  float* prod_row = nd_row.data();

  int shared = out_ndim * sizeof(float);
  if (s == Symbol::plus) {
    gElementBroadcast<<<blocks, threads, shared, stream>>>(int(s), out_rows, out_cols, vec_in.size(), out_ndim, dev_buf,
                                                           offset, stride, prod_row);
  } else {
    gElementBroadcast2<<<blocks, threads, shared, stream>>>(int(s), out_rows, out_cols, vec_in.size(), out_ndim,
                                                            dev_buf, offset, stride, prod_row);
  }

  cudaFree(dev_buf);
  return ret;
}

int Concat(NDArray* out, const std::vector<NDArray*>& in, int axis, cudaStream_t stream) {
  int ret = 0;

  int64_t* dev_buf = NULL;
  std::vector<int64_t> in_data_vec;
  std::vector<float> in_axis_len_vec;
  TShape shape_in_axis_len(1);
  shape_in_axis_len[0] = in.size();
  for (int32_t i = 0; i < in.size(); i++) {
    in_data_vec.push_back((int64_t)(in[i]->data()));
    in_axis_len_vec.push_back((float)(in[i]->shape()[axis]));
  }
  cudaMalloc((void**)&dev_buf, in.size() * sizeof(int64_t));
  cudaMemcpyAsync(dev_buf, in_data_vec.data(), in_data_vec.size() * sizeof(int64_t), cudaMemcpyHostToDevice, stream);
  NDArray nd_axis_len(shape_in_axis_len, false);
  nd_axis_len.copy_from_host(in_axis_len_vec);

  NDArray& input0 = *(in[0]);

  int num_concats = 1;
  int concat_size = 1;
  for (int i = 0; i < axis; ++i) {
    num_concats *= input0.shape()[i];
  }
  for (int i = axis + 1; i < input0.shape().ndim(); ++i) {
    concat_size *= input0.shape()[i];
  }
  int output_concat_axis = out->shape()[axis];
  int output_concat_stride = output_concat_axis * concat_size;

  float* outputPtr = out->data();

  int blocks = std::min(MAX_BLOCKS, (int32_t)in.size());
  int threads = std::min(MAX_THREADS, num_concats);

  gConcat<<<blocks, threads, 0, stream>>>(outputPtr, dev_buf, in.size(), num_concats, concat_size, output_concat_stride,
                                          nd_axis_len.data());

  return ret;
}

int ConcatFast(NDArray* out, const std::vector<NDArray*>& in, int axis, cudaStream_t stream) {
  int ret = 0;
  float* output_data_ptr = out->data();
  std::vector<int64_t> input_data_ptr_vec;
  std::vector<std::vector<int>> input_shape_vec;
  for (int i = 0; i < in.size(); i++) {
    float* cur_ptr = in[i]->data();
    input_data_ptr_vec.push_back(reinterpret_cast<int64_t>(cur_ptr));
    std::vector<int> cur_shape = in[i]->shape().to_vector();
    input_shape_vec.push_back(cur_shape);
  }
  ret = ConcatImpl(output_data_ptr, input_data_ptr_vec, input_shape_vec, axis, stream);
  return ret;
}

int Reduce(NDArray* out, const NDArray* data, const std::vector<int32_t>& axes, enum ReduceType reduce_type,
           cudaStream_t stream) {
  NDArray reduce_in(data->shape(), false);
  NDArray reduce_out(data->shape(), false);
  reduce_in.copy_from_gpu_to_gpu(*data, stream);

  for (int32_t i = 0; i < axes.size(); i++) {
    if (i != 0) {
      reduce_in.copy_from_gpu_to_gpu(reduce_out, stream);
    }
    int32_t axis = axes[i];

    int inside = 1;
    int outside = 1;
    for (int i = 0; i < axis; ++i) {
      outside *= reduce_in.shape()[i];
    }
    for (int i = axis + 1; i < reduce_in.shape().ndim(); ++i) {
      inside *= reduce_in.shape()[i];
    }

    const int limit = reduce_in.shape()[axis];
    const int insideStride = inside * limit;

    int blocks = std::min(MAX_BLOCKS, outside);
    int threads = std::min(MAX_THREADS, inside);
    int shared = sizeof(double) * inside;

    gReduceGeneral<<<blocks, threads, shared, stream>>>(reduce_out.data(), reduce_in.data(), outside, inside,
                                                        insideStride, limit, axis, (int)reduce_type);

    reduce_out.shape()[axis] = 1;
  }

  out->copy_from_gpu_to_gpu(reduce_out, stream);

  return 0;
}

int32_t Topk(NDArray* out_value, NDArray* out_index, const NDArray* data, int32_t k, int32_t axis, int32_t largest,
             int32_t sorted, cudaStream_t stream) {
  int32_t inside = 1;
  int32_t outside = 1;
  for (int32_t i = 0; i < axis; ++i) {
    outside *= data->shape()[i];
  }
  for (int32_t i = axis + 1; i < data->shape().ndim(); ++i) {
    inside *= data->shape()[i];
  }

  const int32_t limit = data->shape()[axis];
  const int32_t insideStride = inside * limit;

  int32_t blocks = std::min(MAX_BLOCKS, outside);
  int32_t threads = std::min(MAX_THREADS, inside);
  int32_t shared = sizeof(double) * inside * limit * 4;

  gTopkGeneral<<<blocks, threads, shared, stream>>>(out_value->data(), out_index->data(), data->data(), outside, inside,
                                                    insideStride, limit, k, largest, sorted);

  return 0;
}

int TopkFast(NDArray* out_value, NDArray* out_index, const NDArray* in, int32_t k, int32_t axis, int32_t largest,
             int32_t sorted, cudaStream_t stream) {
  TShape tshape = in->shape();
  std::vector<int> shape = tshape.to_vector();
  int sorted_axis_size = shape[axis];
  if (sorted_axis_size < 1024) {
    BitonicTopk(out_value, out_index, in, k, axis, largest, sorted, stream);
    return 0;
  } else {
    spdlog::error("TopkFast not implemented for axis size > 1024");
  }

  return 0;
}

int SetValue(NDArray* out, float value, cudaStream_t stream) {
  int out_rows = out->rows();
  int out_cols = out->cols();

  int32_t blocks = std::min(MAX_BLOCKS, out_rows);
  int32_t threads = std::min(MAX_THREADS, out_cols);

  gSetValue<<<blocks, threads, 0, stream>>>(out->data(), out_rows, out_cols, value);

  return 0;
}

int CopyRows(NDArray& Out, const NDArray& In, size_t* dst_ids, size_t* src_ids, size_t nums, cudaStream_t stream) {
  float* d_out = Out.data();
  const float* d_in = In.data();
  if (nums == 0) {
    return 0;
  }

  int threads_x = std::min(MAX_THREADS, (int)In.cols());
  int threads_y = MAX_THREADS / threads_x;
  int blocks = std::min(MAX_BLOCKS, ((int)nums + threads_y - 1) / threads_y);
  gCopyRowsGeneral<<<blocks, dim3(threads_x, threads_y), 0, stream>>>(d_out, d_in, In.cols(), dst_ids, src_ids, nums);
  // cudaStreamSynchronize(0);
  return 0;
}

int CopyRows(NDArray& Out, const NDArray& In, const std::vector<size_t>& dst_ids, const std::vector<size_t>& src_ids,
             cudaStream_t stream) {
  thrust::device_vector<size_t> dev_dst_ids = dst_ids;
  thrust::device_vector<size_t> dev_src_ids = src_ids;

  CopyRows(Out, In, thrust::raw_pointer_cast(dev_dst_ids.data()), thrust::raw_pointer_cast(dev_src_ids.data()),
           dev_dst_ids.size(), stream);
  return 0;
}

int LayerNorm(NDArray* out, const NDArray* in, const NDArray* scales, const NDArray* biases, cudaStream_t stream) {
  int ret = 0;

  int rows = in->rows();
  int cols = in->cols();
  int blocks = std::min(MAX_BLOCKS, rows);
  int threads = std::min(MAX_THREADS, cols);
  int shared = sizeof(double) * threads;
  gLayerNorm<<<blocks, threads, shared, stream>>>(out->data(), in->data(), scales->data(), biases->data(), rows, cols);

  return ret;
}

static bool CheckNeedRearrangeData(const std::vector<int>& index, const std::vector<int>& out_shape) {
  for (int i = 0, left_most = 0; i < index.size(); i++) {
    if (out_shape[i] != 1) {
      if (index[i] < left_most) {
        return true;
      }
      left_most = index[i];
    }
  }
  return false;
}

int Gather(NDArray* output, NDArray* data, NDArray* indices, int32_t axis, cudaStream_t stream) {
  const int N = indices->shape().Size();
  int inside = 1;
  int outside = 1;
  for (int i = 0; i < axis; ++i) {
    outside *= data->shape()[i];
  }
  for (int i = axis + 1; i < data->shape().ndim(); ++i) {
    inside *= data->shape()[i];
  }
  const int limit = data->shape()[axis];
  const int insideStride = inside;
  const int outputOutsideStride = inside * N;
  const int inputOutsideStride = inside * data->shape()[axis];
  const float* indicesPtr = indices->data();
  const float* inputPtr = data->data();
  float* outputPtr = output->data();

  int blocks = std::min(MAX_BLOCKS, outside);
  int threads = std::min(MAX_THREADS, N);

  gGather<<<blocks, threads, 0, stream>>>(outputPtr, inputPtr, indicesPtr, outside, N, outputOutsideStride,
                                          inputOutsideStride, insideStride, limit);

  return 0;
}

int GatherFast(NDArray* output, NDArray* data, NDArray* indices, int32_t axis, cudaStream_t stream) {
  int ret = 0;
  float* output_data = output->data();
  float* input_data = data->data();
  float* indices_data = indices->data();
  std::vector<int> input_shape = data->shape().to_vector();
  std::vector<int> indices_shape = indices->shape().to_vector();
  ret = GatherImpl(output_data, input_data, indices_data, input_shape, indices_shape, axis, stream);
  return ret;
}

int GatherElements(NDArray* output, NDArray* data, NDArray* indices, int32_t axis, cudaStream_t stream) {
  int32_t ndim = data->shape().ndim();
  int32_t indices_size = indices->shape().Size();
  const TShape& data_shape = data->shape();
  const TShape& indices_shape = indices->shape();

  std::vector<float> indices_cpu = indices->copy_to_host();
  std::vector<float> data_cpu = data->copy_to_host();
  std::vector<int32_t> data_index(indices_size * ndim, 0);
  std::vector<float> out_cpu(indices_cpu.size(), 0.0f);

  data_index[axis] = indices_cpu[0];
  for (int32_t i = 1; i < indices_size; i++) {
    bool need_carry = false;

    int32_t* pre_index_begin = data_index.data() + (i - 1) * ndim;

    for (int32_t idim = ndim - 1; idim >= 0; idim--) {
      int32_t value = *(pre_index_begin + idim);

      if (idim == ndim - 1) {
        value += 1;
        if (value >= indices_shape[idim]) {
          need_carry = true;
          value = 0;
        }
        data_index[i * ndim + idim] = value;
      } else {
        if (need_carry) {
          int32_t value = *(pre_index_begin + idim);
          value += 1;
          if (value >= indices_shape[idim]) {
            need_carry = true;
            value = 0;
          } else {
            need_carry = false;
          }
          data_index[i * ndim + idim] = value;
        } else {
          data_index[i * ndim + idim] = *(pre_index_begin + idim);
        }
      }
    }
  }

  for (int32_t i = 0; i < indices_size; i++) {
    for (int32_t idim = ndim - 1; idim >= 0; idim--) {
      if (idim == axis) {
        data_index[i * ndim + idim] = indices_cpu[i];
      }
    }
  }

  for (int32_t i = 0; i < indices_size; i++) {
    std::vector<int32_t> idx_data(data_index.begin() + i * ndim, data_index.begin() + (i + 1) * ndim);

    int32_t data_out_index = data->index(idx_data);

    out_cpu[i] = data_cpu[data_out_index];
  }

  output->copy_from_host(out_cpu);

  return 0;
}

// glue operator
namespace glue {

int Shape(NDArray* out, const NDArray* in, cudaStream_t stream) {
  int ret = 0;
  ret = Shape(out, in, 0, in->dims(), stream);
  return ret;
}

int Shape(NDArray* out, const NDArray* in, int start, int end, cudaStream_t stream) {
  int ret = 0;
  const TShape& shape = in->shape();
  int dim = shape.ndim();
  if (start < 0) {
    start += dim;
  }
  if (end < 0) {
    end += dim;
  }
  // 注意，这里end可以大于或者等于dim。如果end大于dim，索引也会到dim截止。
  end = std::min(end, dim);
  if (start < 0 || start >= dim || end < 0 || start > end) {
    printf("[Error] Shape Opeartor param error: start = %d, end = %d, input tensor dim is %d\n", start, end, dim);
    ret = -1;
    start = 0;
    end = dim;
  }
  int out_size = end - start;
  std::vector<float> out_tensor_cpu(out_size, 0);
  for (int i = start; i < end; ++i) {
    out_tensor_cpu[i] = (float)shape[i];
  }
  TShape out_shape(1);
  out_shape[0] = out_size;
  out->resize(out_shape);
  out->copy_from_host(out_tensor_cpu);
  return ret;
}

}  // namespace glue

// cv operator
namespace cv {

int GlobalAveragePool(NDArray* out, const NDArray* in, cudaStream_t stream) {
  int ret = 0;

  TShape shape = in->shape();
  if (shape.ndim() != 4) {
    printf("[error] input dim != 4, dim = %d\n", shape.ndim());
    return -1;
  }
  int n = shape[0];
  int c = shape[1];
  int h = shape[2];
  int w = shape[3];
  int rows = n * c;
  int cols = h * w;
  int blocks = std::min(MAX_BLOCKS, rows);
  int threads = std::min(MAX_THREADS, cols);
  int shared = sizeof(float) * threads;

  std::vector<int> new_shape_vec = {n, c, 1, 1};
  TShape new_shape(new_shape_vec.begin(), new_shape_vec.end());
  out->resize(new_shape);
  gReduceMean<<<blocks, threads, shared, stream>>>(out->data(), in->data(), rows, cols);

  return ret;
}

}  // namespace cv
}  // namespace internal_ops
