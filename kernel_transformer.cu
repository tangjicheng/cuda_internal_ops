#include <cmath>
#include "kernel_transformer.h"

const unsigned int WARP_SIZE = 32;
const unsigned int WARP_REDUCE_MASK = 0xffffffff;
const float epsilon = 0.000000000001;

template <typename T>
__forceinline__ __device__ T warpReduceSum(T val) {
  for (int mask = (WARP_SIZE >> 1); mask > 0; mask >>= 1) {
    val += __shfl_xor_sync(WARP_REDUCE_MASK, val, mask, WARP_SIZE);
  }
  return val;
}

template <typename T>
__forceinline__ __device__ T warpReduceMax(T val) {
  for (int mask = (WARP_SIZE >> 1); mask > 0; mask >>= 1) {
    val = max(val, __shfl_xor_sync(WARP_REDUCE_MASK, val, mask, WARP_SIZE));
  }
  return val;
}

/* Calculate the sum of all elements in a block */
template <typename T>
__forceinline__ __device__ T blockReduceSum(T val) {
  static __shared__ T shared[32];
  int lane = threadIdx.x & 0x1f;
  int wid = threadIdx.x >> 5;

  val = warpReduceSum<T>(val);

  if (lane == 0) {
    shared[wid] = val;
  }
  __syncthreads();

  // val = (threadIdx.x < (blockDim.x >> 5 )) ? shared[lane] : (T)0.0f;
  val = (threadIdx.x < ((blockDim.x + 31) >> 5)) ? shared[lane] : (T)0.0f;
  val = warpReduceSum<T>(val);
  return val;
}

template <typename T>
__forceinline__ __device__ T blockReduceMax(T val) {
  static __shared__ T shared[32];
  int lane = threadIdx.x & 0x1f;
  int wid = threadIdx.x >> 5;

  val = warpReduceMax<T>(val);

  if (lane == 0) shared[wid] = val;
  __syncthreads();

  val = (threadIdx.x < ((blockDim.x + 31) >> 5)) ? shared[lane] : -1.0e9;
  val = warpReduceMax<T>(val);
  return val;
}

__global__ void gPosEmb(float* out, int rows, int cols, float min_timescale, float log_increment) {
  for (int bid = blockIdx.x; bid < rows; bid += gridDim.x) {
    float* sp = out + bid * cols;
    for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
      int oid = tid / 2;
      if (tid % 2 != 0) {
        oid += cols / 2;
      }
      float scaled_time = bid * min_timescale * expf(tid / 2 * (-log_increment));
      if (tid % 2 == 0) {
        sp[oid] = sin(scaled_time);
      } else {
        sp[oid] = cos(scaled_time);
      }
    }
  }
}

__global__ void gLayerNorm(float* out, const float* in, const float* scales, const float* biases, int rows, int cols) {
  uint block_start = blockIdx.x * cols;
  uint start = block_start + threadIdx.x;
  uint end = block_start + cols;
  float val = 0.0;
  for (uint i = start; i < end; i += blockDim.x) {
    val += in[i];
  }

  // step 0. compute mean
  __shared__ float s_mean;
  float reduce_res = blockReduceSum<float>(val);
  if (threadIdx.x == 0) {
    s_mean = reduce_res / float(cols);
  }
  __syncthreads();

  // step 1. compute variance
  val = 0.0;
  for (uint i = start; i < end; i += blockDim.x) {
    float tmp = in[i] - s_mean;
    val += tmp * tmp;
  }
  __shared__ float s_var;
  reduce_res = blockReduceSum(val);
  if (threadIdx.x == 0) {
    s_var = rsqrtf(reduce_res / float(cols) + epsilon);
  }
  __syncthreads();

  // step 2. layer norm
  for (uint i = start; i < end; i += blockDim.x) {
    val = in[i] - s_mean;
    out[i] = val * s_var * __ldg(&scales[i - block_start]) + __ldg(&biases[i - block_start]);
  }
}

__global__ void gBidirectionalRelativePositionBucket(int row, int col, const float* weight, int num_bucket, int dim,
                                                     int max_distance, float* out) {
  int cur_batch = 0;
  int cur_dim = blockIdx.z;
  num_bucket /= 2;
  for (int cur_row = blockIdx.x; cur_row < row; cur_row += gridDim.x) {
    for (int cur_col = threadIdx.x; cur_col < col; cur_col += blockDim.x) {
      int max_exact = num_bucket / 2;
      float target_bucket = abs(cur_row - cur_col);
      if (target_bucket + 1e-4 > max_exact) {
        target_bucket = floorf(max_exact + logf(target_bucket / max_exact) / logf(max_distance / max_exact) *
                                               (num_bucket - max_exact));
      }
      target_bucket = fminf(target_bucket, num_bucket - 1);
      if (cur_col > cur_row) {
        target_bucket += num_bucket;
      }
      out[cur_batch * (row * col * dim) + cur_row * (col * dim) + cur_col * dim + cur_dim] =
          weight[(int)target_bucket * dim + cur_dim];
    }
  }
}

__global__ void gUnidirectionalRelativePositionBucket(int row, int col, const float* weight, int num_bucket, int dim,
                                                      int max_distance, float* out) {
  int cur_batch = 0;
  int cur_dim = blockIdx.z;
  int cur_row = row - 1;
  for (int cur_col = threadIdx.x; cur_col < col; cur_col += blockDim.x) {
    int max_exact = num_bucket / 2;
    float target_bucket = cur_row > cur_col ? cur_row - cur_col : 0;
    if (target_bucket + 1e-4 > max_exact) {
      target_bucket = floorf(max_exact + logf(target_bucket / max_exact) / logf(max_distance / max_exact) *
                                             (num_bucket - max_exact));
    }
    target_bucket = fminf(target_bucket, num_bucket - 1);
    out[cur_batch * (1 * col * dim) + 0 * (col * dim) + cur_col * dim + cur_dim] =
        weight[(int)target_bucket * dim + cur_dim];
  }
}

__global__ void gLayerVarNorm(const float* in, int row, int col, float eps, const float* scale, float* out) {
  float val = 0.0;
  for (int i = threadIdx.x; i < col; i += blockDim.x) {
    float tmp = in[blockIdx.x * col + i];
    val += tmp * tmp;
  }
  __shared__ float s_var;
  float reduce_res = blockReduceSum<float>(val);
  if (threadIdx.x == 0) s_var = rsqrtf(reduce_res / float(col) + eps);
  __syncthreads();
  for (int i = threadIdx.x; i < col; i += blockDim.x) {
    out[blockIdx.x * col + i] = in[blockIdx.x * col + i] * s_var * scale[i];
  }
}

__global__ void gAttentionMaskRelativePositionEmbeddingSoftmax(int batch, int head, int qlen, int kvlen,
                                                               const float* mask, const float* emb, float* attention) {
  int cur_batch = blockIdx.z;
  int cur_head = blockIdx.y;
  extern __shared__ float shared[];
  for (int cur_qlen = blockIdx.x; cur_qlen < qlen; cur_qlen += gridDim.x) {
    float val = -1.0e9;
    ;
    for (int cur_kvlen = threadIdx.x; cur_kvlen < kvlen; cur_kvlen += blockDim.x) {
      shared[cur_kvlen] =
          attention[cur_batch * head * qlen * kvlen + cur_head * qlen * kvlen + cur_qlen * kvlen + cur_kvlen] +
          emb[cur_qlen * kvlen * head + cur_kvlen * head + cur_head] + mask[cur_batch * kvlen + cur_kvlen];
      val = val > shared[cur_kvlen] ? val : shared[cur_kvlen];
    }
    __syncthreads();
    float max_val = blockReduceMax<float>(val);
    __shared__ float block_max;
    if (threadIdx.x == 0) {
      block_max = max_val;
    }
    __syncthreads();

    val = 0;
    for (int cur_kvlen = threadIdx.x; cur_kvlen < kvlen; cur_kvlen += blockDim.x) {
      val = val + expf(shared[cur_kvlen] - block_max);
    }
    float sum = blockReduceSum<float>(val);
    __shared__ float block_sum;
    if (threadIdx.x == 0) {
      block_sum = sum;
    }
    __syncthreads();

    for (int cur_kvlen = threadIdx.x; cur_kvlen < kvlen; cur_kvlen += blockDim.x) {
      attention[cur_batch * head * qlen * kvlen + cur_head * qlen * kvlen + cur_qlen * kvlen + cur_kvlen] =
          (float)(expf(shared[cur_kvlen] - block_max) / block_sum);
    }
  }
}
