#include "matmul.h"

#include <algorithm>
#include <cstdio>
#include <vector>

#include "source/operator/internal/internal_api.h"

namespace internal_ops {

// inline int broadcast_rule(const std::vector<int>& left, const std::vector<int>& right, std::vector<int>& output) {
//   int ret = 0;
//   // TODO
//   return ret;
// }

int MatMul::InferShape(const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape,
                       std::vector<int>& output_matrix_shape) const {
  int ret = 0;
  output_matrix_shape.clear();
  if (left_matrix_shape.empty() || right_matrix_shape.empty()) {
    printf("[Error] MatMul::InferShape, left_matrix_shape or right_matrix_shape is empty.\n");
    ret = -1;
    return ret;
  }
  // left和right都是1D的情况，输出的矩阵是Scalar，即output_martix_shape.empty() == true
  if (left_matrix_shape.size() == 1 && right_matrix_shape.size() == 1) {
    int left_k = left_matrix_shape[0];
    int right_k = right_matrix_shape[0];
    if (left_k != right_k) {
      printf("[Error] MatMul::InferShape, left_matrix_shape[0] != right_matrix_shape[0].\n");
      ret = -1;
    }
    return ret;
  }
  // left 1D && right ND的情况：left (K), right(P, K, N) ---> output(P, N))
  else if (left_matrix_shape.size() == 1 && right_matrix_shape.size() >= 2) {
    int left_k = left_matrix_shape[0];
    int right_k = right_matrix_shape[right_matrix_shape.size() - 2];
    int n = right_matrix_shape[right_matrix_shape.size() - 1];
    if (left_k != right_k) {
      printf(
          "[Error] MatMul::InferShape, left_matrix_shape[0] != right_matrix_shape[right_matrix_shape.size() - 2].\n");
      ret = -1;
      return ret;
    }
    output_matrix_shape.assign(right_matrix_shape.begin(), right_matrix_shape.end() - 1);
    output_matrix_shape[output_matrix_shape.size() - 1] = n;
    return ret;
  }
  // left ND && right 1D的情况：left (P, M, K), right(K) ---> output(P, M)
  else if (left_matrix_shape.size() >= 2 && right_matrix_shape.size() == 1) {
    int left_k = left_matrix_shape[left_matrix_shape.size() - 1];
    int m = left_matrix_shape[left_matrix_shape.size() - 2];
    int right_k = right_matrix_shape[0];
    if (left_k != right_k) {
      printf("[Error] MatMul::InferShape, left_matrix_shape[left_matrix_shape.size() - 1] != right_matrix_shape[0].\n");
      ret = -1;
      return ret;
    }
    output_matrix_shape.assign(left_matrix_shape.begin(), left_matrix_shape.end() - 1);
    return ret;
  }
  // left ND && right ND的情况，left (P1, M, K) , right (P2, K, N) ---> output(P3, M, N)
  else if (left_matrix_shape.size() >= 2 && right_matrix_shape.size() >= 2) {
    int left_k = left_matrix_shape[left_matrix_shape.size() - 1];
    int m = left_matrix_shape[left_matrix_shape.size() - 2];
    int n = right_matrix_shape[right_matrix_shape.size() - 1];
    int right_k = right_matrix_shape[right_matrix_shape.size() - 2];
    if (left_k != right_k) {
      printf("[Error] MatMul::InferShape, left_matrix_shape[left_matrix_shape.size() - 1] != right_matrix_shape[0].\n");
      ret = -1;
      return ret;
    }
    // P1 和 P2 两个序列必须满足broadcast的规则，通过broadcast的规则，可以得到 P3
    std::vector<int> P1;
    std::vector<int> P2;
    for (int i = 0; i < left_matrix_shape.size() - 2; i++) {
      P1.push_back(left_matrix_shape[i]);
    }
    for (int i = 0; i < right_matrix_shape.size() - 2; i++) {
      P2.push_back(right_matrix_shape[i]);
    }
    std::vector<int> P3;
    // ret = broadcast_rule(P1, P2, P3);
    P3.push_back(m);
    P3.push_back(n);
    return ret;
  }
  return ret;
}

// // right: 1D, left: 1D, output: Scalar
// // call cublasSdot()
// int MatMul::Compute1DWith1D(float* left_matrix, float* right_matrix, float* output_matrix,
//                             const std::vector<int>& left_matrix_shape,
//                             const std::vector<int>& right_matrix_shape) const {
//   int ret = 0;
//   int n = left_matrix_shape[0];
//   cublasStatus_t status = cublasSdot(handle_, n, left_matrix, 1, right_matrix, 1, output_matrix);
//   if (status != CUBLAS_STATUS_SUCCESS) {
//     printf("[Error] MatMul::Compute1DWith1D, cublasSdot failed.\n");
//     ret = -1;
//   }
//   return ret;
// }

// right: (m, k), left: (k, n), output: (m, n)
int MatMul::Compute2DWith2D(float* left_matrix, float* right_matrix, float* output_matrix,
                            const std::vector<int>& left_matrix_shape,
                            const std::vector<int>& right_matrix_shape) const {
  int ret = 0;
  int left_k = left_matrix_shape[left_matrix_shape.size() - 1];
  int m = 1;
  if (left_matrix_shape.size() > 1) {
    m = left_matrix_shape[left_matrix_shape.size() - 2];
  }
  int n = right_matrix_shape[right_matrix_shape.size() - 1];
  int right_k = 1;
  if (right_matrix_shape.size() > 1) {
    right_k = right_matrix_shape[right_matrix_shape.size() - 2];
  }
  if (left_k != right_k) {
    printf("[Error] MatMul::Compute2DWith2D, left_k != right_k.\n");
    ret = -1;
    return ret;
  }
  int k = left_k;
  int lda = left_k;
  int ldb = n;
  int ldc = n;
  float alpha = 1.0f;
  float beta = 0.0f;

  // column major, origin: output = left * right, now: output' = left' * right'
  cublasStatus_t status = cublasSgemm(handle_, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, right_matrix, ldb,
                                      left_matrix, lda, &beta, output_matrix, ldc);
  if (status != CUBLAS_STATUS_SUCCESS) {
    printf("[Error] MatMul::Compute2DWith2D, cublasSgemm failed.\n");
    ret = -1;
  }
  return ret;
}

// right: (batch, m, k), left: (k, n), output: (batch, m, n)
int MatMul::ComputeNDWith2D(float* left_matrix, float* right_matrix, float* output_matrix,
                            const std::vector<int>& left_matrix_shape,
                            const std::vector<int>& right_matrix_shape) const {
  int ret = 0;
  int left_k = left_matrix_shape[left_matrix_shape.size() - 1];
  int m = left_matrix_shape[left_matrix_shape.size() - 2];
  int batch = 1;
  for (int i = 0; i < left_matrix_shape.size() - 2; i++) {
    batch *= left_matrix_shape[i];
  }
  int n = right_matrix_shape[right_matrix_shape.size() - 1];
  int k = left_k;
  int lda = left_k;
  int ldb = n;
  int ldc = n;
  int stride_a = m * left_k;
  int stride_b = 0;
  int stride_c = m * n;
  float alpha = 1.0f;
  float beta = 0.0f;

  // column major, origin: output = left * right, now: output' = left' * right'
  cublasStatus_t status =
      cublasSgemmStridedBatched(handle_, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, right_matrix, ldb, stride_b,
                                left_matrix, lda, stride_a, &beta, output_matrix, ldc, stride_c, batch);

  if (status != CUBLAS_STATUS_SUCCESS) {
    printf("[Error] MatMul::ComputeNDWith2D, cublasSgemmStridedBatched failed.\n");
    ret = -1;
  }

  return ret;
}

// right: (m, k), left: (batch, k, n), output: (batch, m, n)
int MatMul::Compute2DWithND(float* left_matrix, float* right_matrix, float* output_matrix,
                            const std::vector<int>& left_matrix_shape,
                            const std::vector<int>& right_matrix_shape) const {
  int ret = 0;
  int left_k = left_matrix_shape[left_matrix_shape.size() - 1];
  int m = left_matrix_shape[left_matrix_shape.size() - 2];
  int n = right_matrix_shape[right_matrix_shape.size() - 1];
  int batch = 1;
  for (int i = 0; i < right_matrix_shape.size() - 2; i++) {
    batch *= right_matrix_shape[i];
  }
  int k = left_k;
  int lda = left_k;
  int ldb = n;
  int ldc = n;
  int stride_a = 0;
  int stride_b = left_k * n;
  int stride_c = m * n;
  float alpha = 1.0f;
  float beta = 0.0f;

  // column major, origin: output = left * right, now: output' = left' * right'
  cublasStatus_t status =
      cublasSgemmStridedBatched(handle_, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, right_matrix, ldb, stride_b,
                                left_matrix, lda, stride_a, &beta, output_matrix, ldc, stride_c, batch);
  if (status != CUBLAS_STATUS_SUCCESS) {
    printf("[Error] MatMul::Compute2DWithND, cublasSgemmStridedBatched failed.\n");
    ret = -1;
  }

  return ret;
}

// right: (batch, m, k), left: (batch, k, n), output: (batch, m, n)
int MatMul::ComputeNDWithND(float* left_matrix, float* right_matrix, float* output_matrix,
                            const std::vector<int>& left_matrix_shape,
                            const std::vector<int>& right_matrix_shape) const {
  int ret = 0;
  int left_k = left_matrix_shape[left_matrix_shape.size() - 1];
  int m = left_matrix_shape[left_matrix_shape.size() - 2];
  int n = right_matrix_shape[right_matrix_shape.size() - 1];
  int batch = 1;
  for (int i = 0; i < right_matrix_shape.size() - 2; i++) {
    batch *= right_matrix_shape[i];
  }
  int k = left_k;
  int lda = left_k;
  int ldb = n;
  int ldc = n;
  int stride_a = m * k;
  int stride_b = k * n;
  int stride_c = m * n;
  float alpha = 1.0f;
  float beta = 0.0f;
  cublasStatus_t status =
      cublasSgemmStridedBatched(handle_, CUBLAS_OP_N, CUBLAS_OP_N, n, m, k, &alpha, right_matrix, ldb, stride_b,
                                left_matrix, lda, stride_a, &beta, output_matrix, ldc, stride_c, batch);
  if (status != CUBLAS_STATUS_SUCCESS) {
    printf("[Error] MatMul::ComputeNDWithND, cublasSgemmStridedBatched failed.\n");
    ret = -1;
  }
  return ret;
}

int MatMul::ComputeNDWithNDWithPadding(float* left_matrix, float* right_matrix, float* output_matrix,
                                       const std::vector<int>& left_matrix_shape,
                                       const std::vector<int>& right_matrix_shape) const {
  return -1;
  // int ret = 0;
  // int k = left_matrix_shape[left_matrix_shape.size() - 1];
  // int m = left_matrix_shape[left_matrix_shape.size() - 2];
  // int n = right_matrix_shape[right_matrix_shape.size() - 1];
  // std::vector<int> left_stack;
  // std::vector<int> right_stack;
  // std::vector<int> broadcast_stack;
  // for (int i = 0; i < left_matrix_shape.size() - 2; i++) {
  //   left_stack.push_back(left_matrix_shape[i]);
  // }
  // for (int i = 0; i < right_matrix_shape.size() - 2; i++) {
  //   right_stack.push_back(right_matrix_shape[i]);
  // }
  // broadcast_rule(left_stack, right_stack, broadcast_stack);

  // std::vector<int> left_matrix_shape_padded(broadcast_stack.begin(), broadcast_stack.end());
  // left_matrix_shape_padded.push_back(m);
  // left_matrix_shape_padded.push_back(k);
  // std::vector<int> right_matrix_shape_padded(broadcast_stack.begin(), broadcast_stack.end());
  // right_matrix_shape_padded.push_back(k);
  // right_matrix_shape_padded.push_back(n);

  // int left_matrix_padded_size = 1;
  // int right_matrix_padded_size = 1;
  // for (auto left_shape : left_matrix_shape_padded) {
  //   left_matrix_padded_size *= left_shape;
  // }
  // for (auto right_shape : right_matrix_shape_padded) {
  //   right_matrix_padded_size *= right_shape;
  // }

  // // padding
  // // TODO

  // ret = ComputeNDWithND(left_matrix_padded, right_matrix_padded, output_matrix, left_matrix_shape_padded,
  //                 right_matrix_shape_padded);

  // return ret;
}

int MatMul::Compute(float* left_matrix, float* right_matrix, float* output_matrix,
                    const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape) const {
  int ret = 0;
  if (left_matrix_shape.size() <= 2 && right_matrix_shape.size() <= 2) {
    return Compute2DWith2D(left_matrix, right_matrix, output_matrix, left_matrix_shape, right_matrix_shape);
  }
  if (left_matrix_shape.size() <= 2 && right_matrix_shape.size() > 2) {
    return Compute2DWithND(left_matrix, right_matrix, output_matrix, left_matrix_shape, right_matrix_shape);
  }
  if (left_matrix_shape.size() > 2 && right_matrix_shape.size() <= 2) {
    return ComputeNDWith2D(left_matrix, right_matrix, output_matrix, left_matrix_shape, right_matrix_shape);
  }
  // no padding, left_matrix_shape[:-2] == right_matrix_shape[:-2]
  bool is_no_padding = true;
  int left_dim = left_matrix_shape.size();
  int right_dim = right_matrix_shape.size();
  std::vector<int> short_shape(left_matrix_shape.begin(), left_matrix_shape.end() - 2);
  std::vector<int> long_shape(right_matrix_shape.begin(), right_matrix_shape.end() - 2);
  if (right_dim < left_dim) {
    std::swap(short_shape, long_shape);
  }
  // 靠右对齐shape，对比每个维度是否相等
  for (int i = 0; i < short_shape.size(); i++) {
    if (short_shape[short_shape.size() - i - 1] != long_shape[long_shape.size() - i - 1]) {
      is_no_padding = false;
      break;
    }
  }
  for (int i = short_shape.size(); i < long_shape.size(); i++) {
    if (long_shape[long_shape.size() - i - 1] != 1) {
      is_no_padding = false;
      break;
    }
  }

  if (is_no_padding) {
    return ComputeNDWithND(left_matrix, right_matrix, output_matrix, left_matrix_shape, right_matrix_shape);
  } else {
    return ComputeNDWithNDWithPadding(left_matrix, right_matrix, output_matrix, left_matrix_shape, right_matrix_shape);
  }

  return ret;
}

}  // namespace internal_ops
