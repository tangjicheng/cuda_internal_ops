#pragma once

#include "enum.h"

__global__ void gReduceMean(float* out, const float* in, int rows, int cols);

__global__ void gReduceSum(float* out, const float* in, int rows, int cols);

__global__ void gReduceSumVec(float* out, const float* in, int rows, int cols);

__global__ void gReduceGeneral(float* out, float* in, int outside, int inside, int insideStride, int limit, int axis,
                               int reduce_type);

__global__ void gTopkGeneral(float* out_value, float* out_index, const float* in, int outside, int inside,
                             int insideStride, int limit, int k, int largest, int sorted);

__global__ void gRange(float* out, float start, float delta, int number_of_element, int rows, int cols);

__global__ void gContinuous(float* out, const float* in, const float* in_shape, const float* in_stride, int in_dim,
                            int rows, int cols);