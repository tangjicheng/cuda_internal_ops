#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/gather.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>

#include "source/operator/internal/reduce.h"

__forceinline__ __device__ float warpReduceSum(float val) {
  for (int laneMask = 1; laneMask < 32; laneMask <<= 1) {
    val += __shfl_xor_sync(0xffffffff, val, laneMask, 32);
  }
  return val;
}

__forceinline__ __device__ float blockReduceSum(float val) {
  static __shared__ float shared[32];
  int lane = threadIdx.x & 0x1f;
  int warpId = threadIdx.x >> 5;
  val = warpReduceSum(val);
  if (lane == 0) {
    shared[warpId] = val;
  }
  __syncthreads();
  val = (threadIdx.x < ((blockDim.x + 31) >> 5)) ? shared[lane] : 0.0f;
  val = warpReduceSum(val);
  return val;
}

__global__ void gReduceSum(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    float val = 0.0f;
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      val += in[i * cols + j];
    }
    __syncthreads();
    val = blockReduceSum(val);

    if (threadIdx.x == 0) {
      out[i] = val;
    }
  }

  return;
}

__global__ void gReduceMean(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    float val = 0.0f;
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      val += in[i * cols + j];
    }
    __syncthreads();
    val = blockReduceSum(val);

    if (threadIdx.x == 0) {
      out[i] = val / cols;
    }
  }

  return;
}

__global__ void gReduceSumVec(float* out, const float* in, int rows, int cols) {
  for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
    out[tid] = 0.0f;
    for (int i = 0; i < rows; ++i) {
      out[tid] += in[i * cols + tid];
    }
  }
}

__global__ void gReduceGeneral(float* out, float* in, int outside, int inside, int insideStride, int limit, int axis,
                               int reduce_type) {
  for (int i = blockIdx.x; i < outside; i += gridDim.x) {
    extern __shared__ float _share[];
    float* _share_value = _share;

    float* inCurrent = in + i * insideStride;
    float* outCurrent = out + i * inside;

    for (int j = threadIdx.x; j < inside; j += blockDim.x) {
      float& value = _share_value[j];

      switch (reduce_type) {
        case 0:  // mean
        {
          value = 0.0f;
          for (int inum = 0; inum < limit; inum++) {
            value += inCurrent[inum * inside + j];
          }
          value = value / limit;
          break;
        }
        case 1:  // max
        {
          value = -99999999.0f;
          for (int inum = 0; inum < limit; inum++) {
            float compare = inCurrent[inum * inside + j];
            if (compare > value) {
              value = compare;
            }
          }
          break;
        }
        case 2:  // min
        {
          value = 99999999.0f;
          for (int inum = 0; inum < limit; inum++) {
            float compare = inCurrent[inum * inside + j];
            if (compare < value) {
              value = compare;
            }
          }
          break;
        }
        case 3:  // sum
        {
          value = 0.0f;
          for (int inum = 0; inum < limit; inum++) {
            value += inCurrent[inum * inside + j];
          }
          break;
        }
        case 4:  // Prod
        {
          value = 1.0f;
          for (int inum = 0; inum < limit; inum++) {
            value *= inCurrent[inum * inside + j];
          }
          break;
        }
      }

      outCurrent[j] = value;
    }
  }
  return;
}

__global__ void gTopkGeneral(float* out_value, float* out_index, const float* in, int outside, int inside,
                             int insideStride, int limit, int k, int largest, int sorted) {
  for (int i = blockIdx.x; i < outside; i += gridDim.x) {
    extern __shared__ float _share[];

    const float* inCurrent = in + i * insideStride;
    float* outValueCurrent = out_value + i * inside * k;
    float* outIndexCurrent = out_index + i * inside * k;

    for (int j = threadIdx.x; j < inside; j += blockDim.x) {
      float* _share_data = _share + j * limit * 4;
      float* _share_index_ori = _share_data + limit;
      float* _share_value = _share_data + limit * 2;
      float* _share_index_new = _share_data + limit * 3;
      for (int inum = 0; inum < limit; inum++) {
        _share_data[inum] = inCurrent[inum * inside + j];
        _share_index_ori[inum] = inum;
      }

      for (int idx = 0; idx < limit; idx++) {
        int max_val = _share_data[idx];
        int max_idx = idx;
        for (int ilen = idx + 1; ilen < limit; ilen++) {
          if (largest) {
            if (_share_data[ilen] > max_val) {
              max_val = _share_data[ilen];
              max_idx = ilen;
            }
          } else {
            if (_share_data[ilen] < max_val) {
              max_val = _share_data[ilen];
              max_idx = ilen;
            }
          }
        }

        float temp = _share_data[idx];
        _share_value[idx] = max_val;
        _share_index_new[idx] = _share_index_ori[max_idx];

        _share_data[max_idx] = temp;
        _share_index_ori[max_idx] = idx;
      }

      if (sorted) {
        for (int idx = 0; idx < k; idx++) {
          outValueCurrent[idx] = _share_value[idx];
          outIndexCurrent[idx] = _share_index_new[idx];
        }
      } else {
        float temp_value = _share_value[k - 1];
        int num_great = 0;
        for (int idx = 0; idx < limit; idx++) {
          if (largest) {
            if (num_great < k && inCurrent[idx * inside + j] >= temp_value) {
              outValueCurrent[num_great] = inCurrent[idx * inside + j];
              outIndexCurrent[num_great] = idx;
              num_great++;
            }
          } else {
            if (num_great < k && inCurrent[idx * inside + j] <= temp_value) {
              outValueCurrent[num_great] = inCurrent[idx * inside + j];
              outIndexCurrent[num_great] = idx;
              num_great++;
            }
          }
        }
      }
    }
  }
  return;
}

__global__ void gRange(float* out, float start, float delta, int number_of_element, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    int index = j * blockDim.x + threadIdx.x;
    if (index < number_of_element) {
      out[index] = start + index * delta;
    }
  }
  return;
}

__global__ void gContinuous(float* out, const float* in, const float* in_shape, const float* in_stride, int in_dim,
                            int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    extern __shared__ int _shared[];
    int rows_id = blockIdx.x + bid;
    for (int tid = 0; tid < cols; tid += blockDim.x) {
      int cols_id = threadIdx.x + tid;
      int out_index = rows_id * cols + cols_id;

      // 保存in的当前position，每个thread需要大小为in_dim的数组（共享内存空间）
      int* cur_position = _shared + threadIdx.x * in_dim;
      // float* cur_position = index_buf + (blockIdx.x * blockDim.x + threadIdx.x) * in_dim;

      // 计算in_index
      int in_index = out_index;
      for (int i = 0; i < in_dim; i++) {
        cur_position[in_dim - 1 - i] = in_index % (int)(in_shape[in_dim - 1 - i]);
        in_index = in_index / (int)(in_shape[in_dim - 1 - i]);
      }
      in_index = 0;
      for (int i = 0; i < in_dim; i++) {
        in_index += cur_position[i] * in_stride[i];
      }

      out[out_index] = in[in_index];
    }
  }
  return;
}