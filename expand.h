// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Expand算子的实现

#pragma once

#include <vector>

namespace internal_ops {

int ExpandImpl(float* output, const float* input, const float* expand_shape, const std::vector<int>& input_shape,
               int expand_shape_size, cudaStream_t stream);

}  // namespace internal_ops