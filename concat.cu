// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Concat算子的实现
// 按照onnx operator文档中的Concat算子的描述进行实现

#include "cuda_params.h"
#include "cuda_utils.h"
#include "source/ndarray/ndarray.h"
#include "source/operator/internal/concat.h"

namespace internal_ops {

__global__ void concat_kernel(float* output, int64_t* input_vec, int axis, int* concat_size_buf,
                              int* concat_size_range_buf, int* concat_index_map_buf, const fast_divmod block_size_fast,
                              const fast_divmod output_block_size_with_axis_fast, int output_size,
                              int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int output_index = (blockIdx.x + bid) * blockDim.x + threadIdx.x;
    if (output_index >= output_size) {
      return;
    }
    int outer_block_id = 0;
    int output_block_offset = 0;
    int axis_block_id = 0;
    int block_offset = 0;

    // output_index        / output_block_size_with_axis  --除数和余数--> (outer_block_id, output_block_offset)
    // output_block_offset / block_size                   --除数和余数--> (axis_block_id, block_offset)
    output_block_size_with_axis_fast.divmod(output_index, outer_block_id, output_block_offset);
    block_size_fast.divmod(output_block_offset, axis_block_id, block_offset);

    int which_input = concat_index_map_buf[axis_block_id];
    int input_left = (which_input == 0) ? 0 : concat_size_range_buf[which_input - 1];
    int cur_input_id_axis = axis_block_id - input_left;

    int input_index =
        (outer_block_id * concat_size_buf[which_input] + cur_input_id_axis) * block_size_fast.d_ + block_offset;
    output[output_index] = (reinterpret_cast<float*>(input_vec[which_input]))[input_index];
  }
  return;
}

__global__ void simple_1d_concat(float* output, int64_t* input_vec, int output_size, int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int output_index = (blockIdx.x + bid) * blockDim.x + threadIdx.x;
    if (output_index >= output_size) {
      return;
    }
    output[output_index] = (reinterpret_cast<float*>(input_vec[output_index]))[0];
  }
  return;
}

// 特殊情况，如果每个输入都是1维的。例如，
// input: [3], [5], [7]   --concat-->  output: [3, 5, 7]
int Simple1DConcatImpl(float* output, const std::vector<int64_t>& input,
                       const std::vector<std::vector<int>>& input_shape_vec, int axis, cudaStream_t stream) {
  int ret = 0;
  int64_t* input_ptr_buf = nullptr;
  GetBuf(input_ptr_buf, input.size(), int64_t, buf_1);

  cudaMemcpyAsync(input_ptr_buf, input.data(), input.size() * sizeof(int64_t), cudaMemcpyHostToDevice, stream);

  int input_count = input_shape_vec.size();
  // concat 1d, 输入的个数往往比较少，因此设置threads_per_block为32，以提高性能。
  int threads_per_block = 32;
  int required_blocks = input_count / threads_per_block + 1;
  int blocks_per_grid = std::min(required_blocks, MAX_BLOCKS);

  simple_1d_concat<<<threads_per_block, blocks_per_grid, 0, stream>>>(output, input_ptr_buf, input_count,
                                                                      required_blocks);
  return ret;
}

int ConcatImpl(float* output, const std::vector<int64_t>& input, const std::vector<std::vector<int>>& input_shape_vec,
               int axis, cudaStream_t stream) {
  int ret = 0;

  if (input_shape_vec[0].size() == 1 && input_shape_vec[0][0] == 1) {
    ret = Simple1DConcatImpl(output, input, input_shape_vec, axis, stream);
    return ret;
  }

  // 计算concat必要的参数。这里的block指的是一块连续的数据，和cuda的线程块没有关系。
  // block_size: 从axis+1维度，到最后一个维度，包含的元素个数
  // output_block_size_axis: output的从axis维度到最后一个维度，包含元素的个数
  // num_blocks: 上述output_block的个数
  // concat_size: 每个input的axis维度的大小
  // concat_size_range: 每个input的axis维度上，包含本身以及在其之前input的大小
  // concat_index_map: 在axis维度上，output的index所对应的是第几个input
  int input_count = input.size();
  auto input_0_shape = input_shape_vec[0];
  int block_size = 1;
  int num_blocks = 1;
  std::vector<int> concat_size(input_count, 0);
  int output_axis_dim = 0;
  for (int i = axis + 1; i < input_0_shape.size(); ++i) {
    block_size *= input_0_shape[i];
  }
  for (int i = 0; i < axis; i++) {
    num_blocks *= input_0_shape[i];
  }
  for (int i = 0; i < input_count; i++) {
    concat_size[i] = input_shape_vec[i][axis];
    output_axis_dim += concat_size[i];
  }
  std::vector<int> concat_size_range(concat_size);
  for (int i = 1; i < concat_size_range.size(); i++) {
    concat_size_range[i] += concat_size_range[i - 1];
  }
  std::vector<int> concat_index_map(output_axis_dim, 0);
  int kk = 0;
  for (int i = 0; i < input_count; i++) {
    for (int j = 0; j < concat_size[i]; j++) {
      concat_index_map[kk] = i;
      kk += 1;
    }
  }

  int output_block_size_with_axis = output_axis_dim * block_size;
  int output_size = num_blocks * output_block_size_with_axis;

  // TShape input_ptr_tshape(1);
  // TShape concat_size_tshape(1);
  // TShape concat_size_range_tshape(1);
  // TShape concat_index_map_tshape(1);
  // input_ptr_tshape[0] = input.size() * sizeof(int64_t) / sizeof(float);
  // concat_size_tshape[0] = concat_size.size() * sizeof(int) / sizeof(float);
  // concat_size_range_tshape[0] = concat_size_range.size() * sizeof(int) / sizeof(float);
  // concat_index_map_tshape[0] = concat_index_map.size() * sizeof(int) / sizeof(float);

  // NDArray input_ptr_ndarray(input_ptr_tshape);
  // NDArray concat_size_ndarray(concat_size_tshape);
  // NDArray concat_size_range_ndarray(concat_size_range_tshape);
  // NDArray concat_index_map_ndarray(concat_index_map_tshape);

  // int64_t* input_ptr_buf = reinterpret_cast<int64_t*>(input_ptr_ndarray.data());
  // int* concat_size_buf = reinterpret_cast<int*>(concat_size_ndarray.data());
  // int* concat_size_range_buf = reinterpret_cast<int*>(concat_size_range_ndarray.data());
  // int* concat_index_map_buf = reinterpret_cast<int*>(concat_index_map_ndarray.data());

  int64_t* input_ptr_buf = nullptr;
  int* concat_size_buf = nullptr;
  int* concat_size_range_buf = nullptr;
  int* concat_index_map_buf = nullptr;
  GetBuf(input_ptr_buf, input.size(), int64_t, buf_1);
  GetBuf(concat_size_buf, concat_size.size(), int, buf_2);
  GetBuf(concat_size_range_buf, concat_size_range.size(), int, buf_3);
  GetBuf(concat_index_map_buf, concat_index_map.size(), int, buf_4);

  cudaMemcpyAsync(input_ptr_buf, input.data(), input.size() * sizeof(int64_t), cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(concat_size_buf, concat_size.data(), concat_size.size() * sizeof(int), cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(concat_size_range_buf, concat_size_range.data(), concat_size_range.size() * sizeof(int),
             cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(concat_index_map_buf, concat_index_map.data(), concat_index_map.size() * sizeof(int),
             cudaMemcpyHostToDevice, stream);

  fast_divmod block_size_fast(block_size);
  fast_divmod output_block_size_with_axis_fast(output_block_size_with_axis);

  // 为了尽可能并行化，使用最大线程数。blocks_per_grid的大小由线程数和output_size决定。
  int threads_per_block = MAX_THREADS;
  int required_blocks = output_size / threads_per_block + 1;
  int blocks_per_grid = std::min(required_blocks, MAX_BLOCKS);

  concat_kernel<<<blocks_per_grid, threads_per_block, 0, stream>>>(
      output, input_ptr_buf, axis, concat_size_buf, concat_size_range_buf, concat_index_map_buf, block_size_fast,
      output_block_size_with_axis_fast, output_size, required_blocks);

  return ret;
}
}  // namespace internal_ops