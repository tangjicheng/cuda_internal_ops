#pragma once

#include <cstdio>
#include <ctime>

#include "cudnn.h"

namespace internal_ops {

inline double now_ms(void) {
  struct timespec res;
  clock_gettime(CLOCK_REALTIME, &res);
  return 1000.0 * res.tv_sec + (double)res.tv_nsec / 1e6;
}

inline void check(const char* file, int line, cudnnStatus_t t) {
  if (t != CUDNN_STATUS_SUCCESS) {
    fprintf(stderr, "[%s] %d: cudnn err: %s\n", file, line, cudnnGetErrorString(t));
  }
  return;
}

#define CHECK_CUDNN(x) \
  { check(__FILE__, __LINE__, x); }

}  // namespace internal_ops
