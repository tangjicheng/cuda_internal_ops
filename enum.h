#pragma once

namespace internal_ops {

enum Symbol {
  plus = 0,
  minus = 1,
  times = 2,
  division = 3,
  pow = 4,
};

typedef enum ReduceType { ReduceMean, ReduceMax, ReduceMin, ReduceSum, ReduceProd } tagReduceType;

}  // namespace internal_ops