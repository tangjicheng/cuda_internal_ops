// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// MatMul算子的实现

#pragma once

#include <cublas_v2.h>
#include <stdio.h>
#include <vector>

namespace internal_ops {

class MatMul {
 public:
  MatMul() { cublasCreate(&handle_); }
  ~MatMul() { cublasDestroy(handle_); }
  MatMul(const MatMul&) = delete;
  MatMul& operator=(const MatMul&) = delete;
  MatMul(MatMul&&) = delete;
  MatMul& operator=(MatMul&&) = delete;

  int InferShape(const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape,
                 std::vector<int>& output_matrix_shape) const;

  int Compute(float* left_matrix, float* right_matrix, float* output_matrix, const std::vector<int>& left_matrix_shape,
              const std::vector<int>& right_matrix_shape) const;

 private:
  int Compute2DWith2D(float* left_matrix, float* right_matrix, float* output_matrix,
                      const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape) const;

  int ComputeNDWith2D(float* left_matrix, float* right_matrix, float* output_matrix,
                      const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape) const;

  int Compute2DWithND(float* left_matrix, float* right_matrix, float* output_matrix,
                      const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape) const;

  int ComputeNDWithND(float* left_matrix, float* right_matrix, float* output_matrix,
                      const std::vector<int>& left_matrix_shape, const std::vector<int>& right_matrix_shape) const;

  int ComputeNDWithNDWithPadding(float* left_matrix, float* right_matrix, float* output_matrix,
                                 const std::vector<int>& left_matrix_shape,
                                 const std::vector<int>& right_matrix_shape) const;

 private:
  cublasHandle_t handle_;
};
}  // namespace internal_ops