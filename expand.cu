// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// Expand算子的实现

#include "source/operator/internal/expand.h"

#include "cuda_params.h"
#include "cuda_utils.h"
#include "source/ndarray/ndarray.h"

namespace internal_ops {

// 实现方式与broadcast_kernel_general类似
// input_stride和output_stride必须都是长度都为dim
template <int ThreadsPerBlock, int ElementsPerThread>
__global__ void expand_kernel(float* output, const float* input, const int* input_stride, const int* output_stride,
                              int output_size, int dim, int required_blocks) {
  for (int bid = 0; bid < required_blocks; bid += gridDim.x) {
    int this_thread_start_index = (blockIdx.x + bid) * ThreadsPerBlock * ElementsPerThread + threadIdx.x;
    float input_value_reg[ElementsPerThread];
    int current_output_index = this_thread_start_index;

    // 取input的数据到寄存器
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      // 计算input_index
      int input_index = 0;
      int offset = current_output_index;
#pragma unroll
      for (int k = 0; k < dim; k++) {
        int q, r;
        q = offset / output_stride[k];
        r = offset % output_stride[k];
        input_index += q * input_stride[k];
        offset = r;
      }
      // 取数
      if (current_output_index < output_size) {
        input_value_reg[i] = input[input_index];
      }
      current_output_index += ThreadsPerBlock;
    }

    // 写入到output的显存
    current_output_index = this_thread_start_index;
#pragma unroll
    for (int i = 0; i < ElementsPerThread; i++) {
      if (current_output_index < output_size) {
        output[current_output_index] = input_value_reg[i];
      }
      current_output_index += ThreadsPerBlock;
    }
  }
}

int ExpandImpl(float* output, const float* input, const float* expand_shape, const std::vector<int>& input_shape,
               int expand_shape_size, cudaStream_t stream) {
  std::vector<float> expand_shape_float_vec(expand_shape_size, 0.0f);
  cudaMemcpyAsync(expand_shape_float_vec.data(), expand_shape, expand_shape_size * sizeof(float), cudaMemcpyDeviceToHost, stream);
  std::vector<int> expand_shape_vec(expand_shape_size, 0);
  for (int i = 0; i < expand_shape_size; ++i) {
    expand_shape_vec[i] = static_cast<int>(expand_shape_float_vec[i]);
  }

  // 计算broadcast后的input_shape和output_shape
  std::vector<int> output_shape;
  std::vector<int> input_shape_padded;
  std::vector<int> expand_shape_padded;

  int is_broadcast_success =
      broadcast_rule(input_shape, expand_shape_vec, output_shape, input_shape_padded, expand_shape_padded);
  if (is_broadcast_success != 0) {
    printf("[Error] ExpandImpl: broadcast error\n");
    return is_broadcast_success;
  }
  int output_size = 1;
  for (const auto& iter : output_shape) {
    output_size *= iter;
  }

  // 计算broadcast后的output_stride，以及，与output_stride对齐长度的input_stride
  std::vector<int> input_stride_padded = shape_to_stride(input_shape_padded);
  std::vector<int> output_stride_padded = shape_to_stride(output_shape);
  int dim = output_shape.size();

  if (input_stride_padded.size() != dim || output_stride_padded.size() != dim || input_shape_padded.size() != dim) {
    printf("[Error] ExpandImpl: expand error\n");
    return -1;
  }
  for (int i = 0; i < output_stride_padded.size(); ++i) {
    if (input_shape_padded[i] != output_shape[i]) {
      input_stride_padded[i] = 0;
    }
  }

  // 拷贝stride数据进入显存
  int* input_stride_buf = nullptr;
  int* output_stride_buf = nullptr;
  GetBuf(input_stride_buf, input_stride_padded.size(), int, buf_1);
  GetBuf(output_stride_buf, output_stride_padded.size(), int, buf_2);
  cudaMemcpyAsync(input_stride_buf, input_stride_padded.data(), input_stride_padded.size() * sizeof(int),
             cudaMemcpyHostToDevice, stream);
  cudaMemcpyAsync(output_stride_buf, output_stride_padded.data(), output_stride_padded.size() * sizeof(int),
             cudaMemcpyHostToDevice, stream);

  // 计算需要的cuda_blocks
  int elements_per_block = EXPAND_THREADS_PER_BLOCK * EXPAND_ELEMENTS_PER_THREAD;
  int required_blocks = output_size / elements_per_block + 1;
  int cuda_blocks = std::min(MAX_BLOCKS, required_blocks);

  // 执行cuda kernel
  expand_kernel<EXPAND_THREADS_PER_BLOCK, EXPAND_ELEMENTS_PER_THREAD>
      <<<cuda_blocks, EXPAND_THREADS_PER_BLOCK, 0, stream>>>(output, input, input_stride_buf, output_stride_buf,
                                                             output_size, dim, required_blocks);
  return 0;
}

}  // namespace internal_ops