#pragma once

#include <stdio.h>
#include <string>
#include <vector>

#include "cudnn.h"
#include "source/ndarray/ndarray.h"
#include "utils.h"

namespace internal_ops {

class Pooling {
 public:
  Pooling() : mode_(CUDNN_POOLING_MAX) {}

  int Fini() {
    int ret = 0;
    CHECK_CUDNN(cudnnDestroy(handle_));
    CHECK_CUDNN(cudnnDestroyPoolingDescriptor(poolingDesc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(xDesc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(yDesc_));
    return ret;
  }

  int Init(std::string mode = "max") {
    int ret = 0;
    CHECK_CUDNN(cudnnCreate(&handle_));
    CHECK_CUDNN(cudnnCreatePoolingDescriptor(&poolingDesc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&xDesc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&yDesc_));
    if (mode == "avg") {
      mode_ = CUDNN_POOLING_AVERAGE_COUNT_INCLUDE_PADDING;
    }
    return ret;
  }

  int pooling2D(NDArray* y, const NDArray* x, int win_h, int win_w, int stride_h = 1, int stride_w = 1, int pad_h = 1,
                int pad_w = 1) {
    int ret = 0;

    const TShape& tx = x->shape();
    if (tx.ndim() != 4) {
      printf("dim error! x:%d\n", tx.ndim());
    }

    float alpha = 1.0f;
    float beta = 0.0f;

    cudnnNanPropagation_t prop = CUDNN_NOT_PROPAGATE_NAN;

    int dim = 2;
    int stride[2] = {stride_h, stride_w};
    int pad[2] = {pad_h, pad_w};
    int win[2] = {win_h, win_w};
    CHECK_CUDNN(cudnnSetPoolingNdDescriptor(poolingDesc_, mode_, prop, dim, win, pad, stride));

    int xShape[4] = {(int)tx[0], (int)tx[1], (int)tx[2], (int)tx[3]};
    int yShape[4] = {(int)tx[0], (int)tx[1], (int)(tx[2] + 2 * pad_h - win_h) / stride[0] + 1,
                     (int)(tx[3] + 2 * pad_w - win_w) / stride[1] + 1};

    int xDim = 4;
    int yDim = 4;
    int xStride[4] = {xShape[1] * xShape[2] * xShape[3], xShape[2] * xShape[3], xShape[3], 1};
    int yStride[4] = {yShape[1] * yShape[2] * yShape[3], yShape[2] * yShape[3], yShape[3], 1};
    // std::vector<int> ys = {yShape[0], yShape[1], yShape[2], yShape[3]};
    // y->resize(ys);

    CHECK_CUDNN(cudnnSetTensorNdDescriptor(xDesc_, CUDNN_DATA_FLOAT, xDim, xShape, xStride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(yDesc_, CUDNN_DATA_FLOAT, yDim, yShape, yStride));

    CHECK_CUDNN(cudnnPoolingForward(handle_, poolingDesc_, &alpha, xDesc_, x->data(), &beta, yDesc_, y->data()));

    return ret;
  }

 private:
  cudnnPoolingMode_t mode_;
  cudnnHandle_t handle_;
  cudnnPoolingDescriptor_t poolingDesc_;
  cudnnTensorDescriptor_t xDesc_;
  cudnnTensorDescriptor_t yDesc_;
};

}  // namespace internal_ops
