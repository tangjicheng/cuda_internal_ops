#pragma once

__global__ void gCopyRows(float* out, const float* in, const int* src_ids, int nums, int cols);

__global__ void gCopyRows(float* out, const float* in, const float* src_ids, int nums, int cols);

__global__ void gLowTriangleVec(float* out, const float* in, int idx, int cols);

__global__ void gTranspose(float* out, const float* in, int rows, int cols, int* out_trans, int* pre_mul,
                           int* out_shape, int dim);

__global__ void gTranspose3dZYX2ZXYAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx);

__global__ void gTranspose3dZYX2XYZAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx);

__global__ void gTranspose3dZYX2YZXAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx);

__global__ void gTranspose3dZYX2YXZAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx);

__global__ void gTranspose3dZYX2XZYAlgo0(float* out, const float* in, const int ndimz, const int ndimy,
                                         const int ndimx);

__global__ void gConcat3DY(float* out, const float* in1, const float* in2, int dimX, int dimY1, int dimY2, int dimZ);

__global__ void gSum(float* out, const float* in, int rows, int cols, int y);
