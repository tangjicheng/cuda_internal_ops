#pragma once

#include <stdio.h>
#include <vector>
#include "cudnn.h"
#include "source/ndarray/ndarray.h"
#include "utils.h"

namespace internal_ops {

class BatchNorm {
 public:
  BatchNorm() : mode_(CUDNN_BATCHNORM_SPATIAL), epsilon_(0.001) {}

  ~BatchNorm() {}

  int Fini() {
    int ret = 0;
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(xDesc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(yDesc_));
    CHECK_CUDNN(cudnnDestroyTensorDescriptor(bnDesc_));
    CHECK_CUDNN(cudnnDestroy(handle_));
    return ret;
  }

  int Init(const NDArray* scale, const NDArray* bias, const NDArray* mean, const NDArray* var, float epsilon = 0.001f) {
    int ret = 0;
    scale_ = scale;
    bias_ = bias;
    mean_ = mean;
    var_ = var;

    epsilon_ = epsilon;
    if (epsilon_ < CUDNN_BN_MIN_EPSILON) {
      epsilon_ = CUDNN_BN_MIN_EPSILON;
    }

    CHECK_CUDNN(cudnnCreate(&handle_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&xDesc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&yDesc_));
    CHECK_CUDNN(cudnnCreateTensorDescriptor(&bnDesc_));

    return ret;
  }

  int BatchNorm2D(NDArray* y, const NDArray* x) {
    int ret = 0;

    const TShape& tx = x->shape();
    if (tx.ndim() != 4) {
      printf("dim error! x:%d\n", tx.ndim());
    }

    float alpha = 1.0f;
    float beta = 0.0f;

    int xDim = 4;
    int yDim = 4;
    int bnDim = 4;
    int xShape[4] = {(int)tx[0], (int)tx[1], (int)tx[2], (int)tx[3]};
    int yShape[4] = {(int)tx[0], (int)tx[1], (int)tx[2], (int)tx[3]};
    int bnShape[4] = {1, (int)tx[1], 1, 1};
    int xStride[4] = {xShape[1] * xShape[2] * xShape[3], xShape[2] * xShape[3], xShape[3], 1};
    int yStride[4] = {yShape[1] * yShape[2] * yShape[3], yShape[2] * yShape[3], yShape[3], 1};
    int bnStride[4] = {bnShape[1] * bnShape[2] * bnShape[3], bnShape[2] * bnShape[3], bnShape[3], 1};
    std::vector<int> ys = {yShape[0], yShape[1], yShape[2], yShape[3]};
    // y->resize(ys);

    CHECK_CUDNN(cudnnSetTensorNdDescriptor(xDesc_, CUDNN_DATA_FLOAT, xDim, xShape, xStride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(yDesc_, CUDNN_DATA_FLOAT, yDim, yShape, yStride));
    CHECK_CUDNN(cudnnSetTensorNdDescriptor(bnDesc_, CUDNN_DATA_FLOAT, bnDim, bnShape, bnStride));

    CHECK_CUDNN(cudnnBatchNormalizationForwardInference(handle_, mode_, &alpha, &beta, xDesc_, x->data(), yDesc_,
                                                        y->data(), bnDesc_, scale_->data(), bias_->data(),
                                                        mean_->data(), var_->data(), epsilon_));

    return ret;
  }

 private:
  cudnnBatchNormMode_t mode_;
  double epsilon_;

  cudnnHandle_t handle_;
  cudnnTensorDescriptor_t xDesc_;
  cudnnTensorDescriptor_t yDesc_;
  cudnnTensorDescriptor_t bnDesc_;

  const NDArray* scale_;
  const NDArray* bias_;
  const NDArray* mean_;
  const NDArray* var_;
};

}  // namespace internal_ops