#pragma once

__global__ void gPosEmb(float* out, int rows, int cols, float min_timescale, float log_increment);

__global__ void gLayerNorm(float* out, const float* in, const float* scales, const float* biases, int rows, int cols);

__global__ void gBidirectionalRelativePositionBucket(int row, int col, const float* weight, int num_bucket, int dim,
                                                     int max_distance, float* out);

__global__ void gUnidirectionalRelativePositionBucket(int row, int col, const float* weight, int num_bucket, int dim,
                                                      int max_distance, float* out);

__global__ void gLayerVarNorm(const float* in, int row, int col, float eps, const float* scale, float* out);

__global__ void gAttentionMaskRelativePositionEmbeddingSoftmax(int batch, int head, int qlen, int kvlen,
                                                               const float* mask, const float* emb, float* attention);