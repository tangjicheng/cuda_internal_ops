// Copyright 2022 (c) Tencent Inc. All rights reserved.
//
// topk算子的接口

#pragma once

#include "cuda_runtime.h"
#include "source/ndarray/ndarray.h"

namespace internal_ops {
void BitonicTopk(NDArray* out_value, NDArray* out_index, const NDArray* in, int32_t k, int32_t axis, int32_t largest,
                 int32_t sorted, cudaStream_t stream);
}
