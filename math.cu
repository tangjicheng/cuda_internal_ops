#include "math.h"

__global__ void gAbs(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = fabsf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAcos(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = acosf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAcosh(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = acoshf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAsin(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = asinf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAsinh(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = asinhf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAtan(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = atanf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAtanh(float* out, const float* in, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = atanhf(in[i * cols + j]);
    }
  }

  return;
}

__global__ void gAdd(float* out, const float* in1, const float* in2, int rows, int cols) {
  for (int i = blockIdx.x; i < rows; i += gridDim.x) {
    for (int j = threadIdx.x; j < cols; j += blockDim.x) {
      out[i * cols + j] = in1[i * cols + j] + in2[i * cols + j];
    }
  }

  return;
}
