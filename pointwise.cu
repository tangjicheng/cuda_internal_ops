#include "pointwise.h"

// Pointwise函数的特化：1个输入，1个输出

template <>
int Pointwise<acosf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<acosf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<acoshf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<asinf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<asinhf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<atanhf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<atanf>(NDArray* out, const NDArray* in, cudaStream_t stream);

// 求立方根
template <>
int Pointwise<cbrtf>(NDArray* out, const NDArray* in, cudaStream_t stream);

// 向上取整
template <>
int Pointwise<ceilf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<cosf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<coshf>(NDArray* out, const NDArray* in, cudaStream_t stream);

// error function of input
template <>
int Pointwise<erff>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<exp10f>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<exp2f>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<expf>(NDArray* out, const NDArray* in, cudaStream_t stream);

// 取绝对值
template <>
int Pointwise<fabsf>(NDArray* out, const NDArray* in, cudaStream_t stream);

// 向下取整
template <>
int Pointwise<floorf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<log10f>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<log2f>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<logf>(NDArray* out, const NDArray* in, cudaStream_t stream);

// 四舍五入
template <>
int Pointwise<nearbyintf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<sinf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<sinhf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<sqrtf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<tanf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<tanhf>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<internal_ops_identity>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<internal_ops_neg>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<internal_ops_sign>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<internal_ops_sigmoid>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<internal_ops_softplus>(NDArray* out, const NDArray* in, cudaStream_t stream);

template <>
int Pointwise<internal_ops_softsign>(NDArray* out, const NDArray* in, cudaStream_t stream);

__global__ void gElement(int symbol, float* out, float scale, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      float* rowOut = out + j * cols;

      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          if (symbol == 0) {
            rowOut[i] += scale;
          } else if (symbol == 1) {
            rowOut[i] -= scale;
          } else if (symbol == 2) {
            rowOut[i] *= scale;
          } else if (symbol == 3) {
            rowOut[i] /= scale;
          }
        }
      }
    }
  }
}

__global__ void gElement(int symbol, float* out, const float* in1, float scale, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      float* rowOut = out + j * cols;
      const float* rowIn1 = in1 + j * cols;

      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          if (symbol == 0) {
            rowOut[i] = rowIn1[i] + scale;
          } else if (symbol == 1) {
            rowOut[i] = rowIn1[i] - scale;
          } else if (symbol == 2) {
            rowOut[i] = rowIn1[i] * scale;
          } else if (symbol == 3) {
            rowOut[i] = rowIn1[i] / scale;
          }
        }
      }
    }
  }
}

__global__ void gSub2(float* out, const float* in1, float scale, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      float* rowOut = out + j * cols;
      const float* rowIn1 = in1 + j * cols;

      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          rowOut[i] = scale - rowIn1[i];
        }
      }
    }
  }
}

__global__ void gElement(int symbol, float* out, const float* in1, const float* in2, int rows, int cols) {
  for (int bid = 0; bid < rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < rows) {
      float* rowOut = out + j * cols;
      const float* rowIn1 = in1 + j * cols;
      const float* rowIn2 = in2 + j * cols;

      for (int tid = 0; tid < cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < cols) {
          if (symbol == 0) {
            rowOut[i] = rowIn1[i] + rowIn2[i];
          } else if (symbol == 1) {
            rowOut[i] = rowIn1[i] - rowIn2[i];
          } else if (symbol == 2) {
            rowOut[i] = rowIn1[i] * rowIn2[i];
          } else if (symbol == 3) {
            rowOut[i] = rowIn1[i] / rowIn2[i];
          }
        }
      }
    }
  }
}

__global__ void gBroadcastAdd(float* out, const float* in1, int rows, int cols) {
  for (int j = blockIdx.x; j < rows; j += gridDim.x) {
    for (int i = threadIdx.x; i < cols; i += blockDim.x) {
      out[j * cols + i] += in1[i];
    }
  }
}

__global__ void gBroadcastAddNCHW(float* out, const float* in1, int n, int c, int h, int w) {
  int index_n = blockIdx.x;
  int index_c = blockIdx.y;
  int hw = h * w;
  int offset = index_n * c * hw + index_c * hw;
  for (int i = threadIdx.x; i < hw; i += blockDim.x) {
    int index_i = offset + i;
    out[index_i] += in1[index_c];
  }
}

__global__ void gBroadcastVec(int symbol, float* out, const float* in, int rows, int cols, int dim_size, int pre_mul) {
  for (int bid = blockIdx.x; bid < rows; bid += gridDim.x) {
    float* rowOut = out + bid * cols;
    for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
      int out_flat_index = bid * cols + tid;
      int in_id = (out_flat_index / pre_mul) % dim_size;
      if (symbol == 0) {
        rowOut[tid] = rowOut[tid] + in[in_id];
      } else if (symbol == 1) {
        rowOut[tid] = rowOut[tid] - in[in_id];
      } else if (symbol == 2) {
        rowOut[tid] = rowOut[tid] * in[in_id];
      } else if (symbol == 3) {
        rowOut[tid] = rowOut[tid] / in[in_id];
      }
    }
  }
}

__global__ void gBroadcastVec3D(int symbol, float* out, const float* in, int rows, int cols, int in_rows, int in_cols) {
  for (int bid = blockIdx.x; bid < rows; bid += gridDim.x) {
    float* rowOut = out + bid * cols;
    const float* rowIn = in + bid / (rows / in_rows) * in_cols;
    for (int tid = threadIdx.x; tid < cols; tid += blockDim.x) {
      if (symbol == 0) {
        rowOut[tid] = rowOut[tid] + rowIn[tid % in_cols];
      } else if (symbol == 1) {
        rowOut[tid] = rowOut[tid] - rowIn[tid % in_cols];
      } else if (symbol == 2) {
        rowOut[tid] = rowOut[tid] * rowIn[tid % in_cols];
      } else if (symbol == 3) {
        rowOut[tid] = rowOut[tid] / rowIn[tid % in_cols];
      }
    }
  }
}

__global__ void gElementBroadcast(int32_t symbol, int32_t out_rows, int32_t out_cols, int32_t input_ndarry_size,
                                  int32_t out_ndim, int64_t* data, float* offset, float* stride, float* prod_row) {
  float* out = reinterpret_cast<float*>((int64_t)(data[0]));

  for (int j = blockIdx.x; j < out_rows; j += gridDim.x) {
    extern __shared__ float _share[];
    float* _index = _share;

    float prod_sum = 0.0f;
    int row = j;
    int out_index = 0;
    for (int idim = 0; idim < out_ndim - 1; idim++) {
      if (idim < out_ndim - 2) {
        _index[idim] = (int)(row / prod_row[idim]);
        prod_sum += _index[idim] * prod_row[idim];
        row = j - prod_sum;
      } else {
        _index[idim] = row;
      }

      out_index += _index[idim] * (int)(stride[idim]);
    }

    for (int i = threadIdx.x; i < out_cols; i += blockDim.x) {
      int out_index_thread = out_index;
      out_index_thread += i * stride[out_ndim - 1];
      out[out_index_thread] = 0.0f;
      for (int32_t idx_input = 0; idx_input < input_ndarry_size; idx_input++) {
        float* in = reinterpret_cast<float*>((int64_t)(data[1 + idx_input]));
        int in_index = 0;
        for (int idim = 0; idim < out_ndim - 1; idim++) {
          in_index += _index[idim] * stride[out_ndim + idx_input * out_ndim + idim];
        }
        in_index += i * stride[out_ndim + idx_input * out_ndim + out_ndim - 1];

        if (symbol == Symbol::plus) {
          out[out_index_thread] += in[in_index];
        }
      }
    }
  }
}

__global__ void gElementBroadcast2(int32_t symbol, int32_t out_rows, int32_t out_cols, int32_t input_ndarry_size,
                                   int32_t out_ndim, int64_t* data, float* offset, float* stride, float* prod_row) {
  float* out = reinterpret_cast<float*>((int64_t)(data[0]));

  for (int j = blockIdx.x; j < out_rows; j += gridDim.x) {
    extern __shared__ float _share[];
    float* _index = _share;

    float prod_sum = 0.0f;
    int row = j;
    int out_index = 0;
    for (int idim = 0; idim < out_ndim - 1; idim++) {
      if (idim < out_ndim - 2) {
        _index[idim] = (int)(row / prod_row[idim]);
        prod_sum += _index[idim] * prod_row[idim];
        row = j - prod_sum;
      } else {
        _index[idim] = row;
      }

      out_index += _index[idim] * (int)(stride[idim]);
    }

    for (int i = threadIdx.x; i < out_cols; i += blockDim.x) {
      int out_index_thread = out_index;
      out_index_thread += i * stride[out_ndim - 1];
      out[out_index_thread] = 0.0f;

      /*
      // 原来的for循环计算 input_ndarry_size 个输入的in_index，但是目前只需要计算2个
      for (int32_t idx_input = 0; idx_input < input_ndarry_size; idx_input++)
      {
      float* in = reinterpret_cast<float*>((int64_t)(data[1 + idx_input]));
      int in_index = 0;
      for (int idim = 0; idim < out_ndim - 1; idim++)
      {
          in_index += _index[idim] * stride[out_ndim + idx_input * out_ndim + idim];
      }
      in_index += i * stride[out_ndim + idx_input * out_ndim + out_ndim - 1];
      */

      // 只有两个input(in1, in2), out[out_index] = in1[in1_index] / in2[in2_index]
      float* in1 = reinterpret_cast<float*>((int64_t)(data[1]));
      float* in2 = reinterpret_cast<float*>((int64_t)(data[2]));
      // 计算in1_index
      int idx_input = 0;
      int in1_index = 0;
      for (int idim = 0; idim < out_ndim - 1; idim++) {
        in1_index += _index[idim] * stride[out_ndim + idx_input * out_ndim + idim];
      }
      in1_index += i * stride[out_ndim + idx_input * out_ndim + out_ndim - 1];
      // 计算in2_index
      idx_input = 1;
      int in2_index = 0;
      for (int idim = 0; idim < out_ndim - 1; idim++) {
        in2_index += _index[idim] * stride[out_ndim + idx_input * out_ndim + idim];
      }
      in2_index += i * stride[out_ndim + idx_input * out_ndim + out_ndim - 1];
      // 进行运算
      if (symbol == Symbol::minus) {
        out[out_index_thread] = in1[in1_index] - in2[in2_index];
      } else if (symbol == Symbol::division) {
        out[out_index_thread] = in1[in1_index] / in2[in2_index];
      } else if (symbol == Symbol::pow) {
        out[out_index_thread] = powf(in1[in1_index], in2[in2_index]);
      } else if (symbol == Symbol::times) {
        out[out_index_thread] = in1[in1_index] * in2[in2_index];
      }
    }
  }
}

__global__ void gGather(float* outputPtr, const float* inputPtr, const float* indicesPtr, int outside, int N,
                        int outputOutsideStride, int inputOutsideStride, int insideStride, int limit) {
  for (int j = blockIdx.x; j < outside; j += gridDim.x) {
    float* outputO = outputPtr + outputOutsideStride * j;
    const float* inputO = inputPtr + inputOutsideStride * j;

    for (int i = threadIdx.x; i < N; i += blockDim.x) {
      if (indicesPtr[i] >= 0 && indicesPtr[i] < limit) {
        for (int idim = 0; idim < insideStride; idim++) {
          outputO[i * insideStride + idim] = inputO[insideStride * (int32_t)(indicesPtr[i]) + idim];
        }
      }
    }
  }
}

__global__ void gConcat(float* outputPtr, int64_t* inputPtr, int32_t inputSize, int32_t num_concats,
                        int32_t concat_size, int32_t output_concat_stride, float* input_limit) {
  for (int j = blockIdx.x; j < inputSize; j += gridDim.x) {
    int output_concat_axis_offset = 0;
    for (int32_t j_pre = 0; j_pre < j; j_pre++) {
      output_concat_axis_offset += (int32_t)(input_limit[j_pre]);
    }

    float* curInputPtr = reinterpret_cast<float*>((int64_t)(inputPtr[j]));
    const int32_t input_concat_axis = (int32_t)(input_limit[j]);
    const int32_t input_concat_stride = input_concat_axis * concat_size;

    for (int i = threadIdx.x; i < num_concats; i += blockDim.x) {
      float* outputO = outputPtr + output_concat_stride * i;
      float* input0 = curInputPtr + input_concat_stride * i;
      for (int idim = 0; idim < input_concat_stride; idim++) {
        outputO[output_concat_axis_offset * concat_size + idim] = input0[idim];
      }
    }
  }
}

__global__ void gSliceCopy(float* out, const float* in, size_t in_row_begin, size_t in_col_begin, size_t in_col_len,
                           size_t out_row_len, size_t out_col_len) {
  for (int bid = 0; bid < out_row_len; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < out_row_len) {
      float* rowOut = out + j * out_col_len;
      const float* rowIn = in + (in_row_begin + j) * in_col_len + in_col_begin;
      for (int tid = 0; tid < out_col_len; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < out_col_len) {
          rowOut[i] = rowIn[i];
        }
      }
    }
  }
}

__global__ void gShapeStrideCopy(float* out_shape, float* out_stride, const int* in_shape, const int* in_stride,
                                 size_t out_row_len, size_t out_col_len) {
  for (int bid = 0; bid < out_row_len; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < out_row_len) {
      float* row_shape_out = out_shape + j * out_col_len;
      float* row_stride_out = out_stride + j * out_col_len;
      const int* row_shape_in = in_shape + j * out_col_len;
      const int* row_stride_in = in_stride + j * out_col_len;
      for (int tid = 0; tid < out_col_len; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < out_col_len) {
          row_shape_out[i] = row_shape_in[i];
          row_stride_out[i] = row_stride_in[i];
        }
      }
    }
  }
}

__global__ void gCopyRowsGeneral(float* out, const float* in, size_t cols, size_t* dst_ids, size_t* src_ids,
                                 size_t numPairs) {
  for (int row = blockIdx.x * blockDim.y + threadIdx.y; row < numPairs; row += gridDim.x * blockDim.y) {
    size_t dstId = dst_ids[row];
    size_t srcId = src_ids[row];

    float* rowOut = out + dstId * cols;
    const float* rowIn = in + srcId * cols;

    for (int col = threadIdx.x; col < cols; col += blockDim.x) {
      rowOut[col] = rowIn[col];
    }
  }
}

__global__ void gSetValue(float* out, int32_t out_rows, int32_t out_cols, float value) {
  for (int bid = 0; bid < out_rows; bid += gridDim.x) {
    int j = bid + blockIdx.x;
    if (j < out_rows) {
      for (int tid = 0; tid < out_cols; tid += blockDim.x) {
        int i = tid + threadIdx.x;
        if (i < out_cols) {
          out[j * out_cols + i] = value;
        }
      }
    }
  }
}